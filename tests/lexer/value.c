#include <filecontext.h>
#include <generated/tkind.h>
#include <lexer/token.h>
#include <lexer/lexer.h>

#include "../test.h"

static void helper_string(int kind, const char* match, const char* text, size_t idx)
{
    FileContext fctx;
    Stream stream;
    Lexer lexer;
    Token* tok;

    size_t mlen = strlen(match);

    ASSERT_SYSTEM(filecontext_init(&fctx, NULL));
    ASSERT_SYSTEM(stream_init_strRD(&stream, text, -1));
    ASSERT_SYSTEM(lexer_init(&lexer, &stream, &fctx));

    size_t i;
    for(i = 0; i <= idx; i++)
    {
        tok = lexer_next(&lexer, false);
        ASSERT_NONNULL(tok);
        if(i != idx)
            continue;

        ASSERT_EQ(kind, tok->kind);
        ASSERT_MEMEQ(match, mlen, tok->value.v.istr->ptr, tok->value.v.istr->len);
    }

    //lexer_deinit(&lexer);
    stream_close(&stream);
    filecontext_deinit(&fctx);
}

TEST(string1_c)   { helper_string(TK_STRING, "hello,\tworld", "int x = \"hello,\\tworld\"c;", 3); }
TEST(string2_c)   { helper_string(TK_STRING, "abc\x15" "def", "\"abc\\x15def\"c", 0); }
TEST(string3_c)   { helper_string(TK_STRING, "abc\xF0\x90\x8E\xAB" "def", "\"abc\\U000103ABdef\"c", 0); }
TEST(ident1)    { helper_string(TK_IDENT, "xyz", "int x = xyz;", 3); }
TEST(ident2)    { helper_string(TK_IDENT, "max", "5 + 0.max / 2", 4); }
TEST(bom)       { helper_string(TK_IDENT, "foo", "\xEF\xBB\xBF" "int foo = 3;", 1); }
TEST(shebang_c)   { helper_string(TK_STRING, "f", "#!/usr/bin/env coy\nint x = \"f\"c;", 3); }
TEST(bombang_c)   { helper_string(TK_IDENT, "z", "\xEF\xBB\xBF#!/usr/bin/env coy\nint x = 'f'c - z;", 5); }

void test_main(void)
{
    RUN_TEST(string1_c);
    RUN_TEST(string2_c);
    RUN_TEST(string3_c);
    RUN_TEST(ident1);
    RUN_TEST(ident2);
    RUN_TEST(bom);
    RUN_TEST(shebang_c);
    RUN_TEST(bombang_c);
}

#include "../test-runner.c"
