#include <filecontext.h>
#include <generated/tkind.h>
#include <lexer/token.h>
#include <lexer/lexer.h>

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>

#include "../test.h"

static void helper_string(TokenKind kind, const char* str, size_t len, bool mustpass)
{
    FileContext fctx;
    Stream stream;
    Lexer lexer;
    Token* tok1;
    Token* tok2;

    ASSERT_SYSTEM(filecontext_init(&fctx, NULL));
    ASSERT_SYSTEM(stream_init_memRD(&stream, str, len));
    ASSERT_SYSTEM(lexer_init(&lexer, &stream, &fctx));

    if(mustpass)
    {
        tok1 = lexer_next(&lexer, false);
        ASSERT_NONNULL(tok1);
        ASSERT_EQ(kind, tok1->kind);
        tok2 = lexer_next(&lexer, false);
        ASSERT_NONNULL(tok2);
        ASSERT_EQ(TK_EOF, tok2->kind);
    }
    else
    {
        tok1 = lexer_next(&lexer, false);
        if(!tok1) return;
        tok2 = lexer_next(&lexer, false);
        if(!tok2) return;
        ASSERT(kind != tok1->kind || TK_EOF != tok2->kind);
    }

    //lexer_deinit(&lexer);
    stream_close(&stream);
    filecontext_deinit(&fctx);
}
#define CHECK_STRING(TKIND, STR, MUSTPASS)  helper_string((TKIND), (STR), sizeof((STR)) - 1, (MUSTPASS))

TEST(ops_assign)
{
    CHECK_STRING(TK_EQ, "=", true);
    CHECK_STRING(TK_PLUSEQ, "+=", true);
    CHECK_STRING(TK_MINUSEQ, "-=", true);
    CHECK_STRING(TK_STAREQ, "*=", true);
    CHECK_STRING(TK_SLASHEQ, "/=", true);
    CHECK_STRING(TK_PERCENTEQ, "%=", true);
    CHECK_STRING(TK_PERCENTPERCENTEQ, "%%=", true);
    CHECK_STRING(TK_STARSTAREQ, "**=", true);
    CHECK_STRING(TK_CARETEQ, "^=", true);
    CHECK_STRING(TK_AMPEQ, "&=", true);
    CHECK_STRING(TK_PIPEEQ, "|=", true);
    CHECK_STRING(TK_LTLTEQ, "<<=", true);
    CHECK_STRING(TK_GTGTEQ, ">>=", true);
    CHECK_STRING(TK_GTGTGTEQ, ">>>=", true);
    CHECK_STRING(TK_LTLTPIPEEQ, "<<|=", true);
    CHECK_STRING(TK_PIPEGTGTEQ, "|>>=", true);
    CHECK_STRING(TK_TILDEEQ, "~=", true);
}

TEST(ops_arithmetic)
{
    CHECK_STRING(TK_PLUSPLUS, "++", true);
    CHECK_STRING(TK_MINUSMINUS, "--", true);

    CHECK_STRING(TK_PLUS, "+", true);
    CHECK_STRING(TK_MINUS, "-", true);
    CHECK_STRING(TK_STAR, "*", true);
    CHECK_STRING(TK_SLASH, "/", true);
    CHECK_STRING(TK_PERCENT, "%", true);
    CHECK_STRING(TK_PERCENTPERCENT, "%%", true);
    CHECK_STRING(TK_STARSTAR, "**", true);
}

TEST(ops_bitwise)
{
    CHECK_STRING(TK_CARET, "^", true);
    CHECK_STRING(TK_AMP, "&", true);
    CHECK_STRING(TK_PIPE, "|", true);
    CHECK_STRING(TK_LTLT, "<<", true);
    CHECK_STRING(TK_GTGT, ">>", true);
    CHECK_STRING(TK_GTGTGT, ">>>", true);
    CHECK_STRING(TK_LTLTPIPE, "<<|", true);
    CHECK_STRING(TK_PIPEGTGT, "|>>", true);
}

TEST(ops_logical)
{
    CHECK_STRING(TK_AMPAMP, "&&", true);
    CHECK_STRING(TK_PIPEPIPE, "||", true);
}

TEST(ops_cmp)
{
    CHECK_STRING(TK_IS, "is", true);
    CHECK_STRING(TK_EXCLAIMIS, "!is", true);
    CHECK_STRING(TK_LTEQGT, "<=>", true);
    CHECK_STRING(TK_EQEQ, "==", true);
    CHECK_STRING(TK_EXCLAIMEQ, "!=", true);
    CHECK_STRING(TK_LT, "<", true);
    CHECK_STRING(TK_LTEQ, "<=", true);
    CHECK_STRING(TK_GT, ">", true);
    CHECK_STRING(TK_GTEQ, ">=", true);
}

TEST(ops_misc)
{
    CHECK_STRING(TK_TILDE, "~", true);
    CHECK_STRING(TK_IN, "in", true);
    CHECK_STRING(TK_EXCLAIMIN, "!in", true);
    CHECK_STRING(TK_EXCLAIM, "!", true);
    CHECK_STRING(TK_AT, "@", true);
    CHECK_STRING(TK_DOLLAR, "$", true);
    CHECK_STRING(TK_COMMA, ",", true);
    CHECK_STRING(TK_SCOLON, ";", true);
    CHECK_STRING(TK_COLON, ":", true);
    CHECK_STRING(TK_QMARK, "?", true);
    CHECK_STRING(TK_DOT, ".", true);
    CHECK_STRING(TK_DOTDOT, "..", true);
    CHECK_STRING(TK_ELLIPSIS, "...", true);
}

TEST(braces)
{
    CHECK_STRING(TK_LPAREN, "(", true);
    CHECK_STRING(TK_RPAREN, ")", true);
    CHECK_STRING(TK_LARRAY, "[", true);
    CHECK_STRING(TK_RARRAY, "]", true);
    CHECK_STRING(TK_LBRACE, "{", true);
    CHECK_STRING(TK_RBRACE, "}", true);
}

TEST(keywords_record)
{
    CHECK_STRING(TK_CLASS, "class", true);
    CHECK_STRING(TK_STRUCT, "struct", true);
    CHECK_STRING(TK_INTERFACE, "interface", true);
    CHECK_STRING(TK_DELEGATE, "delegate", true);
    CHECK_STRING(TK_ENUM, "enum", true);
}

TEST(type)
{
    CHECK_STRING(TK_TYPE, "void", true);
    CHECK_STRING(TK_TYPE, "bool", true);
    CHECK_STRING(TK_TYPE, "char", true);
    CHECK_STRING(TK_TYPE, "byte", true);
    CHECK_STRING(TK_TYPE, "ubyte", true);
    CHECK_STRING(TK_TYPE, "int", true);
    CHECK_STRING(TK_TYPE, "uint", true);
    CHECK_STRING(TK_TYPE, "long", true);
    CHECK_STRING(TK_TYPE, "ulong", true);
    CHECK_STRING(TK_TYPE, "float", true);
    CHECK_STRING(TK_TYPE, "double", true);
    CHECK_STRING(TK_TYPE, "real", true);
}

TEST(keywords_gen)
{
    CHECK_STRING(TK_OUT, "out", true);
    CHECK_STRING(TK_REF, "ref", true);
    CHECK_STRING(TK_NATIVE, "native", true);
    CHECK_STRING(TK_FINAL, "final", true);
    CHECK_STRING(TK_ABSTRACT, "abstract", true);
    CHECK_STRING(TK_STATIC, "static", true);
    CHECK_STRING(TK_PROPERTY, "property", true);
    CHECK_STRING(TK_OVERRIDE, "override", true);
    CHECK_STRING(TK_ASSERT, "assert", true);
    CHECK_STRING(TK_IF, "if", true);
    CHECK_STRING(TK_ELSE, "else", true);
    CHECK_STRING(TK_FOR, "for", true);
    CHECK_STRING(TK_FOREACH, "foreach", true);
    CHECK_STRING(TK_FOREACH_R, "foreach_r", true);
    CHECK_STRING(TK_WHILE, "while", true);
    CHECK_STRING(TK_DO, "do", true);
    CHECK_STRING(TK_SWITCH, "switch", true);
    CHECK_STRING(TK_CASE, "case", true);
    CHECK_STRING(TK_BREAK, "break", true);
    CHECK_STRING(TK_CONTINUE, "continue", true);
    CHECK_STRING(TK_GOTO, "goto", true);
    CHECK_STRING(TK_RETURN, "return", true);
    CHECK_STRING(TK_MODULE, "module", true);
    CHECK_STRING(TK_DEFAULT, "default", true);
    CHECK_STRING(TK_THIS, "this", true);
    CHECK_STRING(TK_SUPER, "super", true);
    CHECK_STRING(TK_NULL, "null", true);
    CHECK_STRING(TK_TRUE, "true", true);
    CHECK_STRING(TK_FALSE, "false", true);
    CHECK_STRING(TK_PRAGMA, "pragma", true);
}

TEST(reserved)
{
    CHECK_STRING(TK_RESERVED, "wchar", true);
    CHECK_STRING(TK_RESERVED, "dchar", true);
    CHECK_STRING(TK_RESERVED, "cent", true);
    CHECK_STRING(TK_RESERVED, "ucent", true);
    CHECK_STRING(TK_RESERVED, "ifloat", true);
    CHECK_STRING(TK_RESERVED, "idouble", true);
    CHECK_STRING(TK_RESERVED, "ireal", true);
    CHECK_STRING(TK_RESERVED, "cfloat", true);
    CHECK_STRING(TK_RESERVED, "cdouble", true);
    CHECK_STRING(TK_RESERVED, "creal", true);
    CHECK_STRING(TK_RESERVED, "asm", true);
    CHECK_STRING(TK_RESERVED, "label", true);
    CHECK_STRING(TK_RESERVED, "public", true);
    CHECK_STRING(TK_RESERVED, "private", true);
    CHECK_STRING(TK_RESERVED, "protected", true);
    CHECK_STRING(TK_RESERVED, "package", true);
    CHECK_STRING(TK_RESERVED, "typeof", true);
    CHECK_STRING(TK_RESERVED, "sizeof", true);
    CHECK_STRING(TK_RESERVED, "dispose", true);
    CHECK_STRING(TK_RESERVED, "throw", true);
    CHECK_STRING(TK_RESERVED, "try", true);
    CHECK_STRING(TK_RESERVED, "catch", true);
    CHECK_STRING(TK_RESERVED, "finally", true);
    CHECK_STRING(TK_RESERVED, "defer", true);
    CHECK_STRING(TK_RESERVED, "synchronized", true);
    CHECK_STRING(TK_RESERVED, "atomic", true);
    CHECK_STRING(TK_RESERVED, "locked", true);
    CHECK_STRING(TK_RESERVED, "unittest", true);
}

TEST(integer)
{
    CHECK_STRING(TK_INTEGER, "0x0", true);
    CHECK_STRING(TK_INTEGER, "0X12FfAaCc", true);
    CHECK_STRING(TK_INTEGER, "0x123fU", true);
    CHECK_STRING(TK_INTEGER, "0x123fL", true);
    CHECK_STRING(TK_INTEGER, "0x123fLU", true);
    CHECK_STRING(TK_INTEGER, "0x123fUL", true);
    CHECK_STRING(TK_INTEGER, "0q0", true);
    CHECK_STRING(TK_INTEGER, "0Q127", true);
    CHECK_STRING(TK_INTEGER, "0q127U", true);
    CHECK_STRING(TK_INTEGER, "0q127L", true);
    CHECK_STRING(TK_INTEGER, "0q127LU", true);
    CHECK_STRING(TK_INTEGER, "0Q127UL", true);
    CHECK_STRING(TK_INTEGER, "0b0", true);
    CHECK_STRING(TK_INTEGER, "0B1011", true);
    CHECK_STRING(TK_INTEGER, "0b1100U", true);
    CHECK_STRING(TK_INTEGER, "0b1100L", true);
    CHECK_STRING(TK_INTEGER, "0b1100UL", true);
    CHECK_STRING(TK_INTEGER, "0b1100LU", true);
    CHECK_STRING(TK_INTEGER, "0", true);
    CHECK_STRING(TK_INTEGER, "019", true);
    CHECK_STRING(TK_INTEGER, "123", true);
    CHECK_STRING(TK_INTEGER, "123U", true);
    CHECK_STRING(TK_INTEGER, "123u", true);
    CHECK_STRING(TK_INTEGER, "123L", true);
    CHECK_STRING(TK_INTEGER, "123UL", true);
    CHECK_STRING(TK_INTEGER, "123uL", true);
    CHECK_STRING(TK_INTEGER, "123LU", true);
    CHECK_STRING(TK_INTEGER, "123Lu", true);
    CHECK_STRING(TK_INTEGER, "123r", false);
    CHECK_STRING(TK_INTEGER, "123d", false);
    CHECK_STRING(TK_INTEGER, "123f", false);
}

TEST(real)
{
    CHECK_STRING(TK_REAL, "0.0", true);
    CHECK_STRING(TK_REAL, "7.35d", true);
    CHECK_STRING(TK_REAL, "7.35f", true);
    CHECK_STRING(TK_REAL, "7.35e12", true);
    CHECK_STRING(TK_REAL, "7.35e+12", true);
    CHECK_STRING(TK_REAL, "7.35e-12", true);
    CHECK_STRING(TK_REAL, "7.35e-12f", true);
    CHECK_STRING(TK_REAL, "7e5", true);
    CHECK_STRING(TK_REAL, "7e5d", true);
    CHECK_STRING(TK_REAL, "7e5f", true);
    CHECK_STRING(TK_REAL, "3.5U", false);
    CHECK_STRING(TK_REAL, "3.5fU", false);
    CHECK_STRING(TK_REAL, "3.5Uf", false);
    CHECK_STRING(TK_REAL, "3.5L", false);
    CHECK_STRING(TK_REAL, "3.5u", false);
}

TEST(char)
{
    SKIP();
    CHECK_STRING(TK_CHAR, "'x'", true);
    CHECK_STRING(TK_CHAR, "'y'", true);
    CHECK_STRING(TK_CHAR, "'z'", true);
    CHECK_STRING(TK_CHAR, "'abc'", false);
    CHECK_STRING(TK_CHAR, "''", false);
    CHECK_STRING(TK_CHAR, "'\\x30'", true);
    CHECK_STRING(TK_CHAR, "'\\033'", true);
    CHECK_STRING(TK_CHAR, "'\\t'", true);
    CHECK_STRING(TK_CHAR, "'\\\\'", true);
    CHECK_STRING(TK_CHAR, "'\\''", true);
    CHECK_STRING(TK_CHAR, "'x", false);
}
TEST(char_c)
{
    CHECK_STRING(TK_CHAR, "'x'c", true);
    CHECK_STRING(TK_CHAR, "'y'c", true);
    CHECK_STRING(TK_CHAR, "'z'c", true);
    CHECK_STRING(TK_CHAR, "'abc'c", false);
    CHECK_STRING(TK_CHAR, "''c", false);
    CHECK_STRING(TK_CHAR, "'\\x30'c", true);
    CHECK_STRING(TK_CHAR, "'\\033'c", true);
    CHECK_STRING(TK_CHAR, "'\\t'c", true);
    CHECK_STRING(TK_CHAR, "'\\\\'c", true);
    CHECK_STRING(TK_CHAR, "'\\''c", true);
}
TEST(char_w)
{
    SKIP();
    CHECK_STRING(TK_CHAR, "'x'w", true);
    CHECK_STRING(TK_CHAR, "'y'w", true);
    CHECK_STRING(TK_CHAR, "'z'w", true);
    CHECK_STRING(TK_CHAR, "'abc'w", false);
    CHECK_STRING(TK_CHAR, "''w", false);
    CHECK_STRING(TK_CHAR, "'\\x30'w", true);
    CHECK_STRING(TK_CHAR, "'\\033'w", true);
    CHECK_STRING(TK_CHAR, "'\\t'w", true);
    CHECK_STRING(TK_CHAR, "'\\\\'w", true);
    CHECK_STRING(TK_CHAR, "'\\''w", true);
}
TEST(char_d)
{
    SKIP();
    CHECK_STRING(TK_CHAR, "'x'd", true);
    CHECK_STRING(TK_CHAR, "'y'd", true);
    CHECK_STRING(TK_CHAR, "'z'd", true);
    CHECK_STRING(TK_CHAR, "'abc'd", false);
    CHECK_STRING(TK_CHAR, "''d", false);
    CHECK_STRING(TK_CHAR, "'\\x30'd", true);
    CHECK_STRING(TK_CHAR, "'\\033'd", true);
    CHECK_STRING(TK_CHAR, "'\\t'd", true);
    CHECK_STRING(TK_CHAR, "'\\\\'d", true);
    CHECK_STRING(TK_CHAR, "'\\''d", true);
}

TEST(string)
{
    SKIP();
    CHECK_STRING(TK_STRING, "\"\"", true);
    CHECK_STRING(TK_STRING, "\"abc\"", true);
    CHECK_STRING(TK_STRING, "\"abc\\\\\"", true);
    CHECK_STRING(TK_STRING, "\"abc\\\\\"\"", false);
    CHECK_STRING(TK_STRING, "\"abc\\\"", false);
    CHECK_STRING(TK_STRING, "\"abc\\\"\"", true);
    CHECK_STRING(TK_STRING, "\"abc\\\"def\\\\ghi\\nijk\"", true);
    CHECK_STRING(TK_STRING, "\"foo\\xaft\"", true);
    CHECK_STRING(TK_STRING, "\"foo\\xant\"", false);
    CHECK_STRING(TK_STRING, "\"foo\\u1234t\"", true);
    CHECK_STRING(TK_STRING, "\"foo\\u123nt\"", false);
    CHECK_STRING(TK_STRING, "\"foo\\U00010348t\"", true);
    CHECK_STRING(TK_STRING, "\"foo\\U0001034nt\"", false);
    CHECK_STRING(TK_STRING, "r\"\"", true);
    CHECK_STRING(TK_STRING, "r\"abc\"", true);
    CHECK_STRING(TK_STRING, "r\"abc\\ndef\"", true);
    CHECK_STRING(TK_STRING, "r\"abc\\\"", true);
    CHECK_STRING(TK_STRING, "r\"abc\\\"\"", false);
    CHECK_STRING(TK_STRING, "r\"abc\\\"\"\"", true);
    CHECK_STRING(TK_STRING, "r\"abc\"\"def\"", true);
}
TEST(string_c)
{
    CHECK_STRING(TK_STRING, "\"foo\"c", true);
    CHECK_STRING(TK_STRING, "\"foo\\x30\"c", true);
    CHECK_STRING(TK_STRING, "\"foo\\u1030\"c", true);
    CHECK_STRING(TK_STRING, "\"foo\\U00010437\"c", true);
    CHECK_STRING(TK_STRING, "r\"foo\"c", true);
}
TEST(string_w)
{
    SKIP();
    CHECK_STRING(TK_STRING, "\"foo\"w", true);
    CHECK_STRING(TK_STRING, "\"foo\\x30\"w", true);
    CHECK_STRING(TK_STRING, "\"foo\\u1030\"w", true);
    CHECK_STRING(TK_STRING, "\"foo\\U00010437\"w", true);
    CHECK_STRING(TK_STRING, "r\"foo\"w", true);
}
TEST(string_d)
{
    SKIP();
    CHECK_STRING(TK_STRING, "\"foo\"d", true);
    CHECK_STRING(TK_STRING, "\"foo\\x30\"d", true);
    CHECK_STRING(TK_STRING, "\"foo\\u1030\"d", true);
    CHECK_STRING(TK_STRING, "\"foo\\U00010437\"d", true);
    CHECK_STRING(TK_STRING, "r\"foo\"d", true);
}

TEST(xstring)
{
    SKIP();
    CHECK_STRING(TK_XSTRING, "x\"\"", true);
    CHECK_STRING(TK_XSTRING, "x\"10\"", true);
    CHECK_STRING(TK_XSTRING, "x\"10 ef aa 33\"", true);
    CHECK_STRING(TK_XSTRING, "x\"10 ef a a3 3\"", true);
    CHECK_STRING(TK_XSTRING, "x\"10 ef a a3\"", false);
    CHECK_STRING(TK_XSTRING, "x\"10 ef aa bb cc\"c", true);
    CHECK_STRING(TK_XSTRING, "x\"10 ef aa bb cc\"w", false);
    CHECK_STRING(TK_XSTRING, "x\"10 ef aa bb cc\"d", false);
    CHECK_STRING(TK_XSTRING, "x\"10 ef aa bb cc dd\"w", true);
    CHECK_STRING(TK_XSTRING, "x\"10 ef aa bb cc dd\"d", false);
    CHECK_STRING(TK_XSTRING, "x\"10 ef aa bb cc dd ee ff\"d", true);
}

TEST(ident)
{
    CHECK_STRING(TK_IDENT, "a", true);
    CHECK_STRING(TK_IDENT, "_", true);
    CHECK_STRING(TK_IDENT, "A", true);
    CHECK_STRING(TK_IDENT, "a123", true);
    CHECK_STRING(TK_IDENT, "a___", true);
    CHECK_STRING(TK_IDENT, "_A7e5fgj", true);
}

void test_main(void)
{
    RUN_TEST(ops_assign);
    RUN_TEST(ops_arithmetic);
    RUN_TEST(ops_bitwise);
    RUN_TEST(ops_logical);
    RUN_TEST(ops_cmp);
    RUN_TEST(ops_misc);
    RUN_TEST(braces);
    RUN_TEST(keywords_record);
    RUN_TEST(type);
    RUN_TEST(keywords_gen);
    RUN_TEST(reserved);
    RUN_TEST(integer);
    RUN_TEST(real);
    RUN_TEST(char);
    RUN_TEST(char_c);
    RUN_TEST(char_w);
    RUN_TEST(char_d);
    RUN_TEST(string);
    RUN_TEST(string_c);
    RUN_TEST(string_w);
    RUN_TEST(string_d);
    RUN_TEST(xstring);
    RUN_TEST(ident);
}

#include "../test-runner.c"
