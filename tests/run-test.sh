#!/bin/sh
#run_test "$NAME"
NAME="$1"

"./$NAME" &
PID="$!"
wait "$PID"
result="$?"
if [ "$result" != 0 ]; then
    gdb "./$NAME" "./core.$NAME.$PID" -ex "thread apply all bt" -ex "set pagination 0" -batch
    exit "$result"
fi
