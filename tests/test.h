#ifndef TEST_H_
#define TEST_H_

#include <stdlib.h>
#include <setjmp.h>
#include <string.h>
#include <stdio.h>

typedef struct TestMessage TestMessage;
typedef struct TestResult TestResult;

struct TestResult
{
    FILE* file;
    const char* name;
    size_t pass;
    size_t fail;
    size_t skip;
    jmp_buf jbuf;
};

static TestResult TEST_RES;

#define TEST(NAME)          static void test_##NAME(void)
#define SKIP()              longjmp(TEST_RES.jbuf, 1)
#define RUN_TEST(NAME)      do { int jval_ = setjmp(TEST_RES.jbuf); if(!jval_) { TEST_RES.name = #NAME; test_##NAME(); TEST_RES.pass++; fprintf(TEST_RES.file, "PASS %s\n", #NAME); } else if(jval_ == 1) { TEST_RES.skip++; fprintf(TEST_RES.file, "SKIP %s\n", #NAME); } else if(jval_ == 2) { TEST_RES.fail++; } else exit(2); } while(0)

#define ASSERT_BASE(FAILKIND, JVAL, TEST, ...)  do { if(!(TEST)) { fprintf(TEST_RES.file, "%s %s(%d): ", FAILKIND, TEST_RES.name, __LINE__); fprintf(TEST_RES.file, __VA_ARGS__); fprintf(TEST_RES.file, "\n"); fflush(TEST_RES.file); longjmp(TEST_RES.jbuf, (JVAL)); } } while(0)

#define ASSERT_MSG(TEST, ...)       ASSERT_BASE("FAIL", 2, (TEST), __VA_ARGS__);

#define ASSERT_SYSTEMMSG(TEST, ...) ASSERT_BASE("FAIL-SYSTEM", 3, (TEST), __VA_ARGS__);
#define ASSERT_SYSTEM(TEST)         ASSERT_SYSTEMMSG(TEST, #TEST)

#define ASSERT(A)               ASSERT_MSG((A), #A)

#define ASSERT_NULL(A)          ASSERT_MSG(!(A), #A " is NULL")
#define ASSERT_NONNULL(A)       ASSERT_MSG((A), #A " is not NULL")

#define ASSERT_EQ(A, B)         ASSERT_MSG((A) == (B), #A " == " #B)
#define ASSERT_NE(A, B)         ASSERT_MSG((A) != (B), #A " != " #B)

#define ASSERT_MEMEQ(PTR1, LEN1, PTR2, LEN2)    do { size_t len1_ = (LEN1), len2_ = (LEN2); const void* ptr1_ = (PTR1); const void* ptr2_ = (PTR2); \
    ASSERT_MSG(len1_ == len2_ && !memcmp(ptr1_, ptr2_, len1_), #PTR1 "(" #LEN1 ")" " <memeq> " #PTR2 "(" #LEN2 ")");    \
    } while(0)

void test_main(void);

#endif /* TEST_H_ */
