#include <filecontext.h>
#include <generated/tkind.h>
#include <lexer/token.h>
#include <lexer/lexer.h>
#include <parser/ast.h>
#include <parser/parser.h>

#include "../test.h"

static void helper_file(const char* fname)
{
    FileContext fctx;
    Stream stream;
    Lexer lexer;
    Parser parser;
    ANode* root;

    ASSERT_SYSTEM(filecontext_init(&fctx, NULL));
    ASSERT_SYSTEM(stream_init_fnameRD(&stream, fname));
    ASSERT_SYSTEM(lexer_init(&lexer, &stream, &fctx));
    ASSERT_SYSTEM(parser_init(&parser, &lexer, &fctx));

    root = parser_exec(&parser);
    ASSERT_NONNULL(root);

    //lexer_deinit(&lexer);
    stream_close(&stream);
    filecontext_deinit(&fctx);
}

TEST(factorial)
{
    helper_file("tests/semantic/factorial.coy");
}

void test_main(void)
{
    RUN_TEST(factorial);
}

#include "../test-runner.c"
