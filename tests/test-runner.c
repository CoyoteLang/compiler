#include "test.h"

int main(void)
{
    TEST_RES.file = stdout;
    test_main();
    fprintf(TEST_RES.file, "----------\n");
    fprintf(TEST_RES.file, "PASS\tFAIL\tSKIP\n");
    fprintf(TEST_RES.file, "%u\t%u\t%u\n", (unsigned int)TEST_RES.pass, (unsigned int)TEST_RES.fail, (unsigned int)TEST_RES.skip);
    return !!TEST_RES.fail;
}
