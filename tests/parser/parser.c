#include <filecontext.h>
#include <generated/tkind.h>
#include <lexer/token.h>
#include <lexer/lexer.h>
#include <parser/ast.h>
#include <parser/parser.h>

#include "../test.h"

static void helper_string(const char* text)
{
    FileContext fctx;
    Stream stream;
    Lexer lexer;
    Parser parser;
    ANode* root;

    ASSERT_SYSTEM(filecontext_init(&fctx, NULL));
    ASSERT_SYSTEM(stream_init_strRD(&stream, text, -1));
    ASSERT_SYSTEM(lexer_init(&lexer, &stream, &fctx));
    ASSERT_SYSTEM(parser_init(&parser, &lexer, &fctx));

    root = parser_exec(&parser);
    ASSERT_NONNULL(root);

    //lexer_deinit(&lexer);
    stream_close(&stream);
    filecontext_deinit(&fctx);
}

TEST(minimal)
{
    helper_string(
        "native int print(int n);\n"
        "void main()\n"
        "{\n"
        "    print(3);\n"
        "}\n"
    );
}
TEST(expression)
{
    helper_string(
        "#!/usr/bin/env coyote\n"
        "native int print(int);\n"
        "void main()\n"
        "{\n"
        "    print(3 + -2 * (5 / +2) ** 3 - 1.0e-1);\n"
        "}\n"
    );
}

void test_main(void)
{
    RUN_TEST(minimal);
    RUN_TEST(expression);
}

#include "../test-runner.c"
