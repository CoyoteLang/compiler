#!/bin/sh

file='lexical.txt'
prefix='TK_'
iprefix='TKI_'
aprefix='TKA_'

outdir="../src/generated"
outbase="$outdir/tkind"

mkdir -p "$outdir"
printf '' > "$outbase.h"
printf '' > "$outbase.c"

echo '#ifndef LEXER_TKIND_H_' >> "$outbase.h"
echo '#define LEXER_TKIND_H_' >> "$outbase.h"

awk '
BEGIN {
    print "typedef enum TokenKind {";
    print "    '"$prefix"'UNKNOWN,";
    print "    '"$prefix"'EOF,";
    n = 0;
}
/^ignore[ \t]+[A-Z]/ {
    printf "    %s%s,    //", iprefix, $2;
    for(i=3;i<=NF;i++)
        printf " %s", $i;
    print "";
    n++;
    next;
}
/^ast[ \t]+[A-Z]/ {
    printf "    %s%s,    //", aprefix, $2;
    for(i=3;i<=NF;i++)
        printf " %s", $i;
    print "";
    n++;
    next;
}
/^[A-Z]/ {
    printf "    %s%s,    //", prefix, $1;
    for(i=2;i<=NF;i++)
        printf " %s", $i;
    print "";
    n++;
}
END {
    printf "    %s_NUM = %s\n", prefix, n + 2;
    print "} TokenKind;"
    printf "extern const char* TokenKindNames[%s_NUM];\n", prefix;
}
' "prefix=$prefix" "iprefix=$iprefix" "aprefix=$aprefix" "$file" >> "$outbase.h"

echo '#endif /* LEXER_TKIND_H_ */' >> "$outbase.h"

echo '#include "'"$outbase.h"'"' >> "$outbase.c"

awk '
BEGIN {
    print "const char* TokenKindNames[] = {";
    print "    \"<unknown>\",";
    print "    \"<eof>\",";
}
/^ignore[ \t]+[A-Z]/ {
    print "    \"!" $2 "\",";
    next;
}
/^ast[ \t]+[A-Z]/ {
    print "    \"^" $2 "\",";
    next;
}
/^[A-Z]/ {
    print "    \"" $1 "\",";
}
END {
    print "};"
}
' "$file" >> "$outbase.c"
