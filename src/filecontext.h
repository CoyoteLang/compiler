#ifndef FILECONTEXT_H_
#define FILECONTEXT_H_

#include <util/table.h>

typedef struct FileContext
{
    char* fname;
    Table itab;
} FileContext;

FileContext* filecontext_init(FileContext* fctx, const char* fname);
void filecontext_deinit(FileContext* fctx);

#endif /* FILECONTEXT_H_ */
