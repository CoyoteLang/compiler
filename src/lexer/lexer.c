#include <lexer/lexer.h>

// TODO: replace isalnum() and friends
#include <ctype.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <util/unicode.h>

static int lex__raw_getc(Lexer* lex)
{
    int c = stream_getcT(lex->stream);
    if(c == '\n')
    {
        lex->pos.line++;
        lex->pos.col = 0;
    }
    else if(c != STREAM_EOF)
        lex->pos.col++;
    return c;
}

static void lex__advancec(Lexer* lex)
{
    lex->apos[lex->aidx] = lex->pos;
    int c = lex__raw_getc(lex);
    if(c != STREAM_EOF)
        lex->achr[lex->aidx] = c;
    else
        lex->anum--;
    lex->aidx = (lex->aidx + 1) & LEXER_LOOKMASK;
    lex->tok.range.tail = lex->pos;
}
static void lex__advancen(Lexer* lex, int num)
{
    assert(num >= 0);
    while(num--)
        lex__advancec(lex);
}
static int lex__peekc(Lexer* lex, int ahead)
{
    if(ahead >= lex->anum) return STREAM_EOF;
    return lex->achr[(lex->aidx + ahead) & LEXER_LOOKMASK];
}
int lex__getc(Lexer* lex)
{
    int c = lex__peekc(lex, 0);
    lex__advancec(lex);
    return c;
}

Lexer* lexer_init(Lexer* lex, Stream* stream, FileContext* fctx)
{
    if(!lex) return NULL;
    lex->fctx = fctx;
    lex->stream = stream;
    lex->tok.kind = TK_UNKNOWN;
    lex->pos.line = 0;
    lex->pos.col = 0;

    lex->aidx = 0;
    lex->anum = LEXER_LOOKAHEAD;

    int i;
    for(i = 0; i < LEXER_LOOKAHEAD; i++)
        lex__advancec(lex);

    return lex;
}

// TODO: Remove ASCII compiler assumption
static int chr_to_digit(int c, unsigned int base)
{
    switch(base)
    {
    case 16:
        if('A' <= c && c <= 'F')
            return c - 'A' + 10;
        if('a' <= c && c <= 'f')
            return c - 'a' + 10;
        if('0' <= c && c <= '9')
            return c - '0';
        return -1;
    case 10:
        if('0' <= c && c <= '9')
            return c - '0';
        return -1;
    case 8:
        if('0' <= c && c <= '7')
            return c - '0';
        return -1;
    case 2:
        if('0' <= c && c <= '1')
            return c - '0';
        return -1;
    default: assert(0);
    }
    return -1;
}
// TODO: handle overflow
static int64_t lex__handle_number_part(int* ndigits, Lexer* lex, unsigned int base)
{
    int64_t ret = 0;

    *ndigits = 0;

    int v;
    for(;;)
    {
        v = chr_to_digit(lex__peekc(lex, 0), base);
        if(v < 0) break;
        ret = ret * base + v;
        ++*ndigits;
        lex__advancen(lex, 1);
    }
    return ret;
}

// TODO: automatic conversion from 32-bit to 64-bit ints for too large numbers (or to unsigned if necessary)
static Token* lex__handle_number(Lexer* lex)
{
    Token* tok = &lex->tok;
    int c;

    int base = 10;
    bool isreal = false;
    int64_t ipart;
    int64_t fpart = 0;
    int flen = 0;
    int64_t exp = 1;
    int esign = 0;
    int ndigits;

    long double fnum;

    c = lex__peekc(lex, 0);
    if(c == '0')
    {
        switch(lex__peekc(lex, 1))
        {
        case 'X': case 'x': // 0[Xx]${DIG16}+ ${ISUFFIX}
            lex__advancen(lex, 2);
            base = 16;
            break;
        case 'Q': case 'q': // 0[Qq]${DIG8}+ ${ISUFFIX}
            lex__advancen(lex, 2);
            base = 8;
            break;
        case 'B': case 'b': // 0[Bb]${DIG2}+ ${ISUFFIX}
            lex__advancen(lex, 2);
            base = 2;
            break;
        default:
            break;
        }
    }
    ipart = lex__handle_number_part(&ndigits, lex, base);
    if(!ndigits)
        return NULL;

    if(base == 10)
    {
        // .[0-9]+
        if(lex__peekc(lex, 0) == '.')
        {
            c = lex__peekc(lex, 1);
            if(c != STREAM_EOF && isdigit(c))
            {
                isreal = true;
                lex__advancen(lex, 1); // skip the '.'
                fpart = lex__handle_number_part(&flen, lex, base);
                if(!flen)
                    return NULL;
            }
        }

        // [Ee]['-]?${DIG10}+
        c = lex__peekc(lex, 0);
        if(c == 'E' || c == 'e')
        {
            isreal = true;
            lex__advancen(lex, 1); // skip the 'E' or 'e'
            c = lex__peekc(lex, 0);
            if(c == '+')
            {
                esign = 1;
                lex__advancen(lex, 1);
            }
            else if(c == '-')
            {
                esign = -1;
                lex__advancen(lex, 1);
            }
            exp = lex__handle_number_part(&ndigits, lex, 10);
            if(!ndigits)
                return NULL;
            if(esign < 0)
                exp = -exp;
        }
    }

    COYbool isunsigned = COY_FALSE;
    Type* type = NULL;

    for(;;)
    {
        c = lex__peekc(lex, 0);
        if(c == STREAM_EOF || (!isalpha(c) && c != '_'))
            break;
        switch(c)
        {
        case 'F': case 'f':
            if(isunsigned || type || !isreal)
                return NULL; /* repeated width or had 'U' or is integer ... */
            type = &Tfloat;
            break;
        case 'D': case 'd':
            if(isunsigned || type || !isreal)
                return NULL; /* repeated width or had 'U' or is integer ... */
            type = &Tdouble;
            break;
        case 'U': case 'u':
            if(isunsigned || isreal)
                return NULL; /* repeated 'U' or is real, not integer */
            isunsigned = COY_TRUE;
            break;
        case 'L':
            if(type || isreal)
                return NULL; /* repeated 'L' or is real, not integer */
            type = &Tlong;
            break;
        default:
            return NULL; /* unknown specifier */
        }
        lex__advancen(lex, 1);
    }

    if(isreal)
    {
        tok->kind = TK_REAL;
        // default type for real numbers is `real`
        if(!type) type = &Treal;
        fnum = ipart + fpart * powl(10, -flen);
        if(esign)
            fnum *= powl(10, exp);
        tok->value.type = type;
        tok->value.v.r = fnum;
    }
    else
    {
        tok->kind = TK_INTEGER;
        // default type for integers is `int` (unless it's too big, but we adjust that later)
        if(!type) type = &Tint;
        if(isunsigned)
        {
            if((uint64_t)ipart > UINT32_MAX) type = &Tlong;
            tok->value.type = type->u.uinteger.width == Tint.u.uinteger.width ? &Tuint : &Tulong;
            tok->value.v.u = ipart;
        }
        else
        {
            if(ipart < INT32_MIN || INT32_MAX < ipart) type = &Tlong;
            tok->value.type = type;
            tok->value.v.i = ipart;
        }
    }

    return tok;
}
static Token* lex__handle_strcontent(Lexer* lex, int delim, bool rstring)
{
    Token* tok = &lex->tok;

    bool escape = false;

    int c = lex__peekc(lex, 0);
    assert(c == delim);
    lex__advancen(lex, 1);

    size_t strlen = 0;
    COYdchar* str = NULL;

    int i;
    int digit;
    COYchar val;
    COYwchar wval;
    COYdchar dval;
    int iend;
    COYchar* convstr;
    int convlen;

    for(;;)
    {
        c = lex__peekc(lex, 0);
        if(c == STREAM_EOF) /* unterminated character literal */
            return NULL;
        if(escape)
        {
            str = realloc(str, (strlen + 1) * sizeof(COYdchar));
            assert(str);
            switch(c)
            {
            case 'a': str[strlen++] = '\a'; break;
            case 'b': str[strlen++] = '\b'; break;
            case 'f': str[strlen++] = '\f'; break;
            case 'n': str[strlen++] = '\n'; break;
            case 'r': str[strlen++] = '\r'; break;
            case 't': str[strlen++] = '\t'; break;
            case 'v': str[strlen++] = '\v'; break;
            case '\\': str[strlen++] = '\\'; break;
            case '\'': str[strlen++] = '\''; break;
            case '"': str[strlen++] = '"'; break;
            case 'e': str[strlen++] = '\033'; break;
            case 'x':
                val = 0;
                for(i = 0; i < 2; i++)
                {
                    val <<= 4;
                    lex__advancen(lex, 1);
                    digit = chr_to_digit(lex__peekc(lex, 0), 16);
                    if(digit < 0)
                        return NULL; /* invalid hex literal */
                    val |= digit;
                }
                str[strlen++] = val;
                break;
            case 'u':
                wval = 0;
                for(i = 0; i < 4; i++)
                {
                    wval <<= 4;
                    lex__advancen(lex, 1);
                    digit = chr_to_digit(lex__peekc(lex, 0), 16);
                    if(digit < 0)
                        return NULL; /* invalid hex literal */
                    wval |= digit;
                }
                strlen += unicode_utf32from16(&str[strlen], &wval, 1);
                break;
            case 'U':
                dval = 0;
                for(i = 0; i < 8; i++)
                {
                    dval <<= 4;
                    lex__advancen(lex, 1);
                    digit = chr_to_digit(lex__peekc(lex, 0), 16);
                    if(digit < 0)
                        return NULL; /* invalid hex literal */
                    dval |= digit;
                }
                str[strlen++] = dval;
                break;
            case '0': case '1': case '2': case '3':
            case '4': case '5': case '6': case '7':
                val = chr_to_digit(lex__peekc(lex, 0), 8);
                if(val <= 3)
                    iend = 3;
                else
                    iend = 2;
                for(i = 1; i < iend; i++)
                {
                    lex__advancen(lex, 1);
                    digit = chr_to_digit(lex__peekc(lex, 0), 8);
                    if(digit < 0)
                        break; /* we permit invalid oct literals (and just exit early) */
                    val = (val << 3) | digit;
                }
                str[strlen++] = val;
                break;
            default:
                return NULL; /* invalid escape sequence */
            }
            escape = false;
        }
        else if(!rstring && c == '\\')
            escape = true;
        else if(c == delim)
        {
            if(rstring && lex__peekc(lex, 1) == delim)
            {
                lex__advancen(lex, 1);
                str = realloc(str, (strlen + 1) * sizeof(COYdchar));
                assert(str);
                str[strlen++] = c;
            }
            else
                break;
        }
        else
        {
            str = realloc(str, (strlen + 1) * sizeof(COYdchar));
            assert(str);
            str[strlen++] = c;
        }
        lex__advancen(lex, 1);
    }
    lex__advancen(lex, 1);

    Type* type = NULL;

    for(;;)
    {
        c = lex__peekc(lex, 0);
        if(c == STREAM_EOF || (!isalpha(c) && c != '_'))
            break;
        switch(c)
        {
        case 'c':
            if(type)
                return NULL; /* dual modifiers */
            type = &Tstring;
            break;
        case 'w':
            if(type)
                return NULL; /* dual modifiers */
            type = &Twstring;
            break;
        case 'd':
            if(type)
                return NULL; /* dual modifiers */
            type = &Tdstring;
            break;
        default:
            return NULL; /* invalid modifier */
        }
        lex__advancen(lex, 1);
    }

    if(!type) type = &TstringU;

    if(type == &Tstring)
    {
        convstr = malloc(strlen * 4 + 1);
        convlen = unicode_utf8from32(convstr, str, strlen);
        free(str);
        if(convlen < 0)
        {
            free(convstr);
            return NULL; /* invalid Unicode */
        }
        tok->value = value_mkstring(lex->fctx, convstr, convlen);
    }
    else // TODO: &Twstring, &Tcstring, &TstringU
        assert(0 && "TODO: strcontent &Twstring, &Tcstring, &TstringU");
    return tok;
}
static Token* lex__handle_char(Lexer* lex)
{
    lex->tok.kind = TK_CHAR;
    Token* tok = lex__handle_strcontent(lex, '\'', false);
    if(!tok) return NULL;
    if(value_strlen(&tok->value) != 1) return NULL; /* character literals must be of length == 1 */
    if(tok->value.type == &TstringU)
        assert(0 && "TODO: character literal 'x'");
    else if(tok->value.type == &Tstring)
        tok->value = value_mkchar(lex->fctx, tok->value.v.istr->ptr[0]);
    else if(tok->value.type == &Twstring)
        assert(0 && "TODO: character literal 'x'w");
        //tok->value = value_mkwchar(lex->fctx, tok->value.v.iwstr->ptr[0]);
    else if(tok->value.type == &Tdstring)
        assert(0 && "TODO: character literal 'x'd");
        //tok->value = value_mkdchar(lex->fctx, tok->value.v.idstr->ptr[0]);
    return tok;
}
static Token* lex__handle_string(Lexer* lex)
{
    lex->tok.kind = TK_STRING;

    int c = lex__peekc(lex, 0);
    if(c == 'r')
        lex__advancen(lex, 1);
    return lex__handle_strcontent(lex, '"', c == 'r');
}
static Token* lex__handle_xstring(Lexer* lex)
{
    assert(0); /* TODO */
    return NULL;
#if 0
    lex->tok.kind = TK_XSTRING;

    int c = lex__peekc(lex, 0);
    assert(c == 'x');
    lex__advancen(lex, 1);

    c = lex__peekc(lex, 0);
    assert(c == '"');
    lex__advancen(lex, 1);

    size_t nibblelen = 0;
    uint8_t* data = NULL;

    int val;

    for(;;)
    {
        c = lex__peekc(lex, 0);
        lex__advancen(lex, 1);
        if(c == '"')
            break;
        if(c == STREAM_EOF || !isxdigit(c) || !isspace(c))
            return NULL; /* invalid string content or unterminated string */
        if(isspace(c))
            continue;

        val = chr_to_digit(c, 16);
    }

    tok->value.width = 0;

    for(;;)
    {
        c = lex__peekc(lex, 0);
        if(c == STREAM_EOF || (!isalpha(c) && c != '_'))
            break;
        switch(c)
        {
        case 'c':
            if(tok->value.width)
                return NULL; /* dual modifiers */
            tok->value.width = 1;
        case 'w':
            if(tok->value.width)
                return NULL; /* dual modifiers */
            tok->value.width = 2;
            break;
        case 'd':
            if(tok->value.width)
                return NULL; /* dual modifiers */
            tok->value.width = 4;
            break;
        default:
            return NULL; /* invalid modifier */
        }
        lex__advancen(lex, 1);
    }
#endif
}
static Token* lex__handle_ident(Lexer* lex)
{
    Token* tok = &lex->tok;

    int c = lex__peekc(lex, 0);
    assert(c != STREAM_EOF && (isalpha(c) || c == '_'));

    size_t strlen = 0;
    char* str = NULL;

    for(;;)
    {
        c = lex__peekc(lex, 0);
        if(c == STREAM_EOF || (!isalnum(c) && c != '_'))
            break;
        lex__advancen(lex, 1);

        str = realloc(str, strlen + 2);
        assert(str);
        str[strlen++] = c;
    }
    str[strlen] = 0;

    if(0) {}
    else if(!strcmp(str, "in"))
        tok->kind = TK_IN;
    else if(!strcmp(str, "is"))
        tok->kind = TK_IS;
    else if(!strcmp(str, "class"))
        tok->kind = TK_CLASS;
    else if(!strcmp(str, "struct"))
        tok->kind = TK_STRUCT;
    else if(!strcmp(str, "interface"))
        tok->kind = TK_INTERFACE;
    else if(!strcmp(str, "delegate"))
        tok->kind = TK_DELEGATE;
    else if(!strcmp(str, "enum"))
        tok->kind = TK_ENUM;
    else if(!strcmp(str, "void") || !strcmp(str, "bool") || !strcmp(str, "char")
         || !strcmp(str, "byte") || !strcmp(str, "ubyte")
         || !strcmp(str, "int") || !strcmp(str, "uint")
         || !strcmp(str, "long") || !strcmp(str, "ulong")
         || !strcmp(str, "float") || !strcmp(str, "double") || !strcmp(str, "real"))
    {
        tok->kind = TK_TYPE;
        Type* type = type_get_by_name(str);
        assert(type && "Invalid type");
        tok->value = value_mktype(lex->fctx, type);
    }
    else if(!strcmp(str, "out"))
        tok->kind = TK_OUT;
    else if(!strcmp(str, "ref"))
        tok->kind = TK_REF;
    else if(!strcmp(str, "native"))
        tok->kind = TK_NATIVE;
    else if(!strcmp(str, "final"))
        tok->kind = TK_FINAL;
    else if(!strcmp(str, "abstract"))
        tok->kind = TK_ABSTRACT;
    else if(!strcmp(str, "static"))
        tok->kind = TK_STATIC;
    else if(!strcmp(str, "property"))
        tok->kind = TK_PROPERTY;
    else if(!strcmp(str, "override"))
        tok->kind = TK_OVERRIDE;
    else if(!strcmp(str, "assert"))
        tok->kind = TK_ASSERT;
    else if(!strcmp(str, "if"))
        tok->kind = TK_IF;
    else if(!strcmp(str, "else"))
        tok->kind = TK_ELSE;
    else if(!strcmp(str, "for"))
        tok->kind = TK_FOR;
    else if(!strcmp(str, "foreach"))
        tok->kind = TK_FOREACH;
    else if(!strcmp(str, "foreach_r"))
        tok->kind = TK_FOREACH_R;
    else if(!strcmp(str, "while"))
        tok->kind = TK_WHILE;
    else if(!strcmp(str, "do"))
        tok->kind = TK_DO;
    else if(!strcmp(str, "switch"))
        tok->kind = TK_SWITCH;
    else if(!strcmp(str, "case"))
        tok->kind = TK_CASE;
    else if(!strcmp(str, "break"))
        tok->kind = TK_BREAK;
    else if(!strcmp(str, "continue"))
        tok->kind = TK_CONTINUE;
    else if(!strcmp(str, "goto"))
        tok->kind = TK_GOTO;
    else if(!strcmp(str, "return"))
        tok->kind = TK_RETURN;
    else if(!strcmp(str, "module"))
        tok->kind = TK_MODULE;
    else if(!strcmp(str, "default"))
        tok->kind = TK_DEFAULT;
    else if(!strcmp(str, "this"))
        tok->kind = TK_THIS;
    else if(!strcmp(str, "super"))
        tok->kind = TK_SUPER;
    else if(!strcmp(str, "null"))
        tok->kind = TK_NULL;
    else if(!strcmp(str, "true"))
        tok->kind = TK_TRUE;
    else if(!strcmp(str, "false"))
        tok->kind = TK_FALSE;
    else if(!strcmp(str, "pragma"))
        tok->kind = TK_PRAGMA;
    else if(!strcmp(str, "wchar")|| !strcmp(str, "dchar")
         || !strcmp(str, "cent") || !strcmp(str, "ucent")
         || !strcmp(str, "ifloat") || !strcmp(str, "idouble") || !strcmp(str, "ireal") || !strcmp(str, "cfloat") || !strcmp(str, "cdouble") || !strcmp(str, "creal")
         || !strcmp(str, "asm")
         || !strcmp(str, "label")
         || !strcmp(str, "public") || !strcmp(str, "private") || !strcmp(str, "protected") || !strcmp(str, "package")
         || !strcmp(str, "typeof") || !strcmp(str, "sizeof")
         || !strcmp(str, "dispose")
         || !strcmp(str, "throw") || !strcmp(str, "try") || !strcmp(str, "catch") || !strcmp(str, "finally") || !strcmp(str, "defer")
         || !strcmp(str, "synchronized")  || !strcmp(str, "atomic")  || !strcmp(str, "locked")
         || !strcmp(str, "align")
         || !strcmp(str, "unittest"))
        tok->kind = TK_RESERVED;
    else
    {
        tok->kind = TK_IDENT;
        tok->value = value_mksymbol(lex->fctx, (const COYchar*)str, strlen);
    }

    free(str);

    return tok;
}

Token* lexer_next(Lexer* lex, bool opt)
{
    Token* tok = &lex->tok;

    int c;
start:
    if(lex->apos[lex->aidx].line == 0 && lex->apos[lex->aidx].col == 0)
    {
        // skip UTF-8 BOM
        if(lex__peekc(lex, 0) == 0xEF && lex__peekc(lex, 1) == 0xBB && lex__peekc(lex, 2) == 0xBF)
            lex__advancen(lex, 3);
        // skip shebang
        if(lex__peekc(lex, 0) == '#' && lex__peekc(lex, 1) == '!')
        {
            lex__advancen(lex, 2);
            for(;;)
            {
                c = lex__peekc(lex, 0);
                if(c == STREAM_EOF || c == '\n')
                    break;
                lex__advancen(lex, 1);
            }
        }
    }
    tok->range.head = lex->pos;
    c = lex__peekc(lex, 0);
    switch(c)
    {
    case STREAM_EOF:
        tok->kind = TK_EOF;
        break;
    case '=':   // = ==
        c = lex__peekc(lex, 1);
        switch(c)
        {
        case '=':   // ==
            lex__advancen(lex, 2);
            tok->kind = TK_EQEQ;
            break;
        default:    // =
            lex__advancen(lex, 1);
            tok->kind = TK_EQ;
            break;
        }
        break;
    case '+':   // += ++ +
        c = lex__peekc(lex, 1);
        switch(c)
        {
        case '=':   // +=
            lex__advancen(lex, 2);
            tok->kind = TK_PLUSEQ;
            break;
        case '+':   // ++
            lex__advancen(lex, 2);
            tok->kind = TK_PLUSPLUS;
            break;
        default:    // +
            lex__advancen(lex, 1);
            tok->kind = TK_PLUS;
            break;
        }
        break;
    case '-':   // -= -- -
        c = lex__peekc(lex, 1);
        switch(c)
        {
        case '=':   // -=
            lex__advancen(lex, 2);
            tok->kind = TK_MINUSEQ;
            break;
        case '-':   // --
            lex__advancen(lex, 2);
            tok->kind = TK_MINUSMINUS;
            break;
        default:    // -
            lex__advancen(lex, 1);
            tok->kind = TK_MINUS;
            break;
        }
        break;
    case '*':   // *= **= * **
        c = lex__peekc(lex, 1);
        switch(c)
        {
        case '=':   // *=
            lex__advancen(lex, 2);
            tok->kind = TK_STAREQ;
            break;
        case '*':   // **= **
            c = lex__peekc(lex, 2);
            switch(c)
            {
            case '=':   // **=
                lex__advancen(lex, 3);
                tok->kind = TK_STARSTAREQ;
                break;
            default:    // **
                lex__advancen(lex, 2);
                tok->kind = TK_STARSTAR;
                break;
            }
            break;
        default:    // *
            lex__advancen(lex, 1);
            tok->kind = TK_STAR;
            break;
        }
        break;
    case '/':   // /= / // /*
        c = lex__peekc(lex, 1);
        switch(c)
        {
        case '=':   // /=
            lex__advancen(lex, 2);
            tok->kind = TK_SLASHEQ;
            break;
        case '/':   // //
            lex__advancen(lex, 2);
            tok->kind = TKI_SLCOMMENT;
            for(;;)
            {
                c = lex__peekc(lex, 0);
                if(c == STREAM_EOF || c == '\n')
                    break;
                lex__advancen(lex, 1);
            }
            if(!opt)
                goto start;
            break;
        case '*':   // /*
            lex__advancen(lex, 2);
            tok->kind = TKI_MLCOMMENT;
            for(;;)
            {
                c = lex__peekc(lex, 0);
                if(c == STREAM_EOF || (c == '*' && lex__peekc(lex, 1) == '/'))
                    break;
                lex__advancen(lex, 1);
            }
            lex__advancen(lex, 2);
            if(!opt)
                goto start;
            break;
        default:    // /
            lex__advancen(lex, 1);
            tok->kind = TK_SLASH;
            break;
        }
        break;
    case '%':   // %= %%= % %%
        c = lex__peekc(lex, 1);
        switch(c)
        {
        case '=':   // %=
            lex__advancen(lex, 2);
            tok->kind = TK_PERCENTEQ;
            break;
        case '%':   // %%= %%
            c = lex__peekc(lex, 2);
            switch(c)
            {
            case '=':   // %%=
                lex__advancen(lex, 3);
                tok->kind = TK_PERCENTPERCENTEQ;
                break;
            default:    // %%
                lex__advancen(lex, 2);
                tok->kind = TK_PERCENTPERCENT;
                break;
            }
            break;
        default:    // %
            lex__advancen(lex, 1);
            tok->kind = TK_PERCENT;
            break;
        }
        break;
    case '^':   // ^= ^
        c = lex__peekc(lex, 1);
        switch(c)
        {
        case '=':   // ^=
            lex__advancen(lex, 2);
            tok->kind = TK_CARETEQ;
            break;
        default:    // ^
            lex__advancen(lex, 1);
            tok->kind = TK_CARET;
            break;
        }
        break;
    case '&':   // &= & &&
        c = lex__peekc(lex, 1);
        switch(c)
        {
        case '=':   // &=
            lex__advancen(lex, 2);
            tok->kind = TK_AMPEQ;
            break;
        case '&':   // &&
            lex__advancen(lex, 2);
            tok->kind = TK_AMPAMP;
            break;
        default:    // &
            lex__advancen(lex, 1);
            tok->kind = TK_AMP;
            break;
        }
        break;
    case '|':   // |= |>>= | |>> ||
        c = lex__peekc(lex, 1);
        switch(c)
        {
        case '=':   // |=
            lex__advancen(lex, 2);
            tok->kind = TK_PIPEEQ;
            break;
        case '>':   // |>>= |>>
            c = lex__peekc(lex, 2);
            switch(c)
            {
            case '>':   // |>>= |>>
                c = lex__peekc(lex, 3);
                switch(c)
                {
                case '=':   // |>>=
                    lex__advancen(lex, 4);
                    tok->kind = TK_PIPEGTGTEQ;
                    break;
                default:    // |>>
                    lex__advancen(lex, 3);
                    tok->kind = TK_PIPEGTGT;
                    break;
                }
                break;
            default:    // two operators: '|' and start of '>', so we only shift for '|'
                lex__advancen(lex, 1);
                tok->kind = TK_PIPE;
                break;
            }
            break;
        case '|':   // ||
            lex__advancen(lex, 2);
            tok->kind = TK_PIPEPIPE;
            break;
        default:    // |
            lex__advancen(lex, 1);
            tok->kind = TK_PIPE;
            break;
        }
        break;
    case '<':   // <<= <<|= << <<| <=> < <=
        c = lex__peekc(lex, 1);
        switch(c)
        {
        case '<':   // <<= <<|= << <<|
            c = lex__peekc(lex, 2);
            switch(c)
            {
            case '=':   // <<=
                lex__advancen(lex, 3);
                tok->kind = TK_LTLTEQ;
                break;
            case '|':   // <<|= <<|
                c = lex__peekc(lex, 3);
                switch(c)
                {
                case '=':   // <<|=
                    lex__advancen(lex, 4);
                    tok->kind = TK_LTLTPIPEEQ;
                    break;
                default:    // <<|
                    lex__advancen(lex, 3);
                    tok->kind = TK_LTLTPIPE;
                    break;
                }
                break;
            default:    // <<
                lex__advancen(lex, 2);
                tok->kind = TK_LTLT;
                break;
            }
            break;
        case '=':   // <=> <=
            c = lex__peekc(lex, 2);
            switch(c)
            {
            case '>':   // <=>
                lex__advancen(lex, 3);
                tok->kind = TK_LTEQGT;
                break;
            default:    // <=
                lex__advancen(lex, 2);
                tok->kind = TK_LTEQ;
                break;
            }
            break;
        default:    // <
            lex__advancen(lex, 1);
            tok->kind = TK_LT;
            break;
        }
        break;
    case '>':   // >>= >>>= >> >>> > >=
        c = lex__peekc(lex, 1);
        switch(c)
        {
        case '>':   // >>= >>>= >> >>>
            c = lex__peekc(lex, 2);
            switch(c)
            {
            case '=':   // >>=
                lex__advancen(lex, 3);
                tok->kind = TK_GTGTEQ;
                break;
            case '>':   // >>>= >>>
                c = lex__peekc(lex, 3);
                switch(c)
                {
                case '=':   // >>>=
                    lex__advancen(lex, 4);
                    tok->kind = TK_GTGTGTEQ;
                    break;
                default:    // >>>
                    lex__advancen(lex, 3);
                    tok->kind = TK_GTGTGT;
                    break;
                }
                break;
            default:    // >>
                lex__advancen(lex, 2);
                tok->kind = TK_GTGT;
                break;
            }
            break;
        case '=':   // >=
            lex__advancen(lex, 2);
            tok->kind = TK_GTEQ;
            break;
        default:    // >
            lex__advancen(lex, 1);
            tok->kind = TK_GT;
            break;
        }
        break;
    case '~':   // ~= ~
        c = lex__peekc(lex, 1);
        switch(c)
        {
        case '=':   // ~=
            lex__advancen(lex, 2);
            tok->kind = TK_TILDEEQ;
            break;
        default:    // ~
            lex__advancen(lex, 1);
            tok->kind = TK_TILDE;
            break;
        }
        break;
    case '!':   // !in !is != !
        c = lex__peekc(lex, 1);
        switch(c)
        {
        case 'i':   // !in !is
            c = lex__peekc(lex, 2);
            switch(c)
            {
            case 'n':   // !in or !<ident>
                c = lex__peekc(lex, 3);
                if(c != STREAM_EOF && (isalnum(c) || c == '_')) // !<ident>
                {
                    lex__advancen(lex, 1);
                    tok->kind = TK_EXCLAIM;
                }
                else // !in
                {
                    lex__advancen(lex, 3);
                    tok->kind = TK_EXCLAIMIN;
                }
                break;
            case 's':   // !is or !<ident>
                c = lex__peekc(lex, 3);
                if(c != STREAM_EOF && (isalnum(c) || c == '_')) // !<ident>
                {
                    lex__advancen(lex, 1);
                    tok->kind = TK_EXCLAIM;
                }
                else // !is
                {
                    lex__advancen(lex, 3);
                    tok->kind = TK_EXCLAIMIS;
                }
                break;
            default:    // !<ident>
                lex__advancen(lex, 1);
                tok->kind = TK_EXCLAIM;
                break;
            }
            break;
        case '=':   // !=
            lex__advancen(lex, 2);
            tok->kind = TK_EXCLAIMEQ;
            break;
        default:    // !
            lex__advancen(lex, 1);
            tok->kind = TK_EXCLAIM;
            break;
        }
        break;
    case '@':   // @
        lex__advancen(lex, 1);
        tok->kind = TK_AT;
        break;
    case '$':   // $
        lex__advancen(lex, 1);
        tok->kind = TK_DOLLAR;
        break;
    case ',':   // ,
        lex__advancen(lex, 1);
        tok->kind = TK_COMMA;
        break;
    case ';':   // ;
        lex__advancen(lex, 1);
        tok->kind = TK_SCOLON;
        break;
    case ':':   // :
        lex__advancen(lex, 1);
        tok->kind = TK_COLON;
        break;
    case '?':   // \?
        lex__advancen(lex, 1);
        tok->kind = TK_QMARK;
        break;
    case '.':   // . .. ...
        c = lex__peekc(lex, 1);
        switch(c)
        {
        case '.':   // .. ...
            c = lex__peekc(lex, 2);
            switch(c)
            {
            case '.':   // ...
                lex__advancen(lex, 3);
                tok->kind = TK_ELLIPSIS;
                break;
            default:    // ..
                lex__advancen(lex, 2);
                tok->kind = TK_DOTDOT;
                break;
            }
            break;
        default:
            lex__advancen(lex, 1);
            tok->kind = TK_DOT;
            break;
        }
        break;
    case '(':   // (
        lex__advancen(lex, 1);
        tok->kind = TK_LPAREN;
        break;
    case ')':   // )
        lex__advancen(lex, 1);
        tok->kind = TK_RPAREN;
        break;
    case '[':   // [
        lex__advancen(lex, 1);
        tok->kind = TK_LARRAY;
        break;
    case ']':   // ]
        lex__advancen(lex, 1);
        tok->kind = TK_RARRAY;
        break;
    case '{':   // {
        lex__advancen(lex, 1);
        tok->kind = TK_LBRACE;
        break;
    case '}':   // }
        lex__advancen(lex, 1);
        tok->kind = TK_RBRACE;
        break;
    case '0': case '1': case '2': case '3': case '4':
    case '5': case '6': case '7': case '8': case '9':
        tok = lex__handle_number(lex);
        break;
    case '\'':
        tok = lex__handle_char(lex);
        break;
    case '"':
        tok = lex__handle_string(lex);
        break;
    case ' ': case '\t': case '\v':
    case '\f': case '\r': case '\n':
        lex__advancen(lex, 1);
        tok->kind = TKI_WSPACE;
        for(;;)
        {
            c = lex__peekc(lex, 0);
            if(c == STREAM_EOF || !isspace(c))
                break;
            lex__advancen(lex, 1);
        }
        if(!opt)
            goto start;
        break;
    case 'r':
        c = lex__peekc(lex, 1);
        if(c == '"')
            tok = lex__handle_string(lex);
        else
            tok = lex__handle_ident(lex);
        break;
    case 'x':
        if(c == '"')
            tok = lex__handle_xstring(lex);
        else
            tok = lex__handle_ident(lex);
        break;
    default:
        if(isalpha(c) || c == '_')
            tok = lex__handle_ident(lex);
        else
            return NULL; /* error */
        break;
    }
    return tok;
}

Token* lexer_next_all(COYusize* len, Lexer* lex, bool opt)
{
    COYusize mem = LEXER_TOKENS_GROW;
    COYusize ntokens = 0;
    Token* tokens = malloc(mem * sizeof(Token));
    for(;;)
    {
        Token token = *lexer_next(lex, opt);
        if(ntokens == mem)
        {
            mem += LEXER_TOKENS_GROW;
            tokens = realloc(tokens, mem * sizeof(Token));
        }
        tokens[ntokens++] = token;
        if(token.kind == TK_UNKNOWN || token.kind == TK_EOF) break;
    }
    tokens = realloc(tokens, ntokens * sizeof(Token));
    if(len) *len = ntokens;
    return tokens;
}
