#ifndef LEXER_LEXER_H_
#define LEXER_LEXER_H_

#include <util/stream.h>
#include <lexer/token.h>

/* WARNING: Must be a power of 2! Should be the smallest such number >= longest operator. */
#define LEXER_LOOKAHEAD 4
#define LEXER_LOOKMASK  (LEXER_LOOKAHEAD - 1)

#define LEXER_TOKENS_GROW  512

typedef struct Lexer
{
    struct FileContext* fctx;
    Stream* stream;
    Token tok;
    FilePos pos;

    uint8_t aidx, anum;
    unsigned char achr[LEXER_LOOKAHEAD];
    FilePos apos[LEXER_LOOKAHEAD];
} Lexer;

Lexer* lexer_init(Lexer* lex, Stream* stream, struct FileContext* fctx);
Token* lexer_next(Lexer* lex, bool opt);

Token* lexer_next_all(COYusize* len, Lexer* lex, bool opt);

#endif /* LEXER_LEXER_H_ */
