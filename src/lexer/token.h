#ifndef LEXER_TOKEN_H_
#define LEXER_TOKEN_H_

#include <util/types.h>
#include <generated/tkind.h>
#include <util/filepos.h>
#include <util/value.h>

typedef struct Token
{
    TokenKind kind;
    FileRange range;
    Value value;
} Token;

#endif /* LEXER_TOKEN_H_ */
