#ifndef UTIL_TABLE_H_
#define UTIL_TABLE_H_

#include <util/types.h>

struct IString;

typedef struct TableEntry
{
    struct TableEntry* next;
    struct IString* key;
    void* val;
} TableEntry;

#define TABLE_INIT_LEN  512
typedef struct Table
{
    size_t len;
    TableEntry** ents;
} Table;

Table* table_init(Table* tab);
void table_deinit(Table* tab);

TableEntry* table_getents(Table* tab, const COYchar* str, int len);
TableEntry* table_getenti(Table* tab, struct IString* istr);

void* table_geti(Table* tab, struct IString* istr);
TableEntry* table_puts(Table* tab, const COYchar* str, int len, void* val);
TableEntry* table_puti(Table* tab, struct IString* istr, void* val);

#endif /* UTIL_TABLE_H_ */
