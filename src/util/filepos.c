#include <util/filepos.h>

int fpos_cmp(FilePos a, FilePos b)
{
    if(a.line != b.line) return (int)a.line - (int)b.line;
    return (int)a.col - (int)b.col;
}
FilePos fpos_min(FilePos a, FilePos b)
{
    return fpos_cmp(a, b) < 0 ? a : b;
}
FilePos fpos_max(FilePos a, FilePos b)
{
    return fpos_cmp(a, b) > 0 ? a : b;
}

FileRange frange_bounds(FileRange a, FileRange b)
{
    FileRange r;
    r.head = fpos_min(a.head, b.head);
    r.tail = fpos_max(a.tail, b.tail);
    return r;
}
