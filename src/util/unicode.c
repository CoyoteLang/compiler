#include <util/unicode.h>

static size_t strlen16(const COYwchar* str)
{
    size_t len = 0;
    while(str[len])
        len++;
    return len;
}
static size_t strlen32(const COYdchar* str)
{
    size_t len = 0;
    while(str[len])
        len++;
    return len;
}

int unicode_utf8from32(COYchar* dst, const COYdchar* src, int slen)
{
    if(slen < 0) slen = strlen32(src);

    int dlen = 0;
    int i;
    for(i = 0; i < slen; i++)
    {
        if(src[i] <= 0x7F) /* 1 octet */
            dst[dlen++] = src[i];
        else if(src[i] <= 0x07FF) /* 2 octets */
        {
            dst[dlen++] = 0xC0 | (src[i] >> 6);
            dst[dlen++] = 0x80 | (src[i] & 0x3F);
        }
        else if(src[i] <= 0xFFFF) /* 3 octets */
        {
            dst[dlen++] = 0xE0 | (src[i] >> 12);
            dst[dlen++] = 0x80 | ((src[i] >> 6) & 0x3F);
            dst[dlen++] = 0x80 | (src[i] & 0x3F);
        }
        else if(src[i] <= 0x10FFFF) /* 4 octets */
        {
            dst[dlen++] = 0xF0 | (src[i] >> 18);
            dst[dlen++] = 0x80 | ((src[i] >> 12) & 0x3F);
            dst[dlen++] = 0x80 | ((src[i] >> 6) & 0x3F);
            dst[dlen++] = 0x80 | (src[i] & 0x3F);
        }
    }
    return dlen;
}
int unicode_utf32from16(COYdchar* dst, const COYwchar* src, int slen)
{
    if(slen < 0) slen = strlen16(src);

    int dlen = 0;
    int i = 0;
    while(i < slen)
    {
        if(0xD800 <= src[i] && src[i] <= 0xDBFF)
        {
            if(i + 1 >= slen)
                return -1; /* unterminated sequence at src[i] */
            dst[dlen] = (src[i++] & 0x3FF) << 10;
            dst[dlen] |= src[i++] & 0x3FF;
            dst[dlen] += 0x10000;
        }
        else if(src[i] < 0xD800 || 0xDFFF < src[i])
            dst[dlen] = src[i++];
        else return -1; /* invalid sequence at src[i] */

        dlen++;
    }
    return dlen;
}
