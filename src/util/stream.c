#include <util/stream.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>

static size_t cb_file_read(void* data, void* ptr, size_t len)
{
    return fread(ptr, 1, len, data);
}
static bool cb_file_close(void* data)
{
    return !fclose(data);
}

typedef struct MemInfoRD
{
    const char* mem;
    size_t len;
    size_t pos;
} MemInfoRD;
static size_t cb_mem_read(void* data, void* ptr, size_t len)
{
    MemInfoRD* info = data;
    if(info->pos + len > info->len)
        len = info->len - info->pos;
    memcpy(ptr, info->mem + info->pos, len);
    info->pos += len;
    return len;
}
static bool cb_mem_close(void* data)
{
    free(data);
    return true;
}

Stream* stream_init_fnameRD(Stream* stream, const char* fname)
{
    if(!stream) return NULL;

    FILE* file = fopen(fname, "rb");
    if(!file) return NULL;

    stream->read = cb_file_read;
    stream->close = cb_file_close;
    stream->data = file;
    stream->ok = true;
    stream->unget = STREAM_EOF;

    return stream;
}
Stream* stream_init_memRD(Stream* stream, const void* mem, size_t len)
{
    if(!stream) return NULL;

    MemInfoRD* info = malloc(sizeof(MemInfoRD));
    assert(info);

    info->mem = mem;
    info->len = len;
    info->pos = 0;

    stream->read = cb_mem_read;
    stream->close = cb_mem_close;
    stream->data = info;
    stream->ok = true;
    stream->unget = STREAM_EOF;

    return stream;
}
Stream* stream_init_strRD(Stream* stream, const char* str, int len)
{
    return stream_init_memRD(stream, str, len >= 0 ? len : strlen(str));
}

size_t stream_read(Stream* stream, void* ptr, size_t len)
{
    size_t rlen = stream->read ? stream->read(stream->data, ptr, len) : 0;
    if(rlen != len)
        stream->ok = false;
    return rlen;
}

int stream_getc(Stream* stream)
{
    unsigned char cc;
    if(stream->unget != STREAM_EOF)
    {
        cc = stream->unget;
        stream->unget = STREAM_EOF;
    }
    else if(stream_read(stream, &cc, sizeof(cc)) != sizeof(cc))
        return STREAM_EOF;
    return cc;
}
void stream_ungetc(Stream* stream, int c)
{
    assert(stream->unget == STREAM_EOF);
    stream->unget = c;
}
int stream_getcT(Stream* stream)
{
    int c = stream_getc(stream);
    if(c == '\r')
    {
        int cc = stream_getc(stream);
        if(cc != '\n')
            stream_ungetc(stream, cc);
        c = '\n';
    }
    return c;
}

bool stream_close(Stream* stream)
{
    if(!stream) return true;
    return stream->close ? stream->close(stream->data) : true;
}

bool stream_isok(Stream* stream)
{
    return stream->ok;
}
