#include <util/table.h>
#include <util/istring.h>

#include <stdlib.h>
#include <string.h>

Table* table_init(Table* tab)
{
    if(!tab) return NULL;
    tab->len = TABLE_INIT_LEN;
    tab->ents = malloc(tab->len * sizeof(TableEntry*));
    size_t i;
    for(i = 0; i < tab->len; i++)
        tab->ents[i] = NULL;
    return tab;
}
void table_deinit(Table* tab)
{
    if(!tab) return;

    TableEntry* ent;
    TableEntry* nent;
    size_t i;
    for(i = 0; i < tab->len; i++)
    {
        for(ent = tab->ents[i]; ent; ent = nent)
        {
            nent = ent->next;
            free(ent);
        }
    }
    free(tab->ents);
}

static TableEntry* table__getents_hash(Table* tab, const COYchar* str, int len, COYhash hash)
{
    if(len < 0) len = strlen((const char*)str);
    TableEntry* ent;
    for(ent = tab->ents[hash & (tab->len - 1)]; ent; ent = ent->next)
        if(ent->key->hash == hash && ent->key->len == len && !memcmp(ent->key->ptr, str, len))
            return ent;
    return NULL;
}

TableEntry* table_getents(Table* tab, const COYchar* str, int len)
{
    if(len < 0) len = strlen((const char*)str);
    return table__getents_hash(tab, str, len, istring_hash(str, len));
}
TableEntry* table_getenti(Table* tab, IString* istr)
{
    TableEntry* ent;
    for(ent = tab->ents[istr->hash & (tab->len - 1)]; ent; ent = ent->next)
        if(ent->key == istr)
            return ent;
    return NULL;
}

void* table_geti(Table* tab, IString* istr)
{
    TableEntry* ent = table_getenti(tab, istr);
    return ent ? ent->val : NULL;
}
TableEntry* table_puts(Table* tab, const COYchar* str, int len, void* val)
{
    if(len < 0) len = strlen((const char*)str);
    COYhash hash = istring_hash(str, len);
    TableEntry** cent;
    TableEntry* ent = table__getents_hash(tab, str, len, hash);
    if(!ent)
    {
        cent = &tab->ents[hash & (tab->len - 1)];

        ent = malloc(sizeof(TableEntry));
        ent->next = *cent;
        *cent = ent;

        ent->key = malloc(sizeof(IString) + len); /* actually allocates len+1 */
        ent->key->hash = hash;
        ent->key->len = len;
        memcpy(ent->key->ptr, str, len);
        ent->key->ptr[len] = 0;
    }
    ent->val = val;
    return ent;
}
TableEntry* table_puti(Table* tab, IString* istr, void* val)
{
    TableEntry** cent;
    TableEntry* ent = table_getenti(tab, istr);
    if(!ent)
    {
        cent = &tab->ents[istr->hash & (tab->len - 1)];

        ent = malloc(sizeof(TableEntry));
        ent->next = *cent;
        *cent = ent;

        ent->key = istr;
    }
    ent->val = val;
    return ent;
}
