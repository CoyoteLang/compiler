#ifndef UTIL_STREAM_H_
#define UTIL_STREAM_H_

#include <util/types.h>

#define STREAM_EOF -1

typedef struct Stream
{
    size_t (*read)(void* data, void* ptr, size_t len);
    bool (*close)(void* data);
    void* data;
    bool ok;
    int unget;
} Stream;

Stream* stream_init_fnameRD(Stream* stream, const char* fname);
Stream* stream_init_memRD(Stream* stream, const void* mem, size_t len);
Stream* stream_init_strRD(Stream* stream, const char* str, int len);

size_t stream_read(Stream* stream, void* ptr, size_t len);

int stream_getc(Stream* stream);
void stream_ungetc(Stream* stream, int c);
int stream_getcT(Stream* stream);

bool stream_close(Stream* stream);

bool stream_isok(Stream* stream);

#endif /* UTIL_STREAM_H_ */
