#ifndef UTIL_TYPES_H_
#define UTIL_TYPES_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

typedef uint8_t COYbool;
typedef int8_t COYbyte;
typedef uint8_t COYubyte;
typedef int16_t COYshort;
typedef uint16_t COYushort;
typedef int32_t COYint;
typedef uint32_t COYuint;
typedef int64_t COYlong;
typedef uint64_t COYulong;

typedef uint8_t COYchar;
typedef uint16_t COYwchar;
typedef uint32_t COYdchar;

typedef float COYfloat;
typedef double COYdouble;
typedef /* long */ double COYreal;

/* TODO: Make size_t? */
typedef uint32_t COYhash;
typedef uintptr_t COYuintptr;

typedef size_t COYusize;

#define COY_TRUE 1
#define COY_FALSE 0

#endif /* UTIL_TYPES_H_ */
