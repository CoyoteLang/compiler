#ifndef UTIL_CHINTS_H_
#define UTIL_CHINTS_H_

#ifdef __GNUC__
#define COY_HINT_DEPRECATED __attribute__((deprecated))
#elif defined(_MSC_VER)
#define COY_HINT_DEPRECATED __declspec(deprecated)
#else
#define COY_HINT_DEPRECATED
#endif

#endif /* UTIL_CHINTS_H_ */
