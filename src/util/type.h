#ifndef UTIL_TYPE_H_
#define UTIL_TYPE_H_

#include <util/types.h>
#include <util/istring.h>

typedef enum TypeFamily TypeFamily;
typedef enum TypeComplex TypeComplex;
typedef struct Type Type;

enum TypeFamily
{
    TF_VOID,
    TF_BOOLEAN,
    TF_INTEGER,
    TF_REAL,
    TF_CHARACTER,

    TF_STRUCT,
    TF_CLASS,
    TF_ARRAY,
    TF_STRING,// TODO: REMOVE EVENTUALLY! (replace with immutable char[])
    TF_DICT,
    TF_SET,
    TF_DELEGATE,
    TF_OPTION,

    TF_MIXED,

    // internal
    TF_TYPE,
};
enum TypeComplex
{
    COMPLEX_REAL,
    COMPLEX_IMAG,
    COMPLEX_COMPLEX,
    COMPLEX_QUATERNION,
};
struct Type
{
    TypeFamily family;
    union
    {
        struct { COYubyte dimx; COYubyte dimy; } uboolean;
        struct { COYubyte dimx; COYubyte dimy; TypeComplex com; COYubyte width; COYbool is_signed; } uinteger;
        struct { COYubyte dimx; COYubyte dimy; TypeComplex com; COYubyte width; } ureal;
        struct { COYubyte width; } ucharacter;
        struct { IString* name; COYuint nfields; Type** fields; } ustruct;
        struct { IString* name; COYuint ninherits; Type** inherits; COYuint nfields; Type** fields; } uclass;
        struct { Type* val; } uarray;
        struct { Type* val; } ustring;
        struct { Type* key; Type* val; } udict;
        struct { Type* key; } uset;
        struct { Type* ret; COYuint nparams; Type** params; COYbool has_varargs; } udelegate;
        struct { Type* base; } uoption;
    } u;
};

extern Type Tvoid;
extern Type Tbool;
extern Type Tbyte, Tubyte;
extern Type Tshort, Tushort;
extern Type Tint, Tuint;
extern Type Tlong, Tulong;
//Type Tcent, Tucent;
extern Type Tfloat, Tdouble, Treal;
extern Type Tchar, Twchar, Tdchar;

extern Type Tstring, Twstring, Tdstring;

extern Type Tmixed;

// unknowns (special types for the compiler)
extern Type TstringU;

// internals
extern Type Ttype;

Type* type_get_by_name(const char* name);

COYusize type_sizeof(const Type* type);
COYbool type_isnum(const Type* type);
COYbool type_isref(const Type* type);

void type_dumpDBG(const Type* type);

#endif /* UTIL_TYPE_H_ */
