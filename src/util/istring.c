#include <util/istring.h>

#include <string.h>

COYhash hash_djb2(const COYchar *str, size_t len)
{
    COYhash hash = 5381;
    size_t i;
    for(i = 0; i < len; i++)
        hash = ((hash << 5) + hash) + str[i];
    return hash;
}

COYhash istring_hash(const COYchar* str, int len)
{
    if(len < 0) len = strlen((const char*)str);
    return hash_djb2(str, len);
}

IString* istring_intern(Table* itab, const COYchar* str, int len)
{
    TableEntry* entry = table_puts(itab, str, len, NULL);
    entry->val = entry->key; /* for interning, it's actually a set */
    return entry->key;
}
