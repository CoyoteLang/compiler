#ifndef UTIL_ISTRING_H_
#define UTIL_ISTRING_H_

#include <util/types.h>
#include <util/table.h>

typedef struct IString
{
    COYhash hash;
    size_t len;
    COYchar ptr[1];
} IString;

COYhash istring_hash(const COYchar* str, int len);

IString* istring_intern(Table* itab, const COYchar* str, int len);

#endif /* UTIL_ISTRING_H_ */
