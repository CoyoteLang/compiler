#include <util/os/semaphore.h>

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <limits.h>
#else
#include <semaphore.h>
#endif

#include <stdlib.h>

struct Semaphore
{
#ifdef _WIN32
    HANDLE handle;
#else
    sem_t handle;
#endif
};

Semaphore* semaphore_create(int initial)
{
    Semaphore* sem = malloc(sizeof(Semaphore));
#ifdef _WIN32
    sem->handle = CreateSemaphore(NULL, initial, LONG_MAX, NULL);
    if(!sem->handle)
    {
        free(sem);
        return NULL;
    }
#else
    if(sem_init(&sem->handle, 0, initial) == -1)
    {
        free(sem);
        return NULL;
    }
#endif
    return sem;
}
void semaphore_destroy(Semaphore* sem)
{
    if(!sem) return;
#ifdef _WIN32
    CloseHandle(sem->handle);
#else
    sem_destroy(&sem->handle);
#endif
    free(sem);
}

void semaphore_wait(Semaphore* sem)
{
#ifdef _WIN32
    WaitForSingleObject(sem->handle, INFINITE);
#else
    sem_wait(&sem->handle);
#endif
}
void semaphore_post(Semaphore* sem)
{
#ifdef _WIN32
    ReleaseSemaphore(sem->handle, 1, NULL);
#else
    sem_post(&sem->handle);
#endif
}
