#ifndef UTIL_OS_THREADQUEUE_H_
#define UTIL_OS_THREADQUEUE_H_

#include <util/os/mutex.h>
#include <util/os/semaphore.h>

typedef struct ThreadQueue ThreadQueue;
typedef struct ThreadQueueItem ThreadQueueItem;

struct ThreadQueue
{
    Mutex* mutex;
    Semaphore* sem;
    ThreadQueueItem* head;
    ThreadQueueItem* tail;
};
struct ThreadQueueItem
{
    ThreadQueueItem* next;
};

ThreadQueue* threadqueue_init(ThreadQueue* queue);
void threadqueue_deinit(ThreadQueue* queue);

void* threadqueue_push(ThreadQueue* queue, ThreadQueueItem* item, void (*maction)(void*));
void* threadqueue_pop(ThreadQueue* queue, void (*maction)(void*));

#endif /* UTIL_OS_THREADQUEUE_H_ */
