#ifndef UTIL_OS_THREAD_H_
#define UTIL_OS_THREAD_H_

typedef void ThreadFunction(void* udata);
typedef struct Thread Thread;

int thread_get_hwnum(void);

Thread* thread_create(ThreadFunction* func, void* udata);
void* thread_join(Thread* thread);

#endif /* UTIL_OS_THREAD_H_ */
