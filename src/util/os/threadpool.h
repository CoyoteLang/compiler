#ifndef UTIL_OS_THREADPOOL_H_
#define UTIL_OS_THREADPOOL_H_

#include <util/types.h>
#include <util/os/thread.h>
#include <util/os/threadqueue.h>

typedef struct ThreadPool ThreadPool;
typedef struct ThreadPoolData ThreadPoolData;
typedef enum ThreadTaskState ThreadTaskState;
typedef struct ThreadTask ThreadTask;

typedef void ThreadTaskFunction(void* udata);
struct ThreadPool
{
    int nthreads;
    ThreadPoolData* tdata;
    ThreadQueue qpending;
    ThreadQueue qready;
};
enum ThreadTaskState
{
    TASK_PENDING,
    TASK_PROCESSING,
    TASK_READY,
};
struct ThreadTask
{
    ThreadQueueItem item;
    ThreadPool* tpool;
    ThreadTaskFunction* func;
    void* udata;
    ThreadTaskState state;
};

ThreadPool* threadpool_init(ThreadPool* tpool, int nthreads);
void threadpool_deinit(ThreadPool* tpool);

ThreadTask* threadpool_enqueue(ThreadPool* tpool, ThreadTaskFunction* func, void* udata);

/*void threadtask_wait_all(ThreadTask** tasks, COYusize ntasks);
ThreadTask* threadtask_wait_any(ThreadTask** tasks, COYusize ntasks);
void threadtask_wait(ThreadTask* task);*/

#endif /* UTIL_OS_THREADPOOL_H_ */
