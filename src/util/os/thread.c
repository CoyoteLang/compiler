#include <util/os/thread.h>

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#else
#include <pthread.h>
#if defined(__hpux)
#include <sys/mpctl.h>
#else   // IRIX as well (unlike below)
#include <unistd.h>
#endif
#endif

#include <stdlib.h>

struct Thread
{
    ThreadFunction* func;
    void* udata;
#ifdef _WIN32
    HANDLE handle;
    DWORD id;
#else
    pthread_t handle;
#endif
};

// https://stackoverflow.com/questions/150355/programmatically-find-the-number-of-cores-on-a-machine
int thread_get_hwnum(void)
{
#ifdef _WIN32
    // TODO: replace with GetLogicalProcessorInformation (to support >= 32 cores!)
    SYSTEM_INFO sysinfo;
    GetSystemInfo(&sysinfo);
    return sysinfo.dwNumberOfProcessors;
#elif defined(__hpux)   // HP-UX
    return mpctl(MPC_GETNUMSPUS, NULL, NULL);
#elif defined(IRIX)     // IRIX
    return sysconf(_SC_NPROC_ONLN);
#else
    return sysconf(_SC_NPROCESSORS_ONLN);
#endif
}

#ifdef _WIN32
static DWORD WINAPI thread__runner(LPVOID param)
{
    Thread* thread = param;
    thread->func(thread->udata);
    return 0;
}
#else
static void* thread__runner(void* param)
{
    Thread* thread = param;
    thread->func(thread->udata);
    return NULL;
}
#endif

Thread* thread_create(ThreadFunction* func, void* udata)
{
    Thread* thread = malloc(sizeof(Thread));
    thread->func = func;
    thread->udata = udata;
#ifdef _WIN32
    thread->handle = CreateThread(NULL, 0, thread__runner, thread, 0, &thread->id);
    if(!thread->handle)
    {
        free(thread);
        return NULL;
    }
#else
    if(pthread_create(&thread->handle, NULL, thread__runner, thread))
    {
        free(thread);
        return NULL;
    }
#endif
    return thread;
}
void* thread_join(Thread* thread)
{
#ifdef _WIN32
    WaitForSingleObject(thread->handle, INFINITE);
    CloseHandle(thread->handle);
#else
    pthread_join(thread->handle, NULL);
#endif
    void* udata = thread->udata;
    free(thread);
    return udata;
}
