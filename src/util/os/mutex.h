#ifndef UTIL_OS_MUTEX_H_
#define UTIL_OS_MUTEX_H_

typedef struct Mutex Mutex;

Mutex* mutex_create(void);
void mutex_destroy(Mutex* mutex);

void mutex_lock(Mutex* mutex);
void mutex_unlock(Mutex* mutex);

void* mutex_get_handle(Mutex* mutex);

#endif /* UTIL_OS_MUTEX_H_ */
