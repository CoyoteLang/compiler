#include <util/os/mutex.h>

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#else
#include <pthread.h>
#endif

#include <stdlib.h>

struct Mutex
{
#ifdef _WIN32
    CRITICAL_SECTION handle;
#else
    pthread_mutex_t handle;
#endif
};

Mutex* mutex_create(void)
{
    Mutex* mutex = malloc(sizeof(Mutex));
#ifdef _WIN32
    InitializeCriticalSection(&mutex->handle);
#else
    pthread_mutex_init(&mutex->handle, NULL);
#endif
    return mutex;
}
void mutex_destroy(Mutex* mutex)
{
    if(!mutex) return;
#ifdef _WIN32
    DeleteCriticalSection(&mutex->handle);
#else
    pthread_mutex_destroy(&mutex->handle);
#endif
    free(mutex);
}

void mutex_lock(Mutex* mutex)
{
#ifdef _WIN32
    EnterCriticalSection(&mutex->handle);
#else
    pthread_mutex_lock(&mutex->handle);
#endif
}
void mutex_unlock(Mutex* mutex)
{
#ifdef _WIN32
    LeaveCriticalSection(&mutex->handle);
#else
    pthread_mutex_unlock(&mutex->handle);
#endif
}

void* mutex_get_handle(Mutex* mutex)
{
#ifdef _WIN32
    return &mutex->handle;
#else
    return &mutex->handle;
#endif
}
