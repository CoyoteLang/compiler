#ifndef UTIL_OS_SEMAPHORE_H_
#define UTIL_OS_SEMAPHORE_H_

typedef struct Semaphore Semaphore;

Semaphore* semaphore_create(int initial);
void semaphore_destroy(Semaphore* sem);

void semaphore_wait(Semaphore* sem);
void semaphore_post(Semaphore* sem);

#endif /* UTIL_OS_SEMAPHORE_H_ */
