#include <util/os/condvar.h>

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#ifdef __GNUC__
// apparently, these are missing in MinGW ...
VOID WINAPI InitializeConditionVariable(_Out_ PCONDITION_VARIABLE ConditionVariable);
BOOL WINAPI SleepConditionVariableCS(_Inout_ PCONDITION_VARIABLE ConditionVariable, _Inout_ PCRITICAL_SECTION CriticalSection, _In_ DWORD dwMilliseconds);
VOID WINAPI WakeConditionVariable(_Inout_ PCONDITION_VARIABLE ConditionVariable);
VOID WINAPI WakeAllConditionVariable(_Inout_ PCONDITION_VARIABLE ConditionVariable);
#endif

#else
#include <pthread.h>
#endif

#include <stdlib.h>

struct CondVar
{
#ifdef _WIN32
    CONDITION_VARIABLE handle;
#else
    pthread_cond_t handle;
#endif
};

CondVar* condvar_create(void)
{
    CondVar* cvar = malloc(sizeof(CondVar));
#ifdef _WIN32
    InitializeConditionVariable(&cvar->handle);
#else
    pthread_cond_init(&cvar->handle, NULL);
#endif
    return cvar;
}
void condvar_destroy(CondVar* cvar)
{
    if(!cvar) return;
#ifdef _WIN32
#else
    pthread_cond_destroy(&cvar->handle);
#endif
    free(cvar);
}

void condvar_wait(CondVar* cvar, Mutex* mutex)
{
#ifdef _WIN32
    SleepConditionVariableCS(&cvar->handle, mutex_get_handle(mutex), INFINITE);
#else
#endif
}
void condvar_signal(CondVar* cvar)
{
#ifdef _WIN32
    WakeConditionVariable(&cvar->handle);
#else
#endif
}
void condvar_signal_all(CondVar* cvar)
{
#ifdef _WIN32
    WakeAllConditionVariable(&cvar->handle);
#else
#endif
}
