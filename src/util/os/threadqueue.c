#include <util/os/threadqueue.h>

#include <stddef.h>

ThreadQueue* threadqueue_init(ThreadQueue* queue)
{
    if(!queue) return NULL;
    queue->mutex = mutex_create();
    queue->sem = semaphore_create(0);
    queue->head = NULL;
    queue->tail = NULL;
    return queue;
}
void threadqueue_deinit(ThreadQueue* queue)
{
    if(!queue) return;
    semaphore_destroy(queue->sem);
    mutex_destroy(queue->mutex);
}

void* threadqueue_push(ThreadQueue* queue, ThreadQueueItem* item, void (*maction)(void*))
{
    mutex_lock(queue->mutex);
        item->next = NULL;
        if(!queue->head)
            queue->head = item;
        if(queue->tail)
            queue->tail->next = item;
        queue->tail = item;
        if(maction) maction(item);
    mutex_unlock(queue->mutex);
    semaphore_post(queue->sem);
    return item;
}
void* threadqueue_pop(ThreadQueue* queue, void (*maction)(void*))
{
    semaphore_wait(queue->sem);
    mutex_lock(queue->mutex);
        ThreadQueueItem* item = queue->head;
        queue->head = item->next;
        if(!queue->head)
            queue->tail = NULL;
        if(maction) maction(item);
    mutex_unlock(queue->mutex);
    return item;
}
