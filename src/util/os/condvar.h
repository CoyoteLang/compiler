#ifndef UTIL_OS_CONDVAR_H_
#define UTIL_OS_CONDVAR_H_

#include <util/os/mutex.h>

typedef struct CondVar CondVar;

CondVar* condvar_create(void);
void condvar_destroy(CondVar* cvar);

void condvar_wait(CondVar* cvar, Mutex* mutex);
void condvar_signal(CondVar* cvar);
void condvar_signal_all(CondVar* cvar);

#endif /* UTIL_OS_CONDVAR_H_ */
