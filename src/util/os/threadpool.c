#include <util/os/threadpool.h>

#include <stdlib.h>

struct ThreadPoolData
{
    ThreadPool* tpool;
    Thread* thread;
};

static int threadpool__stop_sentinel;

static void threadpool__runner_setprocessing(void* udata)
{
    ((ThreadTask*)udata)->state = TASK_PROCESSING;
}
static void threadpool__runner_setready(void* udata)
{
    ((ThreadTask*)udata)->state = TASK_READY;
}
static void threadpool__runner(void* udata)
{
    ThreadPoolData* data = udata;
    ThreadPool* tpool = data->tpool;
    for(;;)
    {
        ThreadTask* task = threadqueue_pop(&tpool->qpending, threadpool__runner_setprocessing);

        if(task->udata == &threadpool__stop_sentinel)
            break;
        task->func(task->udata);

        threadqueue_push(&tpool->qready, &task->item, threadpool__runner_setready);
    }
}

ThreadPool* threadpool_init(ThreadPool* tpool, int nthreads)
{
    if(!tpool) return NULL;
    if(nthreads < 0) nthreads = thread_get_hwnum();

    threadqueue_init(&tpool->qpending);
    threadqueue_init(&tpool->qready);

    tpool->nthreads = nthreads;
    tpool->tdata = malloc(tpool->nthreads * sizeof(*tpool->tdata));
    int i;
    for(i = 0; i < nthreads; i++)
    {
        tpool->tdata[i].tpool = tpool;
        tpool->tdata[i].thread = thread_create(threadpool__runner, &tpool->tdata[i]);
    }
    return tpool;
}
void threadpool_deinit(ThreadPool* tpool)
{
    int i;
    for(i = 0; i < tpool->nthreads; i++)
        threadpool_enqueue(tpool, NULL, &threadpool__stop_sentinel);
    for(i = 0; i < tpool->nthreads; i++)
        thread_join(tpool->tdata[i].thread);
    while(tpool->qready.head)
    {
        ThreadQueueItem* curr = tpool->qready.head;
        tpool->qready.head = curr->next;
        free(curr);
    }
    threadqueue_deinit(&tpool->qready);
    threadqueue_deinit(&tpool->qpending);
    free(tpool->tdata);
}

ThreadTask* threadpool_enqueue(ThreadPool* tpool, ThreadTaskFunction* func, void* udata)
{
    ThreadTask* task = malloc(sizeof(ThreadTask));
    task->tpool = tpool;
    task->func = func;
    task->udata = udata;
    task->state = TASK_PENDING;
    return threadqueue_push(&tpool->qpending, &task->item, NULL);
}

/*void threadtask_wait_all(ThreadTask** tasks, COYusize ntasks);
ThreadTask* threadtask_wait_any(ThreadTask** tasks, COYusize ntasks);
void threadtask_wait(ThreadTask* task);*/
