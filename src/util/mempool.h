#ifndef UTIL_MEMPOOL_H_
#define UTIL_MEMPOOL_H_

#include <stddef.h>

typedef struct MemPool MemPool;
typedef struct MemItem MemItem;

struct MemItem
{
    MemItem* next;
    void (*mark)(void* ptr, void (*markcb)(void* mdata), void* mdata);
};
struct MemPool
{

};

void* mem_malloc(size_t size);
void* mem_realloc(void* ptr, size_t size);

#endif /* UTIL_MEMPOOL_H_ */
