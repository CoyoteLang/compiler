#ifndef UTIL_UNICODE_H_
#define UTIL_UNICODE_H_

#include <util/types.h>

//int unicode_utf8from16(COYchar* dst, const COYwchar* src, int slen);
int unicode_utf8from32(COYchar* dst, const COYdchar* src, int slen);
//int unicode_utf16from8(COYwchar* dst, const COYchar* src, int slen);
//int unicode_utf16from32(COYwchar* dst, const COYdchar* src, int slen);
//int unicode_utf32from8(COYdchar* dst, const COYchar* src, int slen);
int unicode_utf32from16(COYdchar* dst, const COYwchar* src, int slen);

#endif /* UTIL_UNICODE_H_ */
