#ifndef UTIL_FILEPOS_H_
#define UTIL_FILEPOS_H_

#include <util/types.h>

typedef struct FilePos
{
    uint32_t line;
    uint32_t col;
} FilePos;

typedef struct FileRange
{
    FilePos head;
    FilePos tail;
} FileRange;

int fpos_cmp(FilePos a, FilePos b);
FilePos fpos_min(FilePos a, FilePos b);
FilePos fpos_max(FilePos a, FilePos b);

FileRange frange_bounds(FileRange a, FileRange b);

#endif /* UTIL_FILEPOS_H_ */
