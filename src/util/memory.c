#include <util/memory.h>

#include <stdlib.h>
#include <assert.h>

void* mem_malloc(size_t size)
{
    /* TODO store in a list for clearing */
    void* ptr = malloc(size);
    assert(ptr);
    return ptr;
}
void* mem_realloc(void* ptr, size_t size)
{
    ptr = realloc(ptr, size);
    assert(ptr);
    return ptr;
}
