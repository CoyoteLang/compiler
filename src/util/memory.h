#ifndef UTIL_MEMORY_H_
#define UTIL_MEMORY_H_

#include <stddef.h>

void* mem_malloc(size_t size);
void* mem_realloc(void* ptr, size_t size);

#endif /* UTIL_MEMORY_H_ */
