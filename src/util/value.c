#include <util/value.h>
#include <util/memory.h>

#include <string.h>
#include <assert.h>
#include <math.h>
#include <limits.h>

// TODO: move to appropriate place
#include <stdio.h>
#include <stdarg.h>
#include <inttypes.h>
static void verrorf(FileContext* fctx, const char* format, va_list args)
{
    //FilePos pos = parser->tokens[parser->pos].range.head;
    fprintf(stderr, "SEMANTIC ERROR(?:?): "/*, (unsigned int)(pos.line + 1), (unsigned int)(pos.col + 1)*/);
    vfprintf(stderr, format, args);
    fprintf(stderr, "\n");
    fflush(stderr);
    exit(1);
}
static void errorf(FileContext* fctx, const char* format, ...)
{
    va_list args;
    va_start(args, format);
    verrorf(fctx, format, args);
    va_end(args);
}

Value value_mkvoid(FileContext* fctx)
{
    Value v;
    v.type = &Tvoid;
    return v;
}
Value value_mkbool(FileContext* fctx, COYbool b)
{
    Value v;
    v.type = &Tbool;
    v.v.u = !!b;
    return v;
}

Value value_mkbyte(FileContext* fctx, COYbyte i)
{
    Value v;
    v.type = &Tbyte;
    v.v.i = i;
    return v;
}
Value value_mkubyte(FileContext* fctx, COYubyte u)
{
    Value v;
    v.type = &Tubyte;
    v.v.u = u;
    return v;
}
Value value_mkshort(FileContext* fctx, COYshort i)
{
    Value v;
    v.type = &Tshort;
    v.v.i = i;
    return v;
}
Value value_mkushort(FileContext* fctx, COYushort u)
{
    Value v;
    v.type = &Tushort;
    v.v.u = u;
    return v;
}
Value value_mkint(FileContext* fctx, COYint i)
{
    Value v;
    v.type = &Tint;
    v.v.i = i;
    return v;
}
Value value_mkuint(FileContext* fctx, COYuint u)
{
    Value v;
    v.type = &Tuint;
    v.v.u = u;
    return v;
}
Value value_mklong(FileContext* fctx, COYlong i)
{
    Value v;
    v.type = &Tlong;
    v.v.i = i;
    return v;
}
Value value_mkulong(FileContext* fctx, COYulong u)
{
    Value v;
    v.type = &Tulong;
    v.v.u = u;
    return v;
}
Value value_mkfloat(FileContext* fctx, COYfloat r)
{
    Value v;
    v.type = &Tfloat;
    v.v.r = r;
    return v;
}
Value value_mkdouble(FileContext* fctx, COYdouble r)
{
    Value v;
    v.type = &Tdouble;
    v.v.r = r;
    return v;
}
Value value_mkreal(FileContext* fctx, COYreal r)
{
    Value v;
    v.type = &Treal;
    v.v.r = r;
    return v;
}
Value value_mkchar(FileContext* fctx, COYchar c)
{
    Value v;
    v.type = &Tchar;
    v.v.u = (COYubyte)c;
    return v;
}
Value value_mkwchar(FileContext* fctx, COYwchar c)
{
    Value v;
    v.type = &Twchar;
    v.v.u = c;
    return v;
}
Value value_mkdchar(FileContext* fctx, COYdchar c)
{
    Value v;
    v.type = &Tdchar;
    v.v.u = c;
    return v;
}
Value value_mkstring(FileContext* fctx, const COYchar* str, int len)
{
    Value v;
    v.type = &Tstring;
    v.v.istr = istring_intern(&fctx->itab, str, len);
    return v;
}
/*Value value_mkwstring(FileContext* fctx, const COYwchar* str, int len);
Value value_mkdstring(FileContext* fctx, const COYdchar* str, int len);*/

Value value_mktype(FileContext* fctx, Type* type)
{
    Value v;
    v.type = &Ttype;
    v.v.type = type;
    return v;
}

COYusize value_strlen(Value* a)
{
    assert(a->type->family == TF_STRING && a->type->u.ustring.val == &Tchar);
    return a->v.istr->len;
}
COYbool value_streq(Value* a, Value* b)
{
    assert(a->type->family == TF_STRING && a->type->u.ustring.val == &Tchar);
    assert(b->type->family == TF_STRING && b->type->u.ustring.val == &Tchar);
    return a->v.istr == b->v.istr;
}

// implicit_cast(T) a
Value value_op_icast(FileContext* fctx, Type* T, Value a)
{
    if(a.type == T) return a; /* casting to own type always succeeds */

    // TODO: vector & complex types
    // TODO: compound types (classes, ...)
    switch(a.type->family)
    {
    case TF_BOOLEAN:    // bool => SELF|INTEGER|CHARACTER|REAL
        switch(T->family)
        {
        case TF_BOOLEAN: return a;
        case TF_INTEGER:
            if(T->u.uinteger.is_signed)
            {
                switch(T->u.uinteger.width)
                {
                case 1: return value_mkbyte(fctx, a.v.u);
                case 2: return value_mkshort(fctx, a.v.u);
                case 4: return value_mkint(fctx, a.v.u);
                case 8: return value_mklong(fctx, a.v.u);
                default: assert(0);
                }
            }
            else
            {
                switch(T->u.uinteger.width)
                {
                case 1: return value_mkubyte(fctx, a.v.u);
                case 2: return value_mkushort(fctx, a.v.u);
                case 4: return value_mkuint(fctx, a.v.u);
                case 8: return value_mkulong(fctx, a.v.u);
                default: assert(0);
                }
            }
            assert(0);
        case TF_CHARACTER:
            switch(T->u.ucharacter.width)
            {
            case 1: return value_mkchar(fctx, a.v.u);
            case 2: return value_mkwchar(fctx, a.v.u);
            case 4: return value_mkdchar(fctx, a.v.u);
            default: assert(0);
            }
            break;
        case TF_REAL:
            switch(T->u.ureal.width)
            {
            case 4: return value_mkfloat(fctx, a.v.u);
            case 8: return value_mkdouble(fctx, a.v.u);
            case 16: return value_mkreal(fctx, a.v.u);
            default: assert(0);
            }
            assert(0);
        default: return value_mkvoid(fctx);
        }
        assert(0);
    case TF_INTEGER:    // byte|ubyte|short|ushort|int|uint|long|ulong => SELF|CHARACTER|REAL
        if(a.type->u.uinteger.is_signed)
        {
            switch(T->family)
            {
            case TF_INTEGER:
                if(T->u.uinteger.is_signed)
                {
                    switch(T->u.uinteger.width)
                    {
                    case 1: return value_mkbyte(fctx, a.v.i);
                    case 2: return value_mkshort(fctx, a.v.i);
                    case 4: return value_mkint(fctx, a.v.i);
                    case 8: return value_mklong(fctx, a.v.i);
                    default: assert(0);
                    }
                }
                else
                {
                    switch(T->u.uinteger.width)
                    {
                    case 1: return value_mkubyte(fctx, a.v.i);
                    case 2: return value_mkushort(fctx, a.v.i);
                    case 4: return value_mkuint(fctx, a.v.i);
                    case 8: return value_mkulong(fctx, a.v.i);
                    default: assert(0);
                    }
                }
                assert(0);
            case TF_CHARACTER:
                switch(T->u.ucharacter.width)
                {
                case 1: return value_mkchar(fctx, a.v.i);
                case 2: return value_mkwchar(fctx, a.v.i);
                case 4: return value_mkdchar(fctx, a.v.i);
                default: assert(0);
                }
                assert(0);
            case TF_REAL:
                switch(T->u.ureal.width)
                {
                case 4: return value_mkfloat(fctx, a.v.i);
                case 8: return value_mkdouble(fctx, a.v.i);
                case 16: return value_mkreal(fctx, a.v.i);
                default: assert(0);
                }
                assert(0);
            default: return value_mkvoid(fctx);
            }
        }
        else
        {
            switch(T->family)
            {
            case TF_INTEGER:
                if(T->u.uinteger.is_signed)
                {
                    switch(T->u.uinteger.width)
                    {
                    case 1: return value_mkbyte(fctx, a.v.u);
                    case 2: return value_mkshort(fctx, a.v.u);
                    case 4: return value_mkint(fctx, a.v.u);
                    case 8: return value_mklong(fctx, a.v.u);
                    default: assert(0);
                    }
                }
                else
                {
                    switch(T->u.uinteger.width)
                    {
                    case 1: return value_mkubyte(fctx, a.v.u);
                    case 2: return value_mkushort(fctx, a.v.u);
                    case 4: return value_mkuint(fctx, a.v.u);
                    case 8: return value_mkulong(fctx, a.v.u);
                    default: assert(0);
                    }
                }
                assert(0);
            case TF_CHARACTER:
                switch(T->u.ucharacter.width)
                {
                case 1: return value_mkchar(fctx, a.v.u);
                case 2: return value_mkwchar(fctx, a.v.u);
                case 4: return value_mkdchar(fctx, a.v.u);
                default: assert(0);
                }
                assert(0);
            case TF_REAL:
                switch(T->u.ureal.width)
                {
                case 4: return value_mkfloat(fctx, a.v.u);
                case 8: return value_mkdouble(fctx, a.v.u);
                case 16: return value_mkreal(fctx, a.v.u);
                default: assert(0);
                }
                assert(0);
            default: return value_mkvoid(fctx);
            }
        }
        assert(0);
    case TF_CHARACTER:  // char|wchar|dchar => SELF|INTEGER|REAL
        switch(T->family)
        {
        case TF_INTEGER:
            if(T->u.uinteger.is_signed)
            {
                switch(T->u.uinteger.width)
                {
                case 1: return value_mkbyte(fctx, a.v.u);
                case 2: return value_mkshort(fctx, a.v.u);
                case 4: return value_mkint(fctx, a.v.u);
                case 8: return value_mklong(fctx, a.v.u);
                default: assert(0);
                }
            }
            else
            {
                switch(T->u.uinteger.width)
                {
                case 1: return value_mkubyte(fctx, a.v.u);
                case 2: return value_mkushort(fctx, a.v.u);
                case 4: return value_mkuint(fctx, a.v.u);
                case 8: return value_mkulong(fctx, a.v.u);
                default: assert(0);
                }
            }
            assert(0);
        case TF_CHARACTER:
            switch(T->u.ucharacter.width)
            {
            case 1: return value_mkchar(fctx, a.v.u);
            case 2: return value_mkwchar(fctx, a.v.u);
            case 4: return value_mkdchar(fctx, a.v.u);
            default: assert(0);
            }
            assert(0);
        case TF_REAL:
            switch(T->u.ureal.width)
            {
            case 4: return value_mkfloat(fctx, a.v.u);
            case 8: return value_mkdouble(fctx, a.v.u);
            case 16: return value_mkreal(fctx, a.v.u);
            default: assert(0);
            }
            assert(0);
        default: return value_mkvoid(fctx);
        }
        assert(0);
    case TF_REAL:   // float|double|real => SELF
        switch(T->family)
        {
        case TF_REAL:
            switch(T->u.ureal.width)
            {
            case 4: return value_mkfloat(fctx, a.v.r);
            case 8: return value_mkdouble(fctx, a.v.r);
            case 16: return value_mkreal(fctx, a.v.r);
            default: assert(0);
            }
            assert(0);
        default: return value_mkvoid(fctx);
        }
        assert(0);
    case TF_CLASS:  // $x => SELF|IS_SUPERCLASS($x)
        assert(0 && "TODO: class implicit upcasts");
        return value_mkvoid(fctx);
    case TF_ARRAY:  // $x:pod_type[] => void[]
        assert(0 && "TODO: array implicit casts");
        return value_mkvoid(fctx);
    case TF_STRING: // string => string; wstring => wstring; dstring => dstring; ustring => string|wstring|dstring
        assert(0 && "TODO: string implicit casts");
        return value_mkvoid(fctx);
    case TF_DICT:
        assert(0 && "TODO: dict implicit casts");
        return value_mkvoid(fctx);
    case TF_SET:
        assert(0 && "TODO: set implicit casts");
        return value_mkvoid(fctx);
    case TF_MIXED:  // mixed => SELF
        assert(T != &Tmixed); // this is checked at the very start
        return value_mkvoid(fctx);
    case TF_STRUCT:
    case TF_DELEGATE:   // TODO: allow aliasing `void foo(int)` as `void foo(uint)`? (basically, ignore sign for integers here?)
        return value_mkvoid(fctx);
    case TF_OPTION:
        assert(0 && "TODO: option implicit casts");
        return value_mkvoid(fctx);
    case TF_VOID: assert(0 && "Tried to call implicit_cast() on a void value");
    case TF_TYPE: assert(0 && "Tried to call implicit_cast() on a type");
    }
    assert(0);
    return value_mkvoid(fctx);
}
// cast(T) a
Value value_op_cast(FileContext* fctx, Type* T, Value a)
{
    // if implicit cast works, then explicit always will!
    Value ret = value_op_icast(fctx, T, a);
    if(ret.type != &Tvoid) return ret;

    // TODO: vector & complex types
    // TODO: compound types
    switch(a.type->family)
    {
    case TF_BOOLEAN:    // bool => <IMPLICIT>
        return value_mkvoid(fctx); // under no circumstances can we explicitly cast it, but not also implicitly
    case TF_INTEGER:    // byte|ubyte|short|ushort|int|uint|long|ulong => <IMPLICIT>|BOOL
        if(a.type->u.uinteger.is_signed)
        {
            switch(T->family)
            {
            case TF_BOOLEAN:
                return value_mkbool(fctx, !!a.v.i);
            default: return value_mkvoid(fctx);
            }
        }
        else
        {
            switch(T->family)
            {
            case TF_BOOLEAN:
                return value_mkbool(fctx, !!a.v.u);
            default: return value_mkvoid(fctx);
            }
        }
        assert(0);
    case TF_CHARACTER:  // char|wchar|dchar => <IMPLICIT>|BOOL
        switch(T->family)
        {
        case TF_BOOLEAN:
            return value_mkbool(fctx, !!a.v.u);
        default: return value_mkvoid(fctx);
        }
        assert(0);
    case TF_REAL:   // float|double|real => <IMPLICIT>|BOOL|INTEGER
        switch(T->family)
        {
        case TF_BOOLEAN:
            return value_mkbool(fctx, !!a.v.r);
        case TF_INTEGER:
            if(T->u.uinteger.is_signed)
            {
                switch(T->u.uinteger.width)
                {
                case 1: return value_mkbyte(fctx, (COYbyte)a.v.r);
                case 2: return value_mkshort(fctx, (COYshort)a.v.r);
                case 4: return value_mkint(fctx, (COYint)a.v.r);
                case 8: return value_mklong(fctx, (COYlong)a.v.r);
                default: assert(0);
                }
            }
            else
            {
                switch(T->u.uinteger.width)
                {
                case 1: return value_mkubyte(fctx, (COYubyte)a.v.r);
                case 2: return value_mkushort(fctx, (COYushort)a.v.r);
                case 4: return value_mkuint(fctx, (COYuint)a.v.r);
                case 8: return value_mkulong(fctx, (COYulong)a.v.r);
                default: assert(0);
                }
            }
            assert(0);
        default: return value_mkvoid(fctx);
        }
        assert(0);
    case TF_CLASS:  // $x => <IMPLICIT>|IS_SUBCLASS($x):verify_dynamic
        assert(0 && "TODO: class explicit casts");
        return value_mkvoid(fctx);
    // TODO: should we support e.g. cast(int[])some_byte_array to do a per-element cast?
    case TF_ARRAY:  // $x:pod_type[] => <IMPLICIT>|$y:pod_type[]
        assert(0 && "TODO: array explicit casts");
        return value_mkvoid(fctx);
    case TF_STRING: // string|string|dstring => <IMPLICIT>
        assert(0 && "TODO: string explicit casts");
        return value_mkvoid(fctx);
    case TF_DICT:
        assert(0 && "TODO: dict explicit casts");
        return value_mkvoid(fctx);
    case TF_SET:
        assert(0 && "TODO: set explicit casts");
        return value_mkvoid(fctx);
    case TF_MIXED:  // $x => <IMPLICIT>|IS_CASTABLE($x):verify_dynamic
        assert(0 && "TODO: mixed explicit casts");
        return value_mkvoid(fctx);
    case TF_STRUCT: // TODO: same as for arrays, do we allow same-size POD struct casts?
    case TF_DELEGATE:   // TODO: allow aliasing `void foo(int)` as `void foo(uint)`? (basically, ignore sign for integers here?)
        return value_mkvoid(fctx);
    case TF_OPTION:
        assert(0 && "TODO: option explicit casts");
        return value_mkvoid(fctx);
    case TF_VOID: assert(0 && "Tried to call cast() on a void value");
    case TF_TYPE: assert(0 && "Tried to call cast() on a type");
    }
    assert(0);
    return value_mkvoid(fctx);
}
// bitcast(T) a
Value value_op_bitcast(FileContext* fctx, Type* T, Value a)
{
    if(a.type == T) return a; // TODO: always forbid self-cast for reference types?

    union { uint32_t u32; uint64_t u64; COYfloat f32; COYdouble f64; } conv;

    // TODO: vector & complex types
    // TODO: compound types
    switch(a.type->family)
    {
    case TF_BOOLEAN:    // bool => SELF
        assert(T != &Tbool); // should've been handled at the start of the function
        return value_mkvoid(fctx);
    case TF_INTEGER:    // byte|ubyte => SELF|char; short|ushort => SELF|wchar; int|uint => SELF|dchar|float; long|ulong => SELF|double
        switch(T->family)
        {
        case TF_INTEGER:
            if(a.type->u.uinteger.width != T->u.uinteger.width) return value_mkvoid(fctx);
            if(T->u.uinteger.is_signed)
            {
                switch(T->u.uinteger.width)
                {
                case 1: return value_mkbyte(fctx, a.v.i);
                case 2: return value_mkshort(fctx, a.v.i);
                case 4: return value_mkint(fctx, a.v.i);
                case 8: return value_mklong(fctx, a.v.i);
                default: assert(0);
                }
            }
            else
            {
                switch(T->u.uinteger.width)
                {
                case 1: return value_mkubyte(fctx, a.v.u);
                case 2: return value_mkushort(fctx, a.v.u);
                case 4: return value_mkuint(fctx, a.v.u);
                case 8: return value_mkulong(fctx, a.v.u);
                default: assert(0);
                }
            }
            assert(0);
        case TF_CHARACTER:
            if(a.type->u.uinteger.width != T->u.ucharacter.width) return value_mkvoid(fctx);
            switch(T->u.ucharacter.width)
            {
            case 1: return value_mkchar(fctx, a.v.u);
            case 2: return value_mkwchar(fctx, a.v.u);
            case 4: return value_mkdchar(fctx, a.v.u);
            default: assert(0);
            }
            assert(0);
        case TF_REAL:
            if(T == &Treal || a.type->u.uinteger.width != T->u.ureal.width) return value_mkvoid(fctx);
            switch(T->u.ureal.width)
            {
            case 4:
                conv.u32 = a.v.u;
                return value_mkfloat(fctx, conv.f32);
            case 8:
                conv.u64 = a.v.u;
                return value_mkdouble(fctx, conv.f64);
            }
            assert(0);
        default: return value_mkvoid(fctx);
        }
        assert(0);
    case TF_CHARACTER:  // char => SELF|byte|ubyte; wchar => SELF|short|ushort; dchar => SELF|int|uint|float
        switch(T->family)
        {
        case TF_INTEGER:
            if(a.type->u.ucharacter.width != T->u.uinteger.width) return value_mkvoid(fctx);
            if(T->u.uinteger.is_signed)
            {
                switch(T->u.uinteger.width)
                {
                case 1: return value_mkbyte(fctx, a.v.i);
                case 2: return value_mkshort(fctx, a.v.i);
                case 4: return value_mkint(fctx, a.v.i);
                default: assert(0);
                }
            }
            else
            {
                switch(T->u.uinteger.width)
                {
                case 1: return value_mkubyte(fctx, a.v.u);
                case 2: return value_mkushort(fctx, a.v.u);
                case 4: return value_mkuint(fctx, a.v.u);
                default: assert(0);
                }
            }
            assert(0);
        case TF_CHARACTER:
            if(a.type->u.ucharacter.width != T->u.ucharacter.width) return value_mkvoid(fctx);
            return a;
        case TF_REAL:
            if(T == &Treal || a.type->u.ucharacter.width != T->u.ureal.width) return value_mkvoid(fctx);
            switch(T->u.ureal.width)
            {
            case 4:
                conv.u32 = a.v.u;
                return value_mkfloat(fctx, conv.f32);
            }
            assert(0);
        default: return value_mkvoid(fctx);
        }
        assert(0);
    case TF_REAL:   // float => SELF|int|uint|dchar; double => SELF|long|ulong; real => SELF
        switch(T->family)
        {
        case TF_INTEGER:
            if(a.type->u.ureal.width != T->u.uinteger.width) return value_mkvoid(fctx);
            if(T->u.uinteger.is_signed)
            {
                switch(T->u.uinteger.width)
                {
                case 1: return value_mkbyte(fctx, a.v.i);
                case 2: return value_mkshort(fctx, a.v.i);
                case 4: return value_mkint(fctx, a.v.i);
                default: assert(0);
                }
            }
            else
            {
                switch(T->u.uinteger.width)
                {
                case 1: return value_mkubyte(fctx, a.v.u);
                case 2: return value_mkushort(fctx, a.v.u);
                case 4: return value_mkuint(fctx, a.v.u);
                default: assert(0);
                }
            }
            assert(0);
        case TF_CHARACTER:
            if(a.type->u.ureal.width != T->u.ucharacter.width) return value_mkvoid(fctx);
            switch(T->u.ucharacter.width)
            {
            case 4:
                conv.f32 = a.v.r;
                return value_mkdchar(fctx, conv.u32);
            default: assert(0);
            }
            assert(0);
        case TF_REAL:
            if(T == &Treal || a.type->u.ureal.width != T->u.ureal.width) return value_mkvoid(fctx);
            return a;
        default: return value_mkvoid(fctx);
        }
        assert(0);
    case TF_CLASS:
        assert(0 && "TODO: class bitcasts");
        return value_mkvoid(fctx);
    case TF_ARRAY:
        assert(0 && "TODO: array bitcasts");
        return value_mkvoid(fctx);
    case TF_STRING:
        assert(0 && "TODO: string bitcasts");
        return value_mkvoid(fctx);
    case TF_DICT:
        assert(0 && "TODO: dict bitcasts");
        return value_mkvoid(fctx);
    case TF_SET:
        assert(0 && "TODO: set bitcasts");
        return value_mkvoid(fctx);
    case TF_MIXED:
        assert(T != &Tmixed); // should've been handled at the start
        return value_mkvoid(fctx);
    case TF_STRUCT: // TODO: same as for arrays, do we allow same-size POD struct casts?
    case TF_DELEGATE:   // TODO: allow aliasing `void foo(int)` as `void foo(uint)`? (basically, ignore sign for integers here?)
        return value_mkvoid(fctx);
    case TF_OPTION:
        assert(0 && "TODO: option bitcasts");
        return value_mkvoid(fctx);
    case TF_VOID: assert(0 && "Tried to call bitcast() on a void value");
    case TF_TYPE: assert(0 && "Tried to call bitcast() on a type");
    }
    assert(0);
    return value_mkvoid(fctx);
}

// C-like integer promotions (floats are left alone)
Value value_promote_numeric(FileContext* fctx, Value a)
{
    // TODO: vector & complex types
    switch(a.type->family)
    {
    case TF_BOOLEAN: return value_mkint(fctx, a.v.u);
    case TF_INTEGER:
        if(a.type->u.uinteger.is_signed)
        {
            switch(a.type->u.uinteger.width)
            {
            case 1:
            case 2: return value_mkint(fctx, a.v.i);
            case 4:
            case 8: return a;
            default: assert(0);
            }
        }
        else
        {
            switch(a.type->u.uinteger.width)
            {
            case 1:
            case 2: return value_mkint(fctx, a.v.u);
            case 4:
            case 8: return a;
            default: assert(0);
            }
        }
        assert(0);
    case TF_CHARACTER:
        switch(a.type->u.ucharacter.width)
        {
        case 1:
        case 2:
        case 4: return value_mkint(fctx, a.v.u);
        default: assert(0);
        }
        assert(0);
    case TF_REAL:
        return a;
    case TF_VOID:
    case TF_STRUCT:
    case TF_CLASS:
    case TF_ARRAY:
    case TF_STRING:
    case TF_DICT:
    case TF_SET:
    case TF_DELEGATE:
    case TF_OPTION:
    case TF_MIXED:
        return value_mkvoid(fctx);
    case TF_TYPE:
        assert(0 && "Tried to promote type as numeric");
    }
    assert(0);
    return value_mkvoid(fctx);
}
// C "usual arithmetic conversions" (+ floats)
COYbool value_promote_binary(FileContext* fctx, Value* a, Value* b)
{
    // TODO: vector & complex types
    *a = value_promote_numeric(fctx, *a);
    *b = value_promote_numeric(fctx, *b);
    if(a->type == &Tvoid || b->type == &Tvoid) return COY_FALSE;

    // "If both operands have the same type, then no further conversion is needed."
    if(a->type == b->type) return COY_TRUE;

    if(a->type->family == b->type->family)
    {
        switch(a->type->family)
        {
        case TF_INTEGER:
            /*
             * "Otherwise, if both operands have signed integer types or both
             * have unsigned integer types, the operand with the type of lesser
             * integer conversion rank is converted to the type of the operand
             * with greater rank."
             */
            if(a->type->u.uinteger.is_signed == b->type->u.uinteger.is_signed)
            {
                if(a->type->u.uinteger.width > b->type->u.uinteger.width)
                {
                    *b = value_op_icast(fctx, a->type, *b);
                    return COY_TRUE;
                }
                else
                {
                    *a = value_op_icast(fctx, b->type, *a);
                    return COY_TRUE;
                }
            }
            /*
             * "Otherwise, if the operand that has unsigned integer type has
             * rank greater or equal to the rank of the type of the other
             * operand, then the operand with signed integer type is converted
             * to the type of the operand with unsigned integer type."
             */
            if(!a->type->u.uinteger.is_signed && a->type->u.uinteger.width >= b->type->u.uinteger.width)
            {
                *b = value_op_icast(fctx, a->type, *b);
                return COY_TRUE;
            }
            if(!b->type->u.uinteger.is_signed && b->type->u.uinteger.width >= a->type->u.uinteger.width)
            {

                *a = value_op_icast(fctx, b->type, *a);
                return COY_TRUE;
            }
            /*
             * Otherwise, if the type of the operand with signed integer type
             * can represent all of the values of the type of the operand with
             * unsigned integer type, then the operand with unsigned integer
             * type is converted to the type of the operand with signed integer
             * type.
             */
            if(a->type->u.uinteger.is_signed && a->type->u.uinteger.width > b->type->u.uinteger.width)
            {
                *b = value_op_icast(fctx, a->type, *b);
                return COY_TRUE;
            }
            if(b->type->u.uinteger.is_signed && b->type->u.uinteger.width > a->type->u.uinteger.width)
            {
                *a = value_op_icast(fctx, b->type, *a);
                return COY_TRUE;
            }
            /*
             * Otherwise, both operands are converted to the unsigned integer
             * type corresponding to the type of the operand with signed integer
             * type.
             */
            if(a->type->u.uinteger.is_signed)
            {
                switch(a->type->u.uinteger.width)
                {
                case 4:
                    *a = value_mkuint(fctx, a->v.i);
                    *b = value_mkuint(fctx, b->v.u);
                    return COY_TRUE;
                case 8:
                    *a = value_mkulong(fctx, a->v.i);
                    *b = value_mkulong(fctx, b->v.u);
                    return COY_TRUE;
                default: assert(0);
                }
            }
            if(b->type->u.uinteger.is_signed)
            {
                switch(b->type->u.uinteger.width)
                {
                case 4:
                    *a = value_mkuint(fctx, a->v.u);
                    *b = value_mkuint(fctx, b->v.i);
                    return COY_TRUE;
                case 8:
                    *a = value_mkulong(fctx, a->v.u);
                    *b = value_mkulong(fctx, b->v.i);
                    return COY_TRUE;
                default: assert(0);
                }
            }
            assert(0);
        case TF_REAL:
            if(a->type->u.ureal.width > b->type->u.ureal.width)
            {
                *b = value_op_icast(fctx, a->type, *b);
                return COY_TRUE;
            }
            if(b->type->u.ureal.width > a->type->u.ureal.width)
            {
                *a = value_op_icast(fctx, b->type, *a);
                return COY_TRUE;
            }
            // they should never be == (otherwise, the same-type rule should've caught it)
            assert(0);
        default: assert(0);
        }
    }

    // int OP real; real OP int
    if(a->type->family == TF_INTEGER)
    {
        assert(b->type->family == TF_REAL);
        *a = value_op_icast(fctx, b->type, *a);
        return COY_TRUE;
    }
    if(b->type->family == TF_INTEGER)
    {
        assert(a->type->family == TF_REAL);
        *b = value_op_icast(fctx, a->type, *b);
        return COY_TRUE;
    }
    assert(0);
    return COY_FALSE;
}

typedef struct OpUnary
{
    COYlong (*i)(FileContext*,COYlong);
    COYulong (*u)(FileContext*,COYulong);
    COYreal (*r)(FileContext*,COYreal);
} OpUnary;
typedef struct OpBinary
{
    COYlong (*i)(FileContext*,COYlong,COYlong);
    COYulong (*u)(FileContext*,COYulong,COYulong);
    COYreal (*r)(FileContext*,COYreal,COYreal);
} OpBinary;
Value value_op_arith_unary(FileContext* fctx, Value a, const struct OpUnary* ops)
{
    a = value_promote_numeric(fctx, a);
    // TODO: vector & complex types
    switch(a.type->family)
    {
    case TF_INTEGER:
        if(a.type->u.uinteger.is_signed)
        {
            if(!ops->i) return value_mkvoid(fctx);
            switch(a.type->u.uinteger.width)
            {
            case 4: return value_mkint(fctx, ops->i(fctx, a.v.i));
            case 8: return value_mklong(fctx, ops->i(fctx, a.v.i));
            default: assert(0);
            }
        }
        else
        {
            if(!ops->u) return value_mkvoid(fctx);
            switch(a.type->u.uinteger.width)
            {
            case 4: return value_mkuint(fctx, ops->u(fctx, a.v.u));
            case 8: return value_mkulong(fctx, ops->u(fctx, a.v.u));
            default: assert(0);
            }
        }
        assert(0);
    case TF_REAL:
        if(!ops->r) return value_mkvoid(fctx);
        switch(a.type->u.ureal.width)
        {
        case 4: return value_mkfloat(fctx, ops->r(fctx, a.v.r));
        case 8: return value_mkdouble(fctx, ops->r(fctx, a.v.r));
        case 16: return value_mkreal(fctx, ops->r(fctx, a.v.r));
        default: assert(0);
        }
        assert(0);
    default: assert(0);
    }
    assert(0);
    return value_mkvoid(fctx);
}
Value value_op_arith_binary(FileContext* fctx, Value a, Value b, const struct OpBinary* ops)
{
    if(!value_promote_binary(fctx, &a, &b)) return value_mkvoid(fctx);
    switch(a.type->family)
    {
    case TF_INTEGER:
        if(a.type->u.uinteger.is_signed)
        {
            if(!ops->i) return value_mkvoid(fctx);
            switch(a.type->u.uinteger.width)
            {
            case 4: return value_mkint(fctx, ops->i(fctx, a.v.i, b.v.i));
            case 8: return value_mklong(fctx, ops->i(fctx, a.v.i, b.v.i));
            default: assert(0);
            }
        }
        else
        {
            if(!ops->u) return value_mkvoid(fctx);
            switch(a.type->u.uinteger.width)
            {
            case 4: return value_mkuint(fctx, ops->u(fctx, a.v.u, b.v.u));
            case 8: return value_mkulong(fctx, ops->u(fctx, a.v.u, b.v.u));
            default: assert(0);
            }
        }
        assert(0);
    case TF_REAL:
        if(!ops->r) return value_mkvoid(fctx);
        switch(a.type->u.ureal.width)
        {
        case 4: return value_mkfloat(fctx, ops->r(fctx, a.v.r, b.v.r));
        case 8: return value_mkdouble(fctx, ops->r(fctx, a.v.r, b.v.r));
        case 16: return value_mkreal(fctx, ops->r(fctx, a.v.r, b.v.r));
        default: assert(0);
        }
        assert(0);
    default: assert(0);
    }
    assert(0);
    return value_mkvoid(fctx);
}

#define AFUNC_UNARY(NAME)                                                      \
Value value_op_##NAME(FileContext* fctx, Value a) {                            \
    static const struct OpUnary Ops = { value_uop_##NAME##__i, value_uop_##NAME##__u, value_uop_##NAME##__r };\
    return value_op_arith_unary(fctx, a, &Ops);                                \
}
#define ADECL_UNARY(NAME,OP)                                                   \
static COYlong  value_uop_##NAME##__i(FileContext* fctx, COYlong a)  { return OP a; }\
static COYulong value_uop_##NAME##__u(FileContext* fctx, COYulong a) { return OP a; }\
static COYreal  value_uop_##NAME##__r(FileContext* fctx, COYreal a)  { return OP a; }\
AFUNC_UNARY(NAME)

#define AFUNC_BINARY(NAME)                                                     \
Value value_op_##NAME(FileContext* fctx, Value a, Value b) {                   \
    static const struct OpBinary Ops = { value_bop_##NAME##__i, value_bop_##NAME##__u, value_bop_##NAME##__r };\
    return value_op_arith_binary(fctx, a, b, &Ops);                            \
}
#define ADECL_BINARY(NAME,OP)                                                  \
static COYlong  value_bop_##NAME##__i(FileContext* fctx, COYlong a, COYlong b)   { return a OP b; }\
static COYulong value_bop_##NAME##__u(FileContext* fctx, COYulong a, COYulong b) { return a OP b; }\
static COYreal  value_bop_##NAME##__r(FileContext* fctx, COYreal a, COYreal b)   { return a OP b; }\
AFUNC_BINARY(NAME)

// these are not defined for reals!
#define BFUNC_BINARY(NAME)                                                     \
Value value_op_##NAME(FileContext* fctx, Value a, Value b) {                   \
    static const struct OpBinary Ops = { value_bop_##NAME##__i, value_bop_##NAME##__u, NULL };\
    return value_op_arith_binary(fctx, a, b, &Ops);                            \
}
#define BDECL_BINARY(NAME,OP)                                                  \
static COYlong  value_bop_##NAME##__i(FileContext* fctx, COYlong a, COYlong b)   { return a OP b; }\
static COYulong value_bop_##NAME##__u(FileContext* fctx, COYulong a, COYulong b) { return a OP b; }\
BFUNC_BINARY(NAME)

Value value_op_pos(FileContext* fctx, Value a) { return value_promote_numeric(fctx, a); }
ADECL_UNARY(neg,-)
// !a
Value value_op_not(FileContext* fctx, Value a)
{
    // not subject to usual integer promotion rules (because output is non-integer)!
    // TODO: vector & complex types
    switch(a.type->family)
    {
    case TF_BOOLEAN: return value_mkbool(fctx, !a.v.u);
    case TF_INTEGER:
        if(a.type->u.uinteger.is_signed)
            return value_mkbool(fctx, !a.v.i);
        else
            return value_mkbool(fctx, !a.v.u);
        assert(0);
    case TF_REAL:
        return value_mkbool(fctx, !a.v.r);
    case TF_CHARACTER:
        return value_mkbool(fctx, !a.v.u);
    case TF_OPTION:
        assert(0 && "TODO: !option");
    // illegal!
    case TF_VOID:
    case TF_STRUCT:
    case TF_CLASS:
    case TF_ARRAY:
    case TF_STRING:
    case TF_DICT:
    case TF_SET:
    case TF_DELEGATE:
    case TF_MIXED:
        return value_mkvoid(fctx);
    case TF_TYPE: assert(0 && "Called op_not() on a type");
    }
    assert(0);
    return value_mkvoid(fctx);
}
// ~a
Value value_op_bcom(FileContext* fctx, Value a)
{
    a = value_promote_numeric(fctx, a);
    // TODO: vector & complex types
    switch(a.type->family)
    {
    case TF_INTEGER:
        if(a.type->u.uinteger.is_signed)
        {
            switch(a.type->u.uinteger.width)
            {
            case 4: return value_mkint(fctx, ~a.v.i);
            case 8: return value_mklong(fctx, ~a.v.i);
            default: assert(0);
            }
        }
        else
        {
            switch(a.type->u.uinteger.width)
            {
            case 4: return value_mkuint(fctx, ~a.v.u);
            case 8: return value_mkulong(fctx, ~a.v.u);
            default: assert(0);
            }
        }
        assert(0);
    case TF_REAL: return value_mkvoid(fctx); // ~x is not allowed on reals
    default: assert(0);
    }
    assert(0);
    return value_mkvoid(fctx);
}

ADECL_BINARY(add,+)
ADECL_BINARY(sub,-)
ADECL_BINARY(mul,*)
static COYlong  value_bop_div__i(FileContext* fctx, COYlong a, COYlong b)   { if(!b) errorf(fctx, "Integer division by 0"); return a / b; }
static COYulong value_bop_div__u(FileContext* fctx, COYulong a, COYulong b) { if(!b) errorf(fctx, "Integer division by 0"); return a / b; }
static COYreal  value_bop_div__r(FileContext* fctx, COYreal a, COYreal b)   { return a / b; }
AFUNC_BINARY(div)
// TODO: special rules for rem/mod?
static COYlong  value_bop_rem__i(FileContext* fctx, COYlong a, COYlong b)   { if(!b) errorf(fctx, "Integer remainder by 0"); return a % b; }
static COYulong value_bop_rem__u(FileContext* fctx, COYulong a, COYulong b) { if(!b) errorf(fctx, "Integer remainder by 0"); return a % b; }
static COYreal  value_bop_rem__r(FileContext* fctx, COYreal a, COYreal b)   { return fmodl(a, b); }
AFUNC_BINARY(rem)
static COYlong  value_bop_mod__i(FileContext* fctx, COYlong a, COYlong b)   { if(!b) errorf(fctx, "Integer modulo by 0"); COYlong v = a % b; return v >= 0 ? v : v + b; }
static COYulong value_bop_mod__u(FileContext* fctx, COYulong a, COYulong b) { if(!b) errorf(fctx, "Integer modulo by 0"); return a % b; /* result can never be negative for unsigned */ }
static COYreal  value_bop_mod__r(FileContext* fctx, COYreal a, COYreal b)   { COYreal v = fmodl(a, b); return v >= 0 ? v : v + b; }
AFUNC_BINARY(mod)
static COYlong h__ipow(COYlong base, COYulong exp)
{
    COYlong result = 1;
    while(exp)
    {
        if(exp & 1) result *= base;
        exp >>= 1;
        base *= base;
    }
    return result;
}
static COYulong h__upow(COYulong base, COYulong exp)
{
    COYulong result = 1;
    while(exp)
    {
        if(exp & 1) result *= base;
        exp >>= 1;
        base *= base;
    }
    return result;
}
static COYlong  value_bop_pow__i(FileContext* fctx, COYlong a, COYlong b)   { if(!a && !b) errorf(fctx, "Integer 0 ** 0"); if(!a && b < 0) errorf(fctx, "Integer 0 ** negative"); if(!a) return 1; if(b < 0) return 0; return h__ipow(a, b); }
static COYulong value_bop_pow__u(FileContext* fctx, COYulong a, COYulong b) { if(!a && !b) errorf(fctx, "Integer 0 ** 0"); if(!a) return 1; return h__upow(a, b); }
static COYreal  value_bop_pow__r(FileContext* fctx, COYreal a, COYreal b)   { return powl(a, b); }
AFUNC_BINARY(pow)

BDECL_BINARY(bxor,^)
BDECL_BINARY(band,&)
BDECL_BINARY(bor,|)
// TODO: shift semantics for negative b?
static COYlong  value_bop_bshl__i(FileContext* fctx, COYlong a, COYlong b)   { b &= sizeof(a) * CHAR_BIT - 1; return a << b; }
static COYulong value_bop_bshl__u(FileContext* fctx, COYulong a, COYulong b) { b &= sizeof(a) * CHAR_BIT - 1; return a << b; }
BFUNC_BINARY(bshl)
static COYlong  value_bop_bsha__i(FileContext* fctx, COYlong a, COYlong b)   { b &= sizeof(a) * CHAR_BIT - 1; return (a & ((COYulong)1U << (sizeof(a) * CHAR_BIT - 1))) ? ~(~(COYulong)a >> b) : (COYulong)a >> b; }
static COYulong value_bop_bsha__u(FileContext* fctx, COYulong a, COYulong b) { b &= sizeof(a) * CHAR_BIT - 1; return a >> b; }
BFUNC_BINARY(bsha)
static COYlong  value_bop_bshr__i(FileContext* fctx, COYlong a, COYlong b)   { b &= sizeof(a) * CHAR_BIT - 1; return (COYulong)a >> b; }
static COYulong value_bop_bshr__u(FileContext* fctx, COYulong a, COYulong b) { b &= sizeof(a) * CHAR_BIT - 1; return a >> b; }
BFUNC_BINARY(bshr)

#define ROTMASK(T,b)    (b).type->u.uinteger.is_signed ? ((b).v.i & (sizeof(T) * CHAR_BIT - 1)) : ((b).v.u & (sizeof(T) * CHAR_BIT - 1))
#define ROTL(T,v,r)     ((r) ? (((COYulong)(v) << (r)) | ((COYulong)(v) >> (sizeof(T) * CHAR_BIT - (r)))) : (COYulong)(v))
#define ROTR(T,v,r)     ((r) ? (((COYulong)(v) >> (r)) | ((COYulong)(v) << (sizeof(T) * CHAR_BIT - (r)))) : (COYulong)(v))
Value value_op_brotl(FileContext* fctx, Value a, Value b)   // a<<|b
{
    b = value_promote_numeric(fctx, b);
    if(b.type->family != TF_INTEGER) return value_mkvoid(fctx);

    COYubyte rot;
    switch(a.type->family)
    {
    case TF_BOOLEAN:
        rot = ROTMASK(COYubyte,b);
        return value_mkubyte(fctx, ROTL(COYubyte,a.v.u,rot));
    case TF_INTEGER:
        if(a.type->u.uinteger.is_signed)
        {
            switch(a.type->u.uinteger.width)
            {
            case 1: rot = ROTMASK(COYbyte,b); return value_mkbyte(fctx, ROTL(COYbyte,a.v.i,rot));
            case 2: rot = ROTMASK(COYshort,b); return value_mkshort(fctx, ROTL(COYshort,a.v.i,rot));
            case 4: rot = ROTMASK(COYint,b); return value_mkint(fctx, ROTL(COYint,a.v.i,rot));
            case 8: rot = ROTMASK(COYlong,b); return value_mklong(fctx, ROTL(COYlong,a.v.i,rot));
            default: assert(0);
            }
        }
        else
        {
            switch(a.type->u.uinteger.width)
            {
            case 1: rot = ROTMASK(COYubyte,b); return value_mkubyte(fctx, ROTL(COYubyte,a.v.u,rot));
            case 2: rot = ROTMASK(COYushort,b); return value_mkushort(fctx, ROTL(COYushort,a.v.u,rot));
            case 4: rot = ROTMASK(COYuint,b); return value_mkuint(fctx, ROTL(COYuint,a.v.u,rot));
            case 8: rot = ROTMASK(COYulong,b); return value_mkulong(fctx, ROTL(COYulong,a.v.u,rot));
            default: assert(0);
            }
        }
        assert(0);
    case TF_CHARACTER:
        switch(a.type->u.ucharacter.width)
        {
        case 1: rot = ROTMASK(COYchar,b); return value_mkchar(fctx, ROTL(COYchar,a.v.u,rot));
        case 2: rot = ROTMASK(COYwchar,b); return value_mkwchar(fctx, ROTL(COYwchar,a.v.u,rot));
        case 4: rot = ROTMASK(COYdchar,b); return value_mkdchar(fctx, ROTL(COYdchar,a.v.u,rot));
        default: assert(0);
        }
        assert(0);
    // illegal!
    case TF_VOID:
    case TF_REAL:
    case TF_STRUCT:
    case TF_CLASS:
    case TF_ARRAY:
    case TF_STRING:
    case TF_DICT:
    case TF_SET:
    case TF_DELEGATE:
    case TF_OPTION:
    case TF_MIXED:
        return value_mkvoid(fctx);
    case TF_TYPE: assert(0 && "Called op_brotl() on a type");
    default: assert(0);
    }
    assert(0);
    return value_mkvoid(fctx);
}
Value value_op_brotr(FileContext* fctx, Value a, Value b)   // a|>>b
{
    b = value_promote_numeric(fctx, b);
    if(b.type->family != TF_INTEGER) return value_mkvoid(fctx);

    COYubyte rot;
    switch(a.type->family)
    {
    case TF_BOOLEAN:
        rot = ROTMASK(COYubyte,b);
        return value_mkubyte(fctx, ROTR(COYubyte,a.v.u,rot));
    case TF_INTEGER:
        if(a.type->u.uinteger.is_signed)
        {
            switch(a.type->u.uinteger.width)
            {
            case 1: rot = ROTMASK(COYbyte,b); return value_mkbyte(fctx, ROTR(COYbyte,a.v.i,rot));
            case 2: rot = ROTMASK(COYshort,b); return value_mkshort(fctx, ROTR(COYshort,a.v.i,rot));
            case 4: rot = ROTMASK(COYint,b); return value_mkint(fctx, ROTR(COYint,a.v.i,rot));
            case 8: rot = ROTMASK(COYlong,b); return value_mklong(fctx, ROTR(COYlong,a.v.i,rot));
            default: assert(0);
            }
        }
        else
        {
            switch(a.type->u.uinteger.width)
            {
            case 1: rot = ROTMASK(COYubyte,b); return value_mkubyte(fctx, ROTR(COYubyte,a.v.u,rot));
            case 2: rot = ROTMASK(COYushort,b); return value_mkushort(fctx, ROTR(COYushort,a.v.u,rot));
            case 4: rot = ROTMASK(COYuint,b); return value_mkuint(fctx, ROTR(COYuint,a.v.u,rot));
            case 8: rot = ROTMASK(COYulong,b); return value_mkulong(fctx, ROTR(COYulong,a.v.u,rot));
            default: assert(0);
            }
        }
        assert(0);
    case TF_CHARACTER:
        switch(a.type->u.ucharacter.width)
        {
        case 1: rot = ROTMASK(COYchar,b); return value_mkchar(fctx, ROTR(COYchar,a.v.u,rot));
        case 2: rot = ROTMASK(COYwchar,b); return value_mkwchar(fctx, ROTR(COYwchar,a.v.u,rot));
        case 4: rot = ROTMASK(COYdchar,b); return value_mkdchar(fctx, ROTR(COYdchar,a.v.u,rot));
        default: assert(0);
        }
        assert(0);
    // illegal!
    case TF_VOID:
    case TF_REAL:
    case TF_STRUCT:
    case TF_CLASS:
    case TF_ARRAY:
    case TF_STRING:
    case TF_DICT:
    case TF_SET:
    case TF_DELEGATE:
    case TF_OPTION:
    case TF_MIXED:
        return value_mkvoid(fctx);
    case TF_TYPE: assert(0 && "Called op_brotr() on a type");
    default: assert(0);
    }
    assert(0);
    return value_mkvoid(fctx);
}

Value value_op_cat(FileContext* fctx, Value a, Value b);    // a~b
Value value_op_in(FileContext* fctx, Value a, Value b);     // a in b
Value value_op_nin(FileContext* fctx, Value a, Value b);    // a !in b
Value value_op_is(FileContext* fctx, Value a, Value b);     // a is b
Value value_op_nis(FileContext* fctx, Value a, Value b);    // a !is b

Value value_op_cmp(FileContext* fctx, Value a, Value b);    // a <=> b; a < b; a == b; a != b; a <= b; a > b; a >= b

static void value_dumpDBG_char(COYdchar c, COYchar quot)
{
    if(c == quot || c == '\\')
        printf("\\%c", c);
    else if(' ' <= c && c <= 0x7F)
        printf("%c", c);
    else if(c <= 0xFF)
        printf("\\x%.2" PRIx32, c);
    else if(c <= 0xFFFF)
        printf("\\u%.4" PRIx32, c);
    else
        printf("\\U%.8" PRIx32, c);
}
void value_dumpDBG(Value a)
{
    // TODO: vector & complex types
    COYusize i;
    switch(a.type->family)
    {
    case TF_VOID:
        return;
    case TF_BOOLEAN:
        printf("%s", a.v.u ? "true" : "false");
        return;
    case TF_INTEGER:
        if(a.type->u.uinteger.is_signed)
        {
            printf("%" PRId64 " @%u", a.v.i, a.type->u.uinteger.width);
        }
        else
            printf("%" PRIu64 "U @%u", a.v.u, a.type->u.uinteger.width);
        return;
    case TF_CHARACTER:
        switch(a.type->u.ucharacter.width)
        {
        case 1: printf("'"); value_dumpDBG_char(a.v.u, '\''); printf("'c"); return;
        case 2: printf("'"); value_dumpDBG_char(a.v.u, '\''); printf("'w"); return;
        case 4: printf("'"); value_dumpDBG_char(a.v.u, '\''); printf("'d"); return;
        default: assert(0);
        }
        assert(0);
    case TF_REAL:
        printf("%lf", a.v.r);
        switch(a.type->u.ureal.width)
        {
        case 4: printf("f"); return;
        case 8: printf("d"); return;
        case 16: printf("r"); return;
        default: assert(0);
        }
        assert(0);
    case TF_STRUCT:
        printf("<struct>");
        return;
    case TF_CLASS:
        printf("<class>");
        return;
    case TF_ARRAY:
        printf("<array>");
        return;
    case TF_STRING:
        printf("\"");
        for(i = 0; i < a.v.istr->len; i++)
            value_dumpDBG_char(a.v.istr->ptr[i], '"');
        printf("\"");
        if(!a.type->u.ustring.val)
        { /* nothing */ }
        else if(a.type->u.ustring.val == &Tchar)
            printf("c");
        else if(a.type->u.ustring.val == &Twchar)
            printf("w");
        else if(a.type->u.ustring.val == &Tdchar)
            printf("d");
        else
            assert(0);
        return;
    case TF_DICT:
        printf("<dict>");
        return;
    case TF_SET:
        printf("<set>");
        return;
    case TF_DELEGATE:
        printf("<delegate>");
        return;
    case TF_OPTION:
        printf("<option>");
        return;
    case TF_MIXED:
        printf("<mixed>");
        return;
    case TF_TYPE: assert(0 && "Called op_brotr() on a type");
    default: assert(0);
    }
    assert(0);
}
