#ifndef UTIL_VALUE_H_
#define UTIL_VALUE_H_

#include <filecontext.h>
#include <util/types.h>
#include <util/type.h>
#include <util/istring.h>
#include <util/chints.h>

typedef struct Value
{
    Type* type;
    union
    {
        IString* istr;
        COYlong i;
        COYulong u;
        COYreal r;
        Type* type;
    } v;
} Value;

Value value_mkvoid(FileContext* fctx);
Value value_mkbool(FileContext* fctx, COYbool b);

Value value_mkbyte(FileContext* fctx, COYbyte i);
Value value_mkubyte(FileContext* fctx, COYubyte u);
Value value_mkshort(FileContext* fctx, COYshort i);
Value value_mkushort(FileContext* fctx, COYushort u);
Value value_mkint(FileContext* fctx, COYint i);
Value value_mkuint(FileContext* fctx, COYuint u);
Value value_mklong(FileContext* fctx, COYlong i);
Value value_mkulong(FileContext* fctx, COYulong u);

Value value_mkfloat(FileContext* fctx, COYfloat r);
Value value_mkdouble(FileContext* fctx, COYdouble r);
Value value_mkreal(FileContext* fctx, COYreal r);

Value value_mkchar(FileContext* fctx, COYchar c);
Value value_mkwchar(FileContext* fctx, COYwchar c);
Value value_mkdchar(FileContext* fctx, COYdchar c);

Value value_mkstring(FileContext* fctx, const COYchar* str, int len);
/*Value value_mkwstring(FileContext* fctx, const COYwchar* str, int len);
Value value_mkdstring(FileContext* fctx, const COYdchar* str, int len);*/

Value value_mktype(FileContext* fctx, Type* type);

#define value_mksymbol value_mkstring

COYusize value_strlen(Value* a);
COYbool value_streq(Value* a, Value* b);

Value value_op_icast(FileContext* fctx, Type* T, Value a);
Value value_op_cast(FileContext* fctx, Type* T, Value a);
Value value_op_bitcast(FileContext* fctx, Type* T, Value a);

Value value_promote_numeric(FileContext* fctx, Value a);
COYbool value_promote_binary(FileContext* fctx, Value* a, Value* b);

struct OpUnary;
Value value_op_arith_unary(FileContext* fctx, Value a, const struct OpUnary* ops);
struct OpBinary;
Value value_op_arith_binary(FileContext* fctx, Value a, Value b, const struct OpBinary* ops);

Value value_op_pos(FileContext* fctx, Value a);     // +a
Value value_op_neg(FileContext* fctx, Value a);     // -a
Value value_op_not(FileContext* fctx, Value a);     // !a
Value value_op_bcom(FileContext* fctx, Value a);    // ~a

Value value_op_add(FileContext* fctx, Value a, Value b);    // a+b
Value value_op_sub(FileContext* fctx, Value a, Value b);    // a-b
Value value_op_mul(FileContext* fctx, Value a, Value b);    // a*b
Value value_op_div(FileContext* fctx, Value a, Value b);    // a/b
Value value_op_rem(FileContext* fctx, Value a, Value b);    // a%b
Value value_op_mod(FileContext* fctx, Value a, Value b);    // a%%b
Value value_op_pow(FileContext* fctx, Value a, Value b);    // a**b

Value value_op_bxor(FileContext* fctx, Value a, Value b);   // a^b
Value value_op_band(FileContext* fctx, Value a, Value b);   // a&b
Value value_op_bor(FileContext* fctx, Value a, Value b);    // a|b
Value value_op_bshl(FileContext* fctx, Value a, Value b);   // a<<b
Value value_op_bsha(FileContext* fctx, Value a, Value b);   // a>>b
Value value_op_bshr(FileContext* fctx, Value a, Value b);   // a>>>b
Value value_op_brotl(FileContext* fctx, Value a, Value b);  // a<<|b
Value value_op_brotr(FileContext* fctx, Value a, Value b);  // a|>>b

Value value_op_cat(FileContext* fctx, Value a, Value b);    // a~b
Value value_op_in(FileContext* fctx, Value a, Value b);     // a in b
Value value_op_nin(FileContext* fctx, Value a, Value b);    // a !in b
Value value_op_is(FileContext* fctx, Value a, Value b);     // a is b
Value value_op_nis(FileContext* fctx, Value a, Value b);    // a !is b

Value value_op_cmp(FileContext* fctx, Value a, Value b);    // a <=> b; a < b; a == b; a != b; a <= b; a > b; a >= b

void value_dumpDBG(Value a);

#endif /* UTIL_VALUE_H_ */
