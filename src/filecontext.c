#include <filecontext.h>

#include <stdlib.h>
#include <string.h>

static char InputFName[] = "<input>";

FileContext* filecontext_init(FileContext* fctx, const char* fname)
{
    if(!fctx) return NULL;
    size_t fnlen = fname ? strlen(fname) : 0;
    if(fname)
    {
        fctx->fname = malloc(fnlen + 1);
        memcpy(fctx->fname, fname, fnlen + 1);
    }
    else
        fctx->fname = InputFName;
    table_init(&fctx->itab);
    return fctx;
}
void filecontext_deinit(FileContext* fctx)
{
    if(!fctx) return;
    table_deinit(&fctx->itab);
    if(fctx->fname != InputFName)
        free(fctx->fname);
}
