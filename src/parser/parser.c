#include <parser/parser.h>

#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>

#include <assert.h>

//#define DEBUG_TRACE

#ifdef DEBUG_TRACE
int shiftc = 0;
#define SHIFT(kind) do { if(++shiftc > PARSER_LOOKAHEAD) printf("SHIFT %s\n", TokenKindNames[(kind)]); } while(0)
#define REDUCE(str) printf("REDUCE %s: %s\n", __func__, (str))
//#define TRACE()     printf("TRACE %s (%u,%u) %s\n", __func__, par->lex->pos.line, par->lex->pos.col, TokenKindNames[par->lex->tok.kind])
#define TRACE()     printf("TRACE %s\n", __func__)
#else
#define SHIFT(kind)
#define REDUCE(str)
#define TRACE()
#endif

// TODO: move to appropriate place
static void verrorf(Parser* parser, const char* format, va_list args)
{
    FilePos pos = parser->tokens[parser->pos].range.head;
    fprintf(stderr, "PARSER ERROR(%u:%u): ", (unsigned int)(pos.line + 1), (unsigned int)(pos.col + 1));
    vfprintf(stderr, format, args);
    fprintf(stderr, "\n");
    fflush(stderr);
    longjmp(parser->jbufs[parser->jtop-1], 1);
}
static void errorf(Parser* parser, const char* format, ...)
{
    va_list args;
    va_start(args, format);
    verrorf(parser, format, args);
    va_end(args);
}
static void error_expect(Parser* parser, const char* expect)
{
    errorf(parser, "Found %s, expected %s", TokenKindNames[parser->tokens[parser->pos].kind], expect);
}

static COYusize parser__pushbuf(Parser* par)
{
    //printf("PUSH %u => %u\n", (unsigned)(par->jtop), (unsigned)(par->jtop+1));
    par->jbufs = realloc(par->jbufs, ++par->jtop * sizeof(*par->jbufs));
    return par->jtop - 1U;
}
static COYusize parser__setbuftop(Parser* par, COYusize ntop)
{
    //printf("SET %u => %u\n", (unsigned)(par->jtop), (unsigned)ntop);
    assert(ntop <= par->jtop);
    par->jtop = ntop;
    return ntop - 1U;
}
/*static COYusize parser__popbuf(Parser* par)
{
    return parser__setbuftop(par, par->jtop - 1);
}*/
static TokenKind parser__peekk(Parser* par, int k)
{
    if(k < 0 && par->pos < -k) return TK_UNKNOWN;
    if(par->ntokens - par->pos < k) return TK_UNKNOWN;
    return par->tokens[par->pos+k].kind;
}
static Token parser__shift(Parser* par, TokenKind kind)
{
    if(par->tokens[par->pos].kind != kind) longjmp(par->jbufs[par->jtop-1], 1);
    return par->tokens[par->pos++];
}

#define RMPARENS_(...)    __VA_ARGS__

#define ALTS            { COYusize ALTStop = parser__pushbuf(par); COYusize ALTSpos = par->pos; if(0) {
#define ENDALTS         } else { /*printf("**** %s %u\n", __func__, __LINE__);*/ ALTStop = parser__setbuftop(par, ALTStop); par->pos = ALTSpos; longjmp(par->jbufs[ALTStop], 1); } ALTStop = parser__setbuftop(par, ALTStop); }
#define ALTERNATIVE     } else if(!setjmp(par->jbufs[ALTStop])) { par->pos = ALTSpos;

#define REPEATHELPER_(TYPE,NAME,EXPRA,EXPRB,CHK)                               \
        TYPE NAME;                                                             \
        ALTS                                                                   \
        ALTERNATIVE { RMPARENS_ EXPRA; }                                       \
        ALTERNATIVE { RMPARENS_ EXPRB; }                                       \
        ENDALTS                                                                \
        if(!(CHK)) break;
#define REPEATHELPER_TOK_(NAME,TKIND)   REPEATHELPER_(Token,NAME,(NAME = parser__shift(par,TKIND)),(NAME.kind = TK_UNKNOWN),NAME.kind)
#define REPEATHELPER_NODE(NAME,PFUNC)   REPEATHELPER_(ANode*,NAME,(NAME = parser__getnode(par,PFUNC)),(NAME),NAME)

#define REPEAT0TOK(NAME,TKIND)      for(;;) { REPEATHELPER_TOK_(NAME,TKIND)
#define REPEAT0NODE(NAME,PFUNC)     for(;;) { REPEATHELPER_NODE_(NAME,PFUNC)
#define REPEAT0END                  }

#define REPEAT1                     for(;;) {
#define REPEAT1ENDTOK(NAME,TKIND)   REPEATHELPER_TOK_(NAME,TKIND) }
#define REPEAT1ENDNODE(NAME,PFUNC)  REPEATHELPER_NODE_(NAME,PFUNC) }

typedef ANode* ParseFunction(Parser* par);
typedef struct ParserCacheEntry
{
    struct ParserCacheEntry* next;
    COYulong hash;
    COYusize pos;
    ParseFunction* func;
    ANode* node;
    COYusize endpos;
} ParserCacheEntry;

static ParserCache* pcache__init(ParserCache* pcache)
{
    if(!pcache) return NULL;
    pcache->count = 0;
    pcache->mem = PARSERCACHE_INITSIZE;
    pcache->ents = malloc(pcache->mem * sizeof(*pcache->ents));
    size_t i;
    for(i = 0; i < pcache->mem; i++)
        pcache->ents[i] = NULL;
    return pcache;
}
/*static void pcache__deinit(ParserCache* pcache)
{
    if(!pcache) return;

    size_t i;
    for(i = 0; i < pcache->mem; i++)
    {
        while(pcache->ents[i])
        {
            ParserCacheEntry* curr = pcache->ents[i];
            pcache->ents[i] = curr->next;
            free(curr);
        }
    }
    free(pcache->ents);
}*/

static COYulong h__hash_ulong(COYulong x)
{
    x = (x ^ (x >> 30)) * UINT64_C(0xbf58476d1ce4e5b9);
    x = (x ^ (x >> 27)) * UINT64_C(0x94d049bb133111eb);
    x = x ^ (x >> 31);
    return x;
}
static COYulong h__hash_arr(const COYulong* parts, COYusize nparts)
{
    COYulong hash = 5381;
    while(nparts--)
        hash = ((hash << 5) + hash) ^ *parts++;
    return hash;
}
static COYulong pcache__hash(COYusize pos, ParseFunction* func)
{
    COYulong hashes[] = { h__hash_ulong(pos), h__hash_ulong((COYuintptr)func) };
    return h__hash_arr(hashes, sizeof(hashes) / sizeof(*hashes));
}

static ParserCacheEntry* pcache__findentry_arr(ParserCacheEntry* ent, COYulong hash, COYusize pos, ParseFunction* func)
{
    while(ent)
    {
        if(ent->pos == pos && ent->func == func) break;
        ent = ent->next;
    }
    return ent;
}
static ParserCacheEntry* pcache__getentry(ParserCache* pcache, COYusize pos, ParseFunction* func, Parser* par)
{
    static ANode ErrorGuard;

    COYulong hash = pcache__hash(pos, func);
    COYulong slot = hash & (pcache->mem - 1);

    ParserCacheEntry* ent = pcache__findentry_arr(pcache->ents[slot], hash, pos, func);
    if(ent)
    {
        if(ent->node == &ErrorGuard) longjmp(par->jbufs[par->jtop-1], 1);
        par->pos = ent->endpos;
        return ent;
    }

    ANode* node;
    ALTS
    ALTERNATIVE
    {
        node = func(par);
    }
    ALTERNATIVE
    {
        node = &ErrorGuard;
    }
    ENDALTS

    // if above desired fill rate ...
    if(pcache->count >= pcache->mem * PARSERCACHE_RATIO_NUM / PARSERCACHE_RATIO_DEN)
    {
        COYusize nmem = pcache->mem <<= 1;
        ParserCacheEntry** nents = malloc(nmem * sizeof(*pcache->ents));
        size_t i;
        for(i = 0; i < nmem; i++)
            nents[i] = NULL;

        for(i = 0; i < pcache->mem; i++)
        {
            ParserCacheEntry* next;
            ParserCacheEntry* curr;
            for(curr = pcache->ents[i]; curr; curr = next)
            {
                next = curr;
                COYulong nslot = curr->hash & (nmem - 1);
                curr->next = nents[nslot];
                nents[nslot] = curr->next;
            }
        }

        free(pcache->ents);
        pcache->mem = nmem;
        pcache->ents = nents;

        slot = hash & (nmem - 1);
    }

    ent = malloc(sizeof(ParserCacheEntry));
    ent->next = pcache->ents[slot];
    pcache->ents[slot] = ent;
    ent->hash = hash;
    ent->pos = pos;
    ent->func = func;
    ent->node = node;
    ent->endpos = par->pos;
    if(node == &ErrorGuard) longjmp(par->jbufs[par->jtop-1], 1);
    return ent;
}

// (par->pos,func) => ANode*
ANode* parser__getnode(Parser* par, ParseFunction* func)
{
    return pcache__getentry(&par->pcache, par->pos, func, par)->node;
}

Parser* parser_init(Parser* parser, Lexer* lexer, FileContext* fctx)
{
    if(!parser) return NULL;
    parser->fctx = fctx;
    parser->lexer = lexer;

    parser->ntokens = 0;
    parser->tokens = NULL;
    parser->pos = 0;

    parser->jtop = 0;
    parser->jbufs = NULL;

    pcache__init(&parser->pcache);

    return parser;
}

static ANode* p_program(Parser* par);
static ANode* p_module_stat(Parser* par);
static ANode* p_var_decl_stat(Parser* par);
static ANode* p_nempty_decl_stat(Parser* par);
static ANode* p_decl_stat(Parser* par);
static ANode* p_attrib_part(Parser* par);
static ANode* p_var_decl_part(Parser* par);
static ANode* p_block_stat(Parser* par);
static ANode* p_nempty_stat(Parser* par);
static ANode* p_stat(Parser* par);
static ANode* p_assign_expr_list(Parser* par);
static ANode* p_assign_expr(Parser* par);
static ANode* p_expr_list(Parser* par);
static ANode* p_expr(Parser* par);
static ANode* p_binary_expr(Parser* par);
static ANode* p_call_expr(Parser* par);
static ANode* p_value(Parser* par);
static ANode* p_array(Parser* par);
static ANode* p_array_member_list(Parser* par);
static ANode* p_array_member(Parser* par);
static ANode* p_dotted_id(Parser* par);
static ANode* p_base_type(Parser* par);
static ANode* p_type(Parser* par);

/*
program
    :   module_stat? decl_stat*
    ;
*/
static ANode* p_program(Parser* par)
{
    TRACE();
    ANode* node = NULL;
    int mustdup = 1;
    ALTS
    ALTERNATIVE
    {
        node = parser__getnode(par, p_module_stat);
    }
    ALTERNATIVE
    {
        node = anode_create_kind(par->fctx, TK_MODULE);
        anode_append_chi(node, NULL);
        mustdup = 0;
    }
    ENDALTS
    assert(node);

    for(;;)
    {
        ANode* decl;
        ALTS
        ALTERNATIVE
        {
            decl = parser__getnode(par, p_decl_stat);
        }
        ALTERNATIVE
        {
            decl = NULL;
        }
        ENDALTS
        if(!decl) break;

        if(mustdup)
        {
            mustdup = 0;
            node = anode_create_dup(par->fctx, node);
        }
        anode_append_chi(node, decl);
    }
    assert(node);
    REDUCE(/*TokenKindNames[node->token.kind]*/"");
    return node;
}
/*
module_stat
    :   MODULE $name=dotted_id SCOLON           -> (MODULE $name)
    ;
*/
static ANode* p_module_stat(Parser* par)
{
    TRACE();
    Token module = parser__shift(par, TK_MODULE);
    ANode* name = parser__getnode(par, p_dotted_id);
    parser__shift(par, TK_SCOLON);

    REDUCE(TokenKindNames[module.kind]);
    return anode_create_tokenchi(par->fctx, module, name, ANODE_ENDLIST);
}

static ANode* p_var_decl_stat(Parser* par)
{
    TRACE();
    ANode* type = parser__getnode(par, p_type);
    ANode* part = parser__getnode(par, p_var_decl_part);

    ANode* node = anode_create_kindchi(par->fctx, TKA_VARDECL, type, part, ANODE_ENDLIST);
    for(;;)
    {
        Token comma;
        ALTS
        ALTERNATIVE
        {
            comma = parser__shift(par, TK_COMMA);
        }
        ALTERNATIVE
        {
            comma.kind = TK_UNKNOWN;
        }
        ENDALTS
        if(!comma.kind) break;
        part = parser__getnode(par, p_var_decl_part);

        anode_append_chi(node, part);
    }
    parser__shift(par, TK_SCOLON);
    REDUCE(node ? TokenKindNames[node->token.kind] : "<null>");
    return node;
}
/*
nempty_decl_stat
    :   $attrib=attrib_part (COLON $decls=decl_stat* / LBRACE $decls=decl_stat* RBRACE / $decls=decl_stat)      -> (I_ATTRIB $attrib $decls...)
    /   var_decl_stat
    /   $rtype=type $name=dotted_id LPAREN ($params~=func_decl_param (COMMA $params~=func_decl_param)*)? RPAREN ( SCOLON / $body=block_stat )  ->  (I_FUNDECL $name (TYPE $rtype (I_LIST $params[0]...)) (I_LIST $params[1]...) $body)
    ;
*/
static ANode* p_nempty_decl_stat(Parser* par)
{
    TRACE();
    ANode* node;
    int mustdup;
    ALTS
    ALTERNATIVE
    {
        node = parser__getnode(par, p_attrib_part);
        mustdup = 1;

        ALTS
        ALTERNATIVE
        {
            parser__shift(par, TK_COLON);
            for(;;)
            {
                ANode* decl;
                ALTS
                ALTERNATIVE
                {
                    decl = parser__getnode(par, p_decl_stat);
                }
                ALTERNATIVE
                {
                    decl = NULL;
                }
                ENDALTS
                if(!decl) break;

                if(mustdup)
                {
                    mustdup = 0;
                    node = anode_create_dup(par->fctx, node);
                }
                anode_append_chi(node, decl);
            }
        }
        ALTERNATIVE
        {
            parser__shift(par, TK_LBRACE);
            for(;;)
            {
                ANode* decl;
                ALTS
                ALTERNATIVE
                {
                    decl = parser__getnode(par, p_decl_stat);
                }
                ALTERNATIVE
                {
                    decl = NULL;
                }
                ENDALTS
                if(!decl) break;

                if(mustdup)
                {
                    mustdup = 0;
                    node = anode_create_dup(par->fctx, node);
                }
                anode_append_chi(node, decl);
            }
            parser__shift(par, TK_RBRACE);
        }
        ALTERNATIVE
        {
            ANode* decl = parser__getnode(par, p_decl_stat);
            if(mustdup)
            {
                mustdup = 0;
                node = anode_create_dup(par->fctx, node);
            }
            anode_append_chi(node, decl);
        }
        ENDALTS
    }
    ALTERNATIVE
    {
        node = parser__getnode(par, p_var_decl_stat);
    }
    ALTERNATIVE
    {
        ANode* rtype = parser__getnode(par, p_type);
        ANode* name = parser__getnode(par, p_dotted_id);
        parser__shift(par, TK_LPAREN);
        ANode* ptypes = anode_create_kind(par->fctx, TKA_LIST);
        ANode* pnames = anode_create_kind(par->fctx, TKA_LIST);
        ALTS
        ALTERNATIVE
        {
            ANode* ptype = parser__getnode(par, p_type);
            ANode* pname;
            ALTS
            ALTERNATIVE
            {
                pname = anode_create_token(par->fctx, parser__shift(par, TK_IDENT));
            }
            ALTERNATIVE
            {
                pname = NULL;
            }
            ENDALTS
            anode_append_chi(ptypes, ptype);
            anode_append_chi(pnames, pname);
            for(;;)
            {
                Token comma;
                ALTS
                ALTERNATIVE
                {
                    comma = parser__shift(par, TK_COMMA);
                }
                ALTERNATIVE
                {
                    comma.kind = TK_UNKNOWN;
                }
                ENDALTS
                if(!comma.kind) break;

                ptype = parser__getnode(par, p_type);
                ALTS
                ALTERNATIVE
                {
                    pname = anode_create_token(par->fctx, parser__shift(par, TK_IDENT));
                }
                ALTERNATIVE
                {
                    pname = NULL;
                }
                ENDALTS
                anode_append_chi(ptypes, ptype);
                anode_append_chi(pnames, pname);
            }
        }
        ALTERNATIVE
        {
            /* no params */
        }
        ENDALTS
        parser__shift(par, TK_RPAREN);

        ANode* body;
        ALTS
        ALTERNATIVE
        {
            parser__shift(par, TK_SCOLON);
            body = NULL;
        }
        ALTERNATIVE
        {
            body = parser__getnode(par, p_block_stat);
        }
        ENDALTS

        ANode* type = anode_create_kindchi(par->fctx, TK_DELEGATE, rtype, ptypes, ANODE_ENDLIST);
        node = anode_create_kindchi(par->fctx, TKA_FUNDECL, name, type, pnames, body, ANODE_ENDLIST);
    }
    ENDALTS
    REDUCE(node ? TokenKindNames[node->token.kind] : "<null>");
    return node;
}
/*
decl_stat
    :   nempty_decl_stat
    /   SCOLON
    ;
*/
static ANode* p_decl_stat(Parser* par)
{
    TRACE();
    ANode* node;
    ALTS
    ALTERNATIVE
    {
        node = parser__getnode(par, p_nempty_decl_stat);
    }
    ALTERNATIVE
    {
        parser__shift(par, TK_SCOLON);
        node = NULL;
    }
    ENDALTS
    REDUCE(node ? TokenKindNames[node->token.kind] : "<null>");
    return node;
}
/*
    attrib_part
        :   PUBLIC / PROTECTED / PRIVATE / PACKAGE
        /   NATIVE / FINAL / ABSTRACT / STATIC / PROPERTY / OVERRIDE
        /   PRAGMA LPAREN $id=dotted_id (COMMA $exprs~=expr)* RPAREN    ->  (PRAGMA $id $exprs...)
        /   ALIGN LPAREN $v=(expr / type / DEFAULT) RPAREN  ->  (ALIGN $v)
        ;
*/
static ANode* p_attrib_part(Parser* par)
{
    TRACE();
    ANode* node = NULL;
    Token attrib;
    ALTS
    /*ALTERNATIVE { attrib = parser__shift(par, TK_PUBLIC); }
    ALTERNATIVE { attrib = parser__shift(par, TK_PROTECTED); }
    ALTERNATIVE { attrib = parser__shift(par, TK_PRIVATE); }
    ALTERNATIVE { attrib = parser__shift(par, TK_PACKAGE); }*/
    ALTERNATIVE { attrib = parser__shift(par, TK_NATIVE); }
    ALTERNATIVE { attrib = parser__shift(par, TK_FINAL); }
    ALTERNATIVE { attrib = parser__shift(par, TK_ABSTRACT); }
    ALTERNATIVE { attrib = parser__shift(par, TK_STATIC); }
    ALTERNATIVE { attrib = parser__shift(par, TK_PROPERTY); }
    ALTERNATIVE { attrib = parser__shift(par, TK_OVERRIDE); }
    ALTERNATIVE
    {
        attrib = parser__shift(par, TK_PRAGMA);
        parser__shift(par, TK_LPAREN);
        ANode* id = parser__getnode(par, p_dotted_id);
        node = anode_create_tokenchi(par->fctx, attrib, id, ANODE_ENDLIST);
        for(;;)
        {
            Token comma;
            ALTS
            ALTERNATIVE
            {
                comma = parser__shift(par, TK_COMMA);
            }
            ALTERNATIVE
            {
                comma.kind = TK_UNKNOWN;
            }
            ENDALTS
            if(!comma.kind) break;

            ANode* expr = parser__getnode(par, p_expr);
            anode_append_chi(node, expr);
        }
        parser__shift(par, TK_RPAREN);
    }
    /*ALTERNATIVE
    {
        attrib = parser__shift(par, TK_ALIGN);
        parser__shift(par, TK_LPAREN);
        ANode* val;
        ALTS
        ALTERNATIVE
        {
            val = parser__getnode(par, p_expr);
        }
        ALTERNATIVE
        {
            val = parser__getnode(par, p_type);
        }
        ALTERNATIVE
        {
            val = anode_create_token(par->fctx, parser__shift(par, TK_DEFAULT));
        }
        ENDALTS
        parser__shift(par, TK_RPAREN);

        node = anode_create_tokenchi(par->fctx, attrib, val, ANODE_ENDLIST);
    }*/
    ENDALTS
    if(!node) node = anode_create_token(par->fctx, attrib);
    REDUCE(node ? TokenKindNames[node->token.kind] : "<null>");
    return node;
}
/*
    var_decl_part
        :   $name=IDENT (OPASSIGN $value=expr)?     -> (I_PAIR $name $value)
        ;
*/
static ANode* p_var_decl_part(Parser* par)
{
    TRACE();
    Token name = parser__shift(par, TK_IDENT);
    ANode* value;

    Token assign;
    ALTS
    ALTERNATIVE
    {
        assign = parser__shift(par, TK_EQ);
    }
    ALTERNATIVE
    {
        assign.kind = TK_UNKNOWN;
    }
    ENDALTS
    if(!assign.kind) value = NULL;
    else value = parser__getnode(par, p_expr);

    REDUCE(TokenKindNames[name.kind]);
    return anode_create_tokenchi(par->fctx, name, value, ANODE_ENDLIST);
}


/*block_stat
    :   LBRACE $body=stat* RBRACE -> (I_BLOCK $body...)
    ;
*/
static ANode* p_block_stat(Parser* par)
{
    TRACE();
    Token lbrace = parser__shift(par, TK_LBRACE);
    ANode* node = anode_create_token(par->fctx, lbrace);
    for(;;)
    {
        ANode* stat;
        ALTS
        ALTERNATIVE
        {
            stat = parser__getnode(par, p_stat);
        }
        ALTERNATIVE
        {
            stat = NULL;
        }
        ENDALTS
        if(!stat) break;

        anode_append_chi(node, stat);
    }
    parser__shift(par, TK_RBRACE);
    REDUCE(node ? TokenKindNames[node->token.kind] : "<null>");
    return node;
}
/*
nempty_stat
    :   nempty_decl_stat
    /   block_stat
    /   IF LPAREN $cond=expr RPAREN $iftrue=nempty_stat (ELSE $iffalse=nempty_stat)?    ->  (IF $cond $iftrue $iffalse)
    /   WHILE LPAREN $cond=expr RPAREN $body=nempty_stat            ->  (WHILE $cond $body)
    /   DO $body=nempty_stat WHILE LPAREN $cond=expr RPAREN SCOLON  ->  (DO $cond $body)
    /   FOR LPAREN $init=(var_decl_stat / $list=assign_expr_list SCOLON -> $list / SCOLON ->) $cond=expr? SCOLON $next=assign_expr_list? RPAREN $body=nempty_stat     ->  (FOR $init $cond $next $body)
    /   RETURN expr_list? SCOLON
    /   assign_expr_list SCOLON
    ;
*/
static ANode* p_nempty_stat(Parser* par)
{
    TRACE();
    ANode* node;
    ALTS
    ALTERNATIVE
    {
        node = parser__getnode(par, p_nempty_decl_stat);
    }
    ALTERNATIVE
    {
        node = parser__getnode(par, p_block_stat);
    }
    ALTERNATIVE
    {
        Token ift = parser__shift(par, TK_IF);
        parser__shift(par, TK_LPAREN);
        ANode* cond = parser__getnode(par, p_expr);
        parser__shift(par, TK_RPAREN);
        ANode* iftrue = parser__getnode(par, p_nempty_stat);

        Token elset;
        ALTS
        ALTERNATIVE
        {
            elset = parser__shift(par, TK_ELSE);
        }
        ALTERNATIVE
        {
            elset.kind = TK_UNKNOWN;
        }
        ENDALTS

        ANode* iffalse;
        if(elset.kind)
            iffalse = parser__getnode(par, p_nempty_stat);
        else
            iffalse = NULL;

        node = anode_create_tokenchi(par->fctx, ift, cond, iftrue, iffalse, ANODE_ENDLIST);
    }
    ALTERNATIVE
    {
        Token whilet = parser__shift(par, TK_WHILE);
        parser__shift(par, TK_LPAREN);
        ANode* cond = parser__getnode(par, p_expr);
        parser__shift(par, TK_RPAREN);
        ANode* body = parser__getnode(par, p_nempty_stat);

        node = anode_create_tokenchi(par->fctx, whilet, cond, body, ANODE_ENDLIST);
    }
    ALTERNATIVE
    {
        Token dot = parser__shift(par, TK_DO);
        ANode* body = parser__getnode(par, p_nempty_stat);
        parser__shift(par, TK_WHILE);
        parser__shift(par, TK_LPAREN);
        ANode* cond = parser__getnode(par, p_expr);
        parser__shift(par, TK_RPAREN);
        parser__shift(par, TK_SCOLON);

        node = anode_create_tokenchi(par->fctx, dot, cond, body, ANODE_ENDLIST);
    }
    ALTERNATIVE
    {
        Token fort = parser__shift(par, TK_FOR);
        parser__shift(par, TK_LPAREN);

        ANode* init;
        ALTS
        ALTERNATIVE
        {
            init = parser__getnode(par, p_var_decl_stat);
        }
        ALTERNATIVE
        {
            init = parser__getnode(par, p_assign_expr_list);
            parser__shift(par, TK_SCOLON);
        }
        ALTERNATIVE
        {
            init = NULL;
            parser__shift(par, TK_SCOLON);
        }
        ENDALTS

        ANode* cond;
        ALTS
        ALTERNATIVE
        {
            cond = parser__getnode(par, p_expr);
        }
        ALTERNATIVE
        {
            cond = NULL;
        }
        ENDALTS
        parser__shift(par, TK_SCOLON);

        ANode* next;
        ALTS
        ALTERNATIVE
        {
            next = parser__getnode(par, p_assign_expr_list);
        }
        ALTERNATIVE
        {
            next = NULL;
        }
        ENDALTS

        parser__shift(par, TK_RPAREN);

        ANode* body = parser__getnode(par, p_nempty_stat);

        node = anode_create_tokenchi(par->fctx, fort, init, cond, next, body, ANODE_ENDLIST);
    }
    ALTERNATIVE
    {
        Token returnt = parser__shift(par, TK_RETURN);
        ANode* expr = parser__getnode(par, p_expr_list);
        parser__shift(par, TK_SCOLON);

        node = anode_create_tokenchi(par->fctx, returnt, expr, ANODE_ENDLIST);
    }
    ALTERNATIVE
    {
        node = parser__getnode(par, p_assign_expr_list);
        parser__shift(par, TK_SCOLON);
    }
    ENDALTS
    REDUCE(node ? TokenKindNames[node->token.kind] : "<null>");
    return node;
}
/*
stat
    :   nempty_stat
    /   SCOLON
    ;
*/
static ANode* p_stat(Parser* par)
{
    TRACE();
    ANode* node;
    ALTS
    ALTERNATIVE
    {
        node = parser__getnode(par, p_nempty_stat);
    }
    ALTERNATIVE
    {
        node = NULL;
    }
    ENDALTS
    REDUCE(node ? TokenKindNames[node->token.kind] : "<null>");
    return node;
}

/*
assign_expr_list
    :   $parts~=assign_expr (COMMA $parts~=assign_expr)*    -> (I_LIST $parts...)
    ;
*/
static ANode* p_assign_expr_list(Parser* par)
{
    TRACE();
    ANode* part = parser__getnode(par, p_assign_expr);

    ANode* node = anode_create_kindchi(par->fctx, TKA_LIST, part, ANODE_ENDLIST);
    for(;;)
    {
        Token comma;
        ALTS
        ALTERNATIVE
        {
            comma = parser__shift(par, TK_COMMA);
        }
        ALTERNATIVE
        {
            comma.kind = TK_UNKNOWN;
        }
        ENDALTS
        if(!comma.kind) break;

        part = parser__getnode(par, p_assign_expr);
        anode_append_chi(node, part);
    }
    REDUCE(node ? TokenKindNames[node->token.kind] : "<null>");
    return node;
}
/*
assign_expr
    :   $lvalue=expr    $op=( OPASSIGN
                            / OPADDASSIGN / OPSUBASSIGN
                            / OPMULASSIGN / OPDIVASSIGN / OPREMASSIGN / OPMODASSIGN
                            / OPPOWASSIGN
                            / OPBXORASSIGN / OPBANDASSIGN / OPBORASSIGN
                            / OPBSHLASSIGN / OPBSHAASSIGN / OPBSHRASSIGN
                            / OPBROTLASSIGN / OPBROTRASSIGN
                            / OPCATASSIGN ) $rvalue=expr    -> (I_ASSIGN $op $lvalue $rvalue)
    /   expr
    ;
*/
static ANode* p_assign_expr(Parser* par)
{
    TRACE();
    ANode* node;
    ALTS
    ALTERNATIVE
    {
        ANode* lvalue = parser__getnode(par, p_expr);

        Token op;
        ALTS
        ALTERNATIVE { op = parser__shift(par, TK_EQ); }
        ALTERNATIVE { op = parser__shift(par, TK_PLUSEQ); } ALTERNATIVE { op = parser__shift(par, TK_MINUSEQ); }
        ALTERNATIVE { op = parser__shift(par, TK_STAREQ); } ALTERNATIVE { op = parser__shift(par, TK_SLASHEQ); } ALTERNATIVE { op = parser__shift(par, TK_PERCENTEQ); } ALTERNATIVE { op = parser__shift(par, TK_PERCENTPERCENTEQ); }
        ALTERNATIVE { op = parser__shift(par, TK_STARSTAREQ); }
        ALTERNATIVE { op = parser__shift(par, TK_CARETEQ); } ALTERNATIVE { op = parser__shift(par, TK_AMPEQ); } ALTERNATIVE { op = parser__shift(par, TK_PIPEEQ); }
        ALTERNATIVE { op = parser__shift(par, TK_LTLTEQ); } ALTERNATIVE { op = parser__shift(par, TK_GTGTEQ); } ALTERNATIVE { op = parser__shift(par, TK_GTGTGTEQ); }
        ALTERNATIVE { op = parser__shift(par, TK_LTLTPIPEEQ); } ALTERNATIVE { op = parser__shift(par, TK_PIPEGTGTEQ); }
        ALTERNATIVE { op = parser__shift(par, TK_TILDEEQ); }
        ENDALTS

        ANode* rvalue = parser__getnode(par, p_expr);

        node = anode_create_tokenchi(par->fctx, op, lvalue, rvalue, ANODE_ENDLIST);
    }
    ALTERNATIVE
    {
        node = parser__getnode(par, p_expr);
    }
    ENDALTS
    REDUCE(node ? TokenKindNames[node->token.kind] : "<null>");
    return node;
}
/*
expr_list
    :   $parts~=expr (COMMA $parts~=expr)*  -> (I_LIST $parts...)
    ;
*/
static ANode* p_expr_list(Parser* par)
{
    TRACE();
    ANode* part = parser__getnode(par, p_expr);

    ANode* node = anode_create_kindchi(par->fctx, TKA_LIST, part, ANODE_ENDLIST);
    for(;;)
    {
        Token comma;
        ALTS
        ALTERNATIVE
        {
            comma = parser__shift(par, TK_COMMA);
        }
        ALTERNATIVE
        {
            comma.kind = TK_UNKNOWN;
        }
        ENDALTS
        if(!comma.kind) break;

        part = parser__getnode(par, p_expr);
        anode_append_chi(node, part);
    }
    REDUCE(node ? TokenKindNames[node->token.kind] : "<null>");
    return node;
}
/*
expr
    :   $cond=binary_expr QMARK $iftrue=expr COLON $iffalse=expr    -> (QMARK $cond $iftrue $iffalse)
    /   binary_expr
    ;
*/
static ANode* p_expr(Parser* par)
{
    TRACE();
    ANode* node;
    ALTS
    ALTERNATIVE
    {
        ANode* cond = parser__getnode(par, p_binary_expr);
        Token qmarkt = parser__shift(par, TK_QMARK);
        ANode* iftrue = parser__getnode(par, p_expr);
        parser__shift(par, TK_COLON);
        ANode* iffalse = parser__getnode(par, p_expr);

        node = anode_create_tokenchi(par->fctx, qmarkt, cond, iftrue, iffalse, ANODE_ENDLIST);
    }
    ALTERNATIVE
    {
        node = parser__getnode(par, p_binary_expr);
    }
    ENDALTS
    REDUCE(node ? TokenKindNames[node->token.kind] : "<null>");
    return node;
}

typedef struct Operator
{
    TokenKind tkind;
    char assoc; // 'L'-left, 'R'-right, 'c'-cmp, '!'-disallowed
    signed char prec;
} Operator;
/*
18			x.y, x?.y, x(...), x[...], (...)			left
*/
static const Operator binops[] = {
    {TK_PIPEPIPE,'L',3},
    {TK_AMPAMP,'L',4},
    {TK_IN,'L',5},
    {TK_EXCLAIMIN,'L',5},
    {TK_IS,'c',6},
    {TK_EXCLAIMIS,'c',6},
    {TK_EQEQ,'c',6},
    {TK_EXCLAIMEQ,'c',6},
    {TK_LTEQ,'c',6},
    {TK_LT,'c',6},
    {TK_GTEQ,'c',6},
    {TK_GT,'c',6},
    {TK_LTEQGT,'!',7},
    {TK_TILDE,'L',8},
    {TK_PIPE,'L',9},
    {TK_CARET,'L',10},
    {TK_AMP,'L',11},
    {TK_LTLT,'L',12},
    {TK_GTGT,'L',12},
    {TK_GTGTGT,'L',12},
    {TK_LTLTPIPE,'L',12},
    {TK_PIPEGTGT,'L',12},
    {TK_PLUS,'L',13},
    {TK_MINUS,'L',13},
    {TK_STAR,'L',14},
    {TK_SLASH,'L',14},
    {TK_PERCENT,'L',14},
    {TK_PERCENTPERCENT,'L',14},
    /*prefix:15*/
    {TK_STARSTAR,'R',16},
    /*suffix:17*/
    {TK_DOT,'L',18},
};
static const Operator preops[] = {
    {TK_PLUS,'R',15},
    {TK_MINUS,'R',15},
    {TK_EXCLAIM,'R',15},
    {TK_TILDE,'R',15},
    {TK_STAR,'R',15},
    {TK_STARSTAR,'R',15},
    {TK_PLUSPLUS,'R',15},
    {TK_MINUSMINUS,'R',15},
};
static const Operator postops[] = {
    {TK_PLUSPLUS,'L',17},
    {TK_MINUSMINUS,'L',17},
    //{TK_QMARK,'L',17},
};
static Operator p__find_op(TokenKind kind, const Operator* ops, size_t nops);
static ANode* p__precedence_primary(Parser* par, ParseFunction* primary, int minprec);
static ANode* p__precedence_climb(Parser* par, ParseFunction* primary, int minprec);
static Operator p__find_op(TokenKind kind, const Operator* ops, size_t nops)
{
    static const Operator None = {TK_UNKNOWN,0,-1};
    while(nops--)
        if(ops[nops].tkind == kind)
            return ops[nops];
    return None;
}
static ANode* p__precedence_primary(Parser* par, ParseFunction* primary, int minprec)
{
    TRACE();
    ANode* node;
    Operator op;

    // prefix: +-*a => (+ _ (- _ (* _ a)))
    op = p__find_op(parser__peekk(par, 0), preops, sizeof(preops) / sizeof(*preops));
    if(op.prec >= minprec)
    {
        Token opt = parser__shift(par, op.tkind);
        ANode* b = p__precedence_primary(par, primary, op.assoc == 'L' ? op.prec + 1 : op.prec);
        node = anode_create_tokenchi(par->fctx, opt, ANODE_NULL, b, ANODE_ENDLIST);
    }
    else
        node = parser__getnode(par, primary);

    // postfix: a++-- => (-- (++ a _) _)
    for(;;)
    {
        op = p__find_op(parser__peekk(par, 0), postops, sizeof(postops) / sizeof(*postops));
        if(op.prec < minprec) break;
        Token opt = parser__shift(par, op.tkind);
        node = anode_create_tokenchi(par->fctx, opt, node, ANODE_NULL, ANODE_ENDLIST);
        minprec = op.assoc == 'L' ? op.prec + 1 : op.prec;
    }
    REDUCE(node ? TokenKindNames[node->token.kind] : "<null>");
    return node;
}
static ANode* p__precedence_climb(Parser* par, ParseFunction* primary, int minprec)
{
    TRACE();
    ANode* node = p__precedence_primary(par, primary, minprec);
    for(;;)
    {
        Operator op = p__find_op(parser__peekk(par, 0), binops, sizeof(binops) / sizeof(*binops));
        if(op.prec < minprec) break;
        Token opt = parser__shift(par, op.tkind);
        ANode* rhs = p__precedence_climb(par, primary, op.assoc == 'L' ? op.prec + 1 : op.prec);
        node = anode_create_tokenchi(par->fctx, opt, node, rhs, ANODE_ENDLIST);
    }
    REDUCE(node ? TokenKindNames[node->token.kind] : "<null>");
    return node;
}
static ANode* p_binary_expr(Parser* par)
{
    TRACE();
    ANode* node = p__precedence_climb(par, p_call_expr, 2); /* 1 is {`=`,`+=`,...}; 2 is `a?b:c` (but since it's right-associative, we allow `2`) */
    REDUCE(node ? TokenKindNames[node->token.kind] : "<null>");
    return node;
}

/*
call_expr
    :   $v=value (LPAREN expr_list? RPAREN / LARRAY expr_list? RARRAY)*
    ;
*/
static ANode* p_call_expr(Parser* par)
{
    TRACE();
    ANode* node = parser__getnode(par, p_value);
    for(;;)
    {
        ANode* nnode;
        ALTS
        ALTERNATIVE
        {
            Token lparen = parser__shift(par, TK_LPAREN);
            ANode* args;
            ALTS
            ALTERNATIVE
            {
                args = parser__getnode(par, p_expr_list);
            }
            ALTERNATIVE
            {
                args = NULL;
            }
            ENDALTS
            parser__shift(par, TK_RPAREN);

            nnode = anode_create_tokenchi(par->fctx, lparen, node, args, ANODE_ENDLIST);
        }
        ALTERNATIVE
        {
            Token larray = parser__shift(par, TK_LARRAY);
            ANode* args;
            ALTS
            ALTERNATIVE
            {
                args = parser__getnode(par, p_expr_list);
            }
            ALTERNATIVE
            {
                args = NULL;
            }
            ENDALTS
            parser__shift(par, TK_RARRAY);

            nnode = anode_create_tokenchi(par->fctx, larray, node, args, ANODE_ENDLIST);
        }
        ALTERNATIVE
        {
            nnode = NULL;
        }
        ENDALTS
        if(!nnode) break;
        node = nnode;
    }
    REDUCE(node ? TokenKindNames[node->token.kind] : "<null>");
    return node;
}

static ANode* p_value(Parser* par)
{
    TRACE();
    ANode* node = NULL;
    Token token;
    ALTS
    ALTERNATIVE
    {
        parser__shift(par, TK_LPAREN);
        ANode* expr = parser__getnode(par, p_assign_expr);
        parser__shift(par, TK_RPAREN);

        node = expr;
    }
    ALTERNATIVE { token = parser__shift(par, TK_INTEGER); }
    ALTERNATIVE { token = parser__shift(par, TK_REAL); }
    ALTERNATIVE { token = parser__shift(par, TK_CHAR); }
    ALTERNATIVE { token = parser__shift(par, TK_STRING); }
    ALTERNATIVE { token = parser__shift(par, TK_XSTRING); }
    ALTERNATIVE { token = parser__shift(par, TK_IDENT); }
    ALTERNATIVE
    {
        node = parser__getnode(par, p_array);
    }
    ALTERNATIVE { token = parser__shift(par, TK_THIS); }
    ALTERNATIVE { token = parser__shift(par, TK_SUPER); }
    ALTERNATIVE { token = parser__shift(par, TK_NULL); }
    ALTERNATIVE { token = parser__shift(par, TK_TRUE); }
    ALTERNATIVE { token = parser__shift(par, TK_FALSE); }
    ENDALTS
    if(!node) node = anode_create_token(par->fctx, token);
    REDUCE(node ? TokenKindNames[node->token.kind] : "<null>");
    return node;
}

/*
array
    :   LARRAY $items=array_member_list? RARRAY -> (LARRAY $items)
    ;
*/
static ANode* p_array(Parser* par)
{
    TRACE();
    Token larray = parser__shift(par, TK_LARRAY);
    ANode* items;
    ALTS
    ALTERNATIVE
    {
        items = parser__getnode(par, p_array_member_list);
    }
    ALTERNATIVE
    {
        items = NULL;
    }
    ENDALTS
    parser__shift(par, TK_RARRAY);
    REDUCE(TokenKindNames[larray.kind]);
    return anode_create_tokenchi(par->fctx, larray, items, ANODE_ENDLIST);
}
/*
array_member_list
    :   $parts~=array_member (COMMA $parts~=array_member)*  -> (I_LIST $parts...)
    ;
*/
static ANode* p_array_member_list(Parser* par)
{
    TRACE();
    ANode* part = parser__getnode(par, p_array_member);

    ANode* node = anode_create_kindchi(par->fctx, TKA_LIST, part, ANODE_ENDLIST);
    for(;;)
    {
        Token comma;
        ALTS
        ALTERNATIVE
        {
            comma = parser__shift(par, TK_COMMA);
        }
        ALTERNATIVE
        {
            comma.kind = TK_UNKNOWN;
        }
        ENDALTS
        if(!comma.kind) break;

        part = parser__getnode(par, p_array_member);
        anode_append_chi(node, part);
    }
    REDUCE(node ? TokenKindNames[node->token.kind] : "<null>");
    return node;
}
/*
array_member
    :   $k=value COLON $v=expr    -> (COLON $k $v)
    /   expr
    ;
*/
static ANode* p_array_member(Parser* par)
{
    TRACE();
    ANode* node;
    ALTS
    ALTERNATIVE
    {
        ANode* key = parser__getnode(par, p_value);
        Token colon = parser__shift(par, TK_COLON);
        ANode* val = parser__getnode(par, p_expr);

        node = anode_create_tokenchi(par->fctx, colon, key, val, ANODE_ENDLIST);
    }
    ALTERNATIVE
    {
        node = parser__getnode(par, p_expr);
    }
    ENDALTS
    REDUCE(node ? TokenKindNames[node->token.kind] : "<null>");
    return node;
}

/*
dotted_id
    :   $global=DOT? $i~=IDENT (DOT $i~=IDENT)*      -> (DOT $global $i...)
    ;
*/
static ANode* p_dotted_id(Parser* par)
{
    TRACE();
    ANode* global;
    ALTS
    ALTERNATIVE
    {
        global = anode_create_token(par->fctx, parser__shift(par, TK_DOT));
    }
    ALTERNATIVE
    {
        global = NULL;
    }
    ENDALTS

    ANode* ident = anode_create_token(par->fctx, parser__shift(par, TK_IDENT));

    ANode* node;
    if(global)
    {
        anode_append_chi(global, ident);
        node = global;
    }
    else
        node = ident;

    // a.b.c
    // (a.b).c
    // (. (. a b) c)
    for(;;)
    {
        ANode* dot;
        ALTS
        ALTERNATIVE
        {
            dot = anode_create_token(par->fctx, parser__shift(par, TK_DOT));
        }
        ALTERNATIVE
        {
            dot = NULL;
        }
        ENDALTS
        if(!dot) break;
        ident = anode_create_token(par->fctx, parser__shift(par, TK_IDENT));

        anode_append_chi(dot, node);
        anode_append_chi(dot, ident);
        node = dot;
    }
    REDUCE(node ? TokenKindNames[node->token.kind] : "<null>");
    return node;
}

/*
base_type
    :   (TYPE / dotted_id) (QMARK / OPMUL)*
    ;
*/
static ANode* p_base_type(Parser* par)
{
    TRACE();
    ANode* node;
    ALTS
    ALTERNATIVE
    {
        node = anode_create_token(par->fctx, parser__shift(par, TK_TYPE));
    }
    ALTERNATIVE
    {
        node = parser__getnode(par, p_dotted_id);
    }
    ENDALTS

    for(;;)
    {
        Token postop;
        ALTS
        ALTERNATIVE
        {
            postop = parser__shift(par, TK_QMARK);
        }
        ALTERNATIVE
        {
            postop = parser__shift(par, TK_STAR);
        }
        ALTERNATIVE
        {
            postop.kind = TK_UNKNOWN;
        }
        ENDALTS
        if(!postop.kind) break;

        node = anode_create_tokenchi(par->fctx, postop, node, ANODE_ENDLIST);
    }
    REDUCE(node ? TokenKindNames[node->token.kind] : "<null>");
    return node;
}
/*
type
    :   $base=base_type (LARRAY $key~=type? (COMMA $key~=type?)* RARRAY)*
    ;
*/
static ANode* p_type(Parser* par)
{
    TRACE();
    ANode* node = parser__getnode(par, p_base_type);
    for(;;)
    {
        Token larray;
        ALTS
        ALTERNATIVE
        {
            larray = parser__shift(par, TK_LARRAY);
        }
        ALTERNATIVE
        {
            larray.kind = TK_UNKNOWN;
        }
        ENDALTS
        if(!larray.kind) break;

        node = anode_create_kindchi(par->fctx, TK_LARRAY, node, ANODE_ENDLIST);

        ANode* key;
        ALTS
        ALTERNATIVE
        {
            key = parser__getnode(par, p_type);
        }
        ALTERNATIVE
        {
            key = NULL;
        }
        ENDALTS
        anode_append_chi(node, key);

        for(;;)
        {
            Token comma;
            ALTS
            ALTERNATIVE
            {
                comma = parser__shift(par, TK_COMMA);
            }
            ALTERNATIVE
            {
                comma.kind = TK_UNKNOWN;
            }
            ENDALTS
            if(!comma.kind) break;

            ALTS
            ALTERNATIVE
            {
                key = parser__getnode(par, p_type);
            }
            ALTERNATIVE
            {
                key = NULL;
            }
            ENDALTS
            anode_append_chi(node, key);
        }
        parser__shift(par, TK_RARRAY);
    }
    REDUCE(node ? TokenKindNames[node->token.kind] : "<null>");
    return node;
}

ANode* parser_exec(Parser* parser)
{
    parser->tokens = lexer_next_all(&parser->ntokens, parser->lexer, 0);
    assert(parser->ntokens);
    if(parser->tokens[parser->ntokens-1].kind == TK_UNKNOWN) return NULL;
    parser->pos = 0;

    COYusize jbuf = parser__pushbuf(parser);
    if(setjmp(parser->jbufs[jbuf])) return NULL;

    ANode* root = p_program(parser);
    if(!root) return NULL;
    if(parser__peekk(parser, 0) != TK_EOF)
        error_expect(parser, "<eof>");

    parser__setbuftop(parser, jbuf);
    return root;
}
