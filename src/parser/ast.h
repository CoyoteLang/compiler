#ifndef PARSER_AST_H_
#define PARSER_AST_H_

#include <lexer/token.h>

#include <stdarg.h>

typedef enum ANodeKind ANodeKind;
typedef struct ANode ANode;
typedef struct ANodeList ANodeList;

enum ANodeKind
{
    AK__HEAD = TK__NUM,

    AK_ROOT,
    AK_BLOCK,
    AK_CALL,
    AK_DECL_FUNC,
    AK_DECL_VAR,

    AK__NUM
};

struct ANodeList
{
    size_t len;
    ANode** ptr;
};
struct ANode
{
    Token token;
    ANodeList chi;
    struct SemInfo* sem;
};
ANode ANode_guard_expand;
ANode ANode_guard_endlist;

void alist_concat(ANodeList* a, ANodeList b);
void alist_append(ANodeList* a, ANode* node);

void anode_concat_chi(ANode* node, ANodeList list);
void anode_append_chi(ANode* node, ANode* child);

ANodeList* alist_init(ANodeList* alist);

#define ANODE_NULL      (void*)NULL
#define ANODE_EXPAND    &ANode_guard_expand
#define ANODE_ENDLIST   &ANode_guard_endlist
ANode* anode_create_tokenchiV(FileContext* fctx, Token token, va_list args);
ANode* anode_create_tokenchi(FileContext* fctx, Token token, ...);
ANode* anode_create_token(FileContext* fctx, Token token);
ANode* anode_create_kindchiV(FileContext* fctx, TokenKind kind, va_list args);
ANode* anode_create_kindchi(FileContext* fctx, TokenKind kind, ...);
ANode* anode_create_kind(FileContext* fctx, TokenKind kind);

ANode* anode_create_dup(FileContext* fctx, ANode* node);

void anode_dumpDBG(ANode* node);

#endif /* PARSER_AST_H_ */
