#ifndef PARSER_PARSER_H_
#define PARSER_PARSER_H_

#include <lexer/lexer.h>
#include <parser/ast.h>
#include <setjmp.h>

/* WARNING: Must be a power of 2 (including 2^0) */
/*#define PARSER_LOOKAHEAD    1
#define PARSER_LOOKMASK     (PARSER_LOOKAHEAD - 1)*/

/* WARNING: Must be a power of 2 (including 2^0) */
#define PARSERCACHE_INITSIZE    512
/* WARNING: num/den must be <= 1 */
#define PARSERCACHE_RATIO_NUM   3
#define PARSERCACHE_RATIO_DEN   4


typedef struct ParserCache
{
    COYusize count;
    COYusize mem;
    struct ParserCacheEntry** ents;
} ParserCache;

typedef struct Parser
{
    struct FileContext* fctx;
    Lexer* lexer;

    COYusize ntokens;
    Token* tokens;
    COYusize pos;

    COYusize jtop;
    jmp_buf* jbufs;

    ParserCache pcache;
} Parser;

Parser* parser_init(Parser* parser, Lexer* lexer, struct FileContext* fctx);
ANode* parser_exec(Parser* parser);

#endif /* PARSER_PARSER_H_ */
