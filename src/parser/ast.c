#include <parser/ast.h>
#include <util/memory.h>

#include <string.h>
#include <stdlib.h>

#include <assert.h>

// <DEBUG ONLY>
#include <stdio.h>
// </DEBUG ONLY>

void alist_concat(ANodeList* a, ANodeList b)
{
    a->ptr = mem_realloc(a->ptr, (a->len + b.len) * sizeof(ANode*));
    memcpy(a->ptr + a->len, b.ptr, b.len * sizeof(ANode*));
    a->len += b.len;
}
void alist_append(ANodeList* a, ANode* node)
{
    a->ptr = mem_realloc(a->ptr, (a->len + 1) * sizeof(ANode*));
    a->ptr[a->len++] = node;
}

void anode_concat_chi(ANode* node, ANodeList list)
{
    size_t i;
    for(i = 0; i < list.len; i++)
        node->token.range = frange_bounds(node->token.range, list.ptr[i]->token.range);
    alist_concat(&node->chi, list);
}
void anode_append_chi(ANode* node, ANode* child)
{
    if(!child) return;

    node->token.range = frange_bounds(node->token.range, child->token.range);
    alist_append(&node->chi, child);
}

ANodeList* alist_init(ANodeList* alist)
{
    if(!alist) return NULL;
    alist->len = 0;
    alist->ptr = NULL;
    return alist;
}

ANode* anode_create_tokenchiV(FileContext* fctx, Token token, va_list args)
{
    ANode* node = malloc(sizeof(ANode));
    node->token = token;
    alist_init(&node->chi);
    bool expand = false;
    for(;;)
    {
        ANode* child = va_arg(args, void*);
        if(child == &ANode_guard_expand)
        {
            expand = true;
            continue;
        }
        if(child == &ANode_guard_endlist) break;

        if(expand)
        {
            expand = false;
            anode_concat_chi(node, child->chi);
        }
        else
            anode_append_chi(node, child);
    }
    return node;
}
ANode* anode_create_tokenchi(FileContext* fctx, Token token, ...)
{
    va_list args;
    va_start(args, token);
    ANode* node = anode_create_tokenchiV(fctx, token, args);
    va_end(args);
    return node;
}
ANode* anode_create_token(FileContext* fctx, Token token)
{
    return anode_create_tokenchi(fctx, token, ANODE_ENDLIST);
}
ANode* anode_create_kindchiV(FileContext* fctx, TokenKind kind, va_list args)
{
    Token token;
    token.kind = kind;
    token.range.head.line = 0;
    token.range.head.col = 0;
    token.range.tail.line = 0;
    token.range.tail.col = 0;
    token.value = value_mkvoid(fctx);
    return anode_create_tokenchiV(fctx, token, args);
}
ANode* anode_create_kindchi(FileContext* fctx, TokenKind kind, ...)
{
    va_list args;
    va_start(args, kind);
    ANode* node = anode_create_kindchiV(fctx, kind, args);
    va_end(args);
    return node;
}
ANode* anode_create_kind(FileContext* fctx, TokenKind kind)
{
    return anode_create_kindchi(fctx, kind, ANODE_ENDLIST);
}

ANode* anode_create_dup(FileContext* fctx, ANode* node)
{
    ANode* nnode = anode_create_token(fctx, node->token);
    anode_concat_chi(nnode, node->chi);
    return nnode;
}

static void anode_dumpDBG_indent(ANode* node, int indent)
{
    if(!node)
    {
        printf("%*s<null>\n", indent * 2, "");
        return;
    }
    printf("%*s(%s", indent * 2, "", TokenKindNames[node->token.kind]);
    if(node->token.value.type)
    {
        printf(":");
        value_dumpDBG(node->token.value);
    }
    if(node->chi.len)
    {
        size_t i;
        for(i = 0; i < node->chi.len; i++)
        {
            printf("\n");
            anode_dumpDBG_indent(node->chi.ptr[i], indent + 1);
        }
        printf("%.*s", indent * 2, "");
    }
    printf(")");
}
void anode_dumpDBG(ANode* node)
{
    anode_dumpDBG_indent(node, 0);
}
